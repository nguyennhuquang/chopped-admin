(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["admin-admin-module"],{

/***/ "./src/app/admin/admin-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/admin/admin-routing.module.ts ***!
  \***********************************************/
/*! exports provided: AdminRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminRoutingModule", function() { return AdminRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _admin_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin.component */ "./src/app/admin/admin.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home/home.component */ "./src/app/admin/home/home.component.ts");
/* harmony import */ var _auth_auth_guard_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../auth/auth-guard.guard */ "./src/app/auth/auth-guard.guard.ts");
/* harmony import */ var _dish_dish_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dish/dish.component */ "./src/app/admin/dish/dish.component.ts");
/* harmony import */ var _material_material_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./material/material.component */ "./src/app/admin/material/material.component.ts");
/* harmony import */ var _material_category_material_category_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./material-category/material-category.component */ "./src/app/admin/material-category/material-category.component.ts");
/* harmony import */ var _tool_tool_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./tool/tool.component */ "./src/app/admin/tool/tool.component.ts");
/* harmony import */ var _spice_spice_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./spice/spice.component */ "./src/app/admin/spice/spice.component.ts");
/* harmony import */ var _timeline_timeline_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./timeline/timeline.component */ "./src/app/admin/timeline/timeline.component.ts");
/* harmony import */ var _material_dish_material_dish_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./material-dish/material-dish.component */ "./src/app/admin/material-dish/material-dish.component.ts");
/* harmony import */ var _country_country_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./country/country.component */ "./src/app/admin/country/country.component.ts");
/* harmony import */ var _spice_dish_spice_dish_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./spice-dish/spice-dish.component */ "./src/app/admin/spice-dish/spice-dish.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














var routes = [
    {
        path: '',
        children: [
            {
                path: 'dashboard',
                component: _home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"]
            },
            {
                path: 'dish',
                component: _dish_dish_component__WEBPACK_IMPORTED_MODULE_5__["DishComponent"],
            },
            {
                path: 'category',
                component: _material_category_material_category_component__WEBPACK_IMPORTED_MODULE_7__["MaterialCategoryComponent"],
            },
            {
                path: 'category/:id/material',
                component: _material_material_component__WEBPACK_IMPORTED_MODULE_6__["MaterialComponent"]
            },
            {
                path: 'tool',
                component: _tool_tool_component__WEBPACK_IMPORTED_MODULE_8__["ToolComponent"]
            },
            {
                path: 'spice',
                component: _spice_spice_component__WEBPACK_IMPORTED_MODULE_9__["SpiceComponent"]
            },
            {
                path: 'dish/:id/recipe',
                component: _timeline_timeline_component__WEBPACK_IMPORTED_MODULE_10__["TimelineComponent"]
            },
            {
                path: 'dish/:id/material',
                component: _material_dish_material_dish_component__WEBPACK_IMPORTED_MODULE_11__["MaterialDishComponent"]
            },
            {
                path: 'country',
                component: _country_country_component__WEBPACK_IMPORTED_MODULE_12__["CountryComponent"]
            },
            {
                path: 'dish/:id/spice',
                component: _spice_dish_spice_dish_component__WEBPACK_IMPORTED_MODULE_13__["SpiceDishComponent"]
            }
        ],
        component: _admin_component__WEBPACK_IMPORTED_MODULE_2__["AdminComponent"],
        canActivate: [_auth_auth_guard_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuardGuard"]]
    }
];
var AdminRoutingModule = /** @class */ (function () {
    function AdminRoutingModule() {
    }
    AdminRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AdminRoutingModule);
    return AdminRoutingModule;
}());



/***/ }),

/***/ "./src/app/admin/admin.component.html":
/*!********************************************!*\
  !*** ./src/app/admin/admin.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper\">\n    <app-header></app-header>\n\n    <div class=\"page-wrap\">\n        <app-nav-bar></app-nav-bar>\n\n        <router-outlet></router-outlet>\n\n        <app-footer></app-footer>\n    </div>\n</div>\n<ng-snotify></ng-snotify>\n\n\n\n"

/***/ }),

/***/ "./src/app/admin/admin.component.scss":
/*!********************************************!*\
  !*** ./src/app/admin/admin.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2FkbWluLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/admin.component.ts":
/*!******************************************!*\
  !*** ./src/app/admin/admin.component.ts ***!
  \******************************************/
/*! exports provided: AdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminComponent", function() { return AdminComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AdminComponent = /** @class */ (function () {
    function AdminComponent() {
    }
    AdminComponent.prototype.ngOnInit = function () {
    };
    AdminComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin',
            template: __webpack_require__(/*! ./admin.component.html */ "./src/app/admin/admin.component.html"),
            styles: [__webpack_require__(/*! ./admin.component.scss */ "./src/app/admin/admin.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AdminComponent);
    return AdminComponent;
}());



/***/ }),

/***/ "./src/app/admin/admin.module.ts":
/*!***************************************!*\
  !*** ./src/app/admin/admin.module.ts ***!
  \***************************************/
/*! exports provided: AdminModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminModule", function() { return AdminModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _admin_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./admin-routing.module */ "./src/app/admin/admin-routing.module.ts");
/* harmony import */ var _admin_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./admin.component */ "./src/app/admin/admin.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home/home.component */ "./src/app/admin/home/home.component.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var _dish_dish_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./dish/dish.component */ "./src/app/admin/dish/dish.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _material_material_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./material/material.component */ "./src/app/admin/material/material.component.ts");
/* harmony import */ var _material_category_material_category_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./material-category/material-category.component */ "./src/app/admin/material-category/material-category.component.ts");
/* harmony import */ var _tool_tool_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./tool/tool.component */ "./src/app/admin/tool/tool.component.ts");
/* harmony import */ var _spice_spice_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./spice/spice.component */ "./src/app/admin/spice/spice.component.ts");
/* harmony import */ var _timeline_timeline_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./timeline/timeline.component */ "./src/app/admin/timeline/timeline.component.ts");
/* harmony import */ var _material_dish_material_dish_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./material-dish/material-dish.component */ "./src/app/admin/material-dish/material-dish.component.ts");
/* harmony import */ var _country_country_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./country/country.component */ "./src/app/admin/country/country.component.ts");
/* harmony import */ var _spice_dish_spice_dish_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./spice-dish/spice-dish.component */ "./src/app/admin/spice-dish/spice-dish.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_admin_component__WEBPACK_IMPORTED_MODULE_3__["AdminComponent"], _home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"], _dish_dish_component__WEBPACK_IMPORTED_MODULE_6__["DishComponent"], _material_material_component__WEBPACK_IMPORTED_MODULE_8__["MaterialComponent"], _material_category_material_category_component__WEBPACK_IMPORTED_MODULE_9__["MaterialCategoryComponent"], _tool_tool_component__WEBPACK_IMPORTED_MODULE_10__["ToolComponent"], _spice_spice_component__WEBPACK_IMPORTED_MODULE_11__["SpiceComponent"], _timeline_timeline_component__WEBPACK_IMPORTED_MODULE_12__["TimelineComponent"], _material_dish_material_dish_component__WEBPACK_IMPORTED_MODULE_13__["MaterialDishComponent"], _country_country_component__WEBPACK_IMPORTED_MODULE_14__["CountryComponent"], _spice_dish_spice_dish_component__WEBPACK_IMPORTED_MODULE_15__["SpiceDishComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _admin_routing_module__WEBPACK_IMPORTED_MODULE_2__["AdminRoutingModule"],
                _core_core_module__WEBPACK_IMPORTED_MODULE_5__["CoreModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormsModule"]
            ],
            providers: []
        })
    ], AdminModule);
    return AdminModule;
}());



/***/ }),

/***/ "./src/app/admin/country/country.component.html":
/*!******************************************************!*\
  !*** ./src/app/admin/country/country.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n      <div class=\"page-header\">\n          <div class=\"row align-items-end\">\n              <div class=\"col-lg-8\">\n                  <div class=\"page-header-title\">\n                      <i class=\"ik ik-inbox bg-blue\"></i>\n                      <div class=\"d-inline\">\n                          <h5>Data Table</h5>\n                          <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"col-lg-4\">\n                  <nav class=\"breadcrumb-container\" aria-label=\"breadcrumb\">\n                      <ol class=\"breadcrumb\">\n                          <li class=\"breadcrumb-item\">\n                              <a href=\"../index.html\"><i class=\"ik ik-home\"></i></a>\n                          </li>\n                          <li class=\"breadcrumb-item\">\n                              <a href=\"#\">Tables</a>\n                          </li>\n                          <li class=\"breadcrumb-item active\" aria-current=\"page\">Data Table</li>\n                      </ol>\n                  </nav>\n              </div>\n          </div>\n      </div>\n\n\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <div class=\"card\">\n                  <div class=\"card-header d-block\">\n                      <h3 style=\"float: left;\">Quản lý Quốc gia</h3>\n                      <div class=\"input-group\" style=\"margin-bottom: 0 !important; float: right; width: unset;\">\n                          <button class=\"btn btn-info buttonAddNew\" type=\"submit\" style=\"height: 35px;\" (click)= \"openModalAdd()\">Create</button>\n                      </div>\n                  </div>\n                  <div class=\"card-body\">\n                      <table class=\"table\"> <!-- data_table -->\n                          <thead>\n                              <tr>\n                                  <th>#</th>\n                                  <th>Tên quốc gia</th>\n                                  <th>Mã quốc gia</th>\n                                  <th class=\"nosort\">Thao tác</th>\n                              </tr>\n                          </thead>\n                          <tbody>\n                              <tr *ngFor=\"let item of arr_country\">\n                                <td>{{item.id}}</td>\n                                <td>{{item.name}}</td>\n                                <td>{{item.code}}</td>\n                                <td>\n                                    <div class=\"table-actions\" style=\"text-align: left;\">\n                                        <a (click)=\"openModalEdit(item)\"><i class=\"ik ik-eye\"></i></a>\n                                        <a (click)=\"onDeleteModel(item.id)\"><i class=\"ik ik-trash-2\"></i></a>\n                                    </div>\n                                </td>\n                              </tr>\n                          </tbody>\n                      </table>\n                  </div>\n              </div>\n          </div>\n      </div>\n  </div>\n</div>\n\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"modalAddEdit\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n          <div class=\"modal-header\">\n              <h5 class=\"modal-title\" id=\"modalTitle\"></h5>\n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                  <span aria-hidden=\"true\">&times;</span>\n              </button>\n          </div>\n          <div class=\"card-body\">\n              <form class=\"forms-sample\" (ngSubmit)=\"onSubmit(f,image)\" #f=\"ngForm\">\n                <div class=\"form-group\">\n                    <label for=\"name\">Tên Quốc gia</label>\n                    <input type=\"text\" class=\"form-control\" id=\"name\" name=\"name\" placeholder=\"Tên danh mục\" ngModel>\n                </div>\n                <div class=\"form-group\">\n                  <label for=\"name\">Mã Quốc gia</label>\n                  <input type=\"text\" class=\"form-control\" id=\"code\" name=\"code\" placeholder=\"Mã Quốc gia\" ngModel>\n              </div>\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-secondary btnDelete\" data-dismiss=\"modal\">Delete</button>\n                    <button type=\"submit\" class=\"btn btn-primary\" id=\"submit\">Save changes</button>\n                </div>\n              </form>\n          </div>\n          \n      </div>\n  </div>\n</div>\n\n<!-- Modal Delete -->\n<div id=\"modalDelete\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog modal-dialog-centered\">\n      <div class=\"modal-content\" style=\"padding: 30px;\">\n          <input type=\"hidden\" id=\"id-delete\" name=\"id-delete\" />\n          <h2 class=\"h2-waring text-center\">Are You Sure?</h2>\n          <p class=\"lead text-muted text-center\" style=\"display: block;\">You\n              will not restore this data!</p>\n          <div class=\"sa-button-container\" style=\"text-align: center\">\n            <button class=\"confirm btn-lg btn-info\" data-dismiss=\"modal\" id=\"delete\" style=\"margin-right: 10px\" (click)=\"destroy()\">\n                Agree\n            </button>\n              <button class=\"cancel btn-lg btn-default\"\n                      style=\"border: 1px solid #ccc\" data-dismiss=\"modal\">Cancel</button>\n          </div>\n      </div>\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/country/country.component.scss":
/*!******************************************************!*\
  !*** ./src/app/admin/country/country.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2NvdW50cnkvY291bnRyeS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/country/country.component.ts":
/*!****************************************************!*\
  !*** ./src/app/admin/country/country.component.ts ***!
  \****************************************************/
/*! exports provided: CountryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountryComponent", function() { return CountryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/x/http/toast-notification.service */ "./src/x/http/toast-notification.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/shared/shared.service */ "./src/app/common/shared/shared.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CountryComponent = /** @class */ (function () {
    function CountryComponent(api, toast, router, shared) {
        this.api = api;
        this.toast = toast;
        this.router = router;
        this.shared = shared;
        this.arr_country = [];
    }
    CountryComponent.prototype.ngOnInit = function () {
        this.loadOrigin();
    };
    CountryComponent.prototype.openModalAdd = function () {
        this.modalAction = "add";
        $('#name').val("");
        $('#code').val("");
        $("#modalAddEdit").modal('show');
    };
    CountryComponent.prototype.openModalEdit = function (item) {
        this.modalAction = "edit";
        this.idCategory = item.id;
        $('#name').val(item.name);
        $('#code').val(item.code);
        $("#modalAddEdit").modal('show');
    };
    CountryComponent.prototype.onDeleteModel = function (idModel) {
        $('#modalDelete').modal('show');
        this.idCategory = idModel;
    };
    CountryComponent.prototype.onSubmit = function (f, image) {
        if ($('#name').val().trim() == "" || $('#code').val().trim() == "") {
            this.toast.error("Không được để trống trường truyền lên!");
            return;
        }
        switch (this.modalAction) {
            case "add":
                this.create(f);
                break;
            case "edit":
                this.update(f);
                break;
            default:
            // code block
        }
    };
    CountryComponent.prototype.create = function (f) {
        var _this = this;
        this.api.onAddCountry({ name: f.value.name, code: f.value.code }).subscribe(function (res) {
            if (res.code_status == 200) {
                _this.loadOrigin();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    CountryComponent.prototype.update = function (f) {
        var _this = this;
        var body = {
            id: this.idCategory,
            name: f.value.name,
            code: f.value.code
        };
        this.api.onUpdateCountry(body).subscribe(function (res) {
            if (res.code_status == 200) {
                _this.loadOrigin();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    CountryComponent.prototype.destroy = function () {
        var _this = this;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]();
        params = params.append('id', JSON.stringify(this.idCategory));
        this.api.onDeleteCountry(params).subscribe(function (res) {
            _this.loadOrigin();
            _this.toast.success(res['message']);
            $('#modalDelete').modal('hide');
        });
    };
    CountryComponent.prototype.loadOrigin = function () {
        var _this = this;
        this.api.ongetAllCountry().subscribe(function (res) {
            _this.arr_country = res.data;
        });
    };
    CountryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-country',
            template: __webpack_require__(/*! ./country.component.html */ "./src/app/admin/country/country.component.html"),
            styles: [__webpack_require__(/*! ./country.component.scss */ "./src/app/admin/country/country.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__["ToastNotificationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__["SharedService"]])
    ], CountryComponent);
    return CountryComponent;
}());



/***/ }),

/***/ "./src/app/admin/dish/dish.component.html":
/*!************************************************!*\
  !*** ./src/app/admin/dish/dish.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n      <div class=\"page-header\">\n          <div class=\"row align-items-end\">\n              <div class=\"col-lg-8\">\n                  <div class=\"page-header-title\">\n                      <i class=\"ik ik-inbox bg-blue\"></i>\n                      <div class=\"d-inline\">\n                          <h5>Data Table</h5>\n                          <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"col-lg-4\">\n                  <nav class=\"breadcrumb-container\" aria-label=\"breadcrumb\">\n                      <ol class=\"breadcrumb\">\n                          <li class=\"breadcrumb-item\">\n                              <a href=\"../index.html\"><i class=\"ik ik-home\"></i></a>\n                          </li>\n                          <li class=\"breadcrumb-item\">\n                              <a href=\"#\">Tables</a>\n                          </li>\n                          <li class=\"breadcrumb-item active\" aria-current=\"page\">Data Table</li>\n                      </ol>\n                  </nav>\n              </div>\n          </div>\n      </div>\n\n\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <div class=\"card\">\n                  <div class=\"card-header d-block\">\n                      <h3 style=\"float: left;\">Quản lý món ăn</h3>\n                      <div class=\"input-group\" style=\"margin-bottom: 0 !important; float: right; width: unset;\">\n                          <button class=\"btn btn-info buttonAddNew\" type=\"submit\" style=\"height: 35px;\" (click)= \"openModalAdd()\">Create</button>\n                      </div>\n                  </div>\n                  <div class=\"card-body\">\n                      <table class=\"table\"> <!-- data_table -->\n                          <thead>\n                              <tr>\n                                  <th>#</th>\n                                  <th>Món ăn</th>\n                                  <th class=\"nosort\">Ảnh</th>\n                                  <th>Xuất sứ</th>\n                                  <th>Nguyên Liệu</th>\n                                  <th>Công thức nấu ăn</th>\n                                  <th class=\"nosort\">Thao tác</th>\n                              </tr>\n                          </thead>\n                          <tbody>\n                              <tr *ngFor=\"let item of dish_response\">\n                                    <td>{{item.id}}</td>\n                                    <td>{{item.name}}</td>\n                                    <td><img src=\"{{item.image}}\" class=\"table-user-thumb text-left\" alt=\"{{item.name}}\"></td>\n                                    <td>{{item.origin.name}}</td>\n                                    <td>\n                                        <a routerLink=\"/admin/dish/{{item.id}}/material\">go to material</a>\n                                    </td>\n                                    <td>\n                                        <a routerLink=\"/admin/dish/{{item.id}}/recipe\">go to recipe</a>\n                                    </td>\n                                    <td>\n                                        <div class=\"table-actions\" style=\"text-align: left;\">\n                                            <a (click)=\"openModalEdit(item)\"><i class=\"ik ik-eye\"></i></a>\n                                            <a (click)=\"onDeleteModel(item.id)\"><i class=\"ik ik-trash-2\"></i></a>\n                                        </div>\n                                  </td>\n                              </tr>\n                          </tbody>\n                      </table>\n                  </div>\n              </div>\n          </div>\n      </div>\n  </div>\n</div>\n\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"modalAddEdit\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n          <div class=\"modal-header\">\n              <h5 class=\"modal-title\" id=\"modalTitle\"></h5>\n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                  <span aria-hidden=\"true\">&times;</span>\n              </button>\n          </div>\n          <div class=\"card-body\">\n              <form class=\"forms-sample\" (ngSubmit)=\"onSubmit(f,image)\" #f=\"ngForm\">\n                <div class=\"form-group\">\n                    <label for=\"name\">Tên món ăn</label>\n                    <input type=\"text\" class=\"form-control\" id=\"name\" name=\"name\" placeholder=\"Tên món ăn\" ngModel>\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"purchase-price\">Giá mua(VNĐ)</label>\n                    <input type=\"number\" class=\"form-control\" id=\"purchase-price\" placeholder=\"Giá mua(VNĐ)\" name=\"purchase_price\" ngModel>\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"price\">Giá bán(VNĐ)</label>\n                    <input type=\"number\" class=\"form-control\" id=\"price\" name=\"price\" ngModel placeholder=\"Giá bán(VNĐ)\">\n                </div>\n\n                <div class=\"form-group\">\n                    <label for=\"time\">Thời gian nấu</label>\n                    <select class=\"form-control\" id=\"time\" name=\"time\" [(ngModel)] = \"time\" >\n                        <option value=\"0\">--Chọn thời gian nấu--</option>\n                        <option value=\"900\">15 Phút</option>\n                        <option value=\"1800\">30 Phút</option>\n                        <option value=\"2700\">45 Phút</option>\n                    </select>\n                </div>\n\n                <div class=\"form-group\">\n                    <label for=\"origin\">Xuất sứ</label>\n                    <select class=\"form-control\" id=\"origin\" name=\"origin\" [(ngModel)] = \"origin\" >\n                        <option value=\"0\">--Chọn quốc gia--</option>\n                        <option *ngFor=\"let item of arr_country\" value=\"{{item.id}}\">{{item.name}}</option>\n                    </select>\n                </div>\n\n                <div class=\"form-group\">\n                    <label for=\"ration\">Khẩu phần</label>\n                    <input type=\"number\" class=\"form-control\" id=\"ration\" name=\"ration\" ngModel placeholder=\"Khẩu phần\">\n                </div>\n\n                <div class=\"form-group\">\n                    <label for=\"image\">Ảnh mô tả</label>\n                    <input type=\"file\" id=\"image\" class=\"form-control\" #image (change)=\"onFileChanged($event)\">\n                    <img [src]=\"imageUrl\" alt=\"no-image\" class=\"no-img\" id=\"preview\">\n                </div>\n\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-secondary btnDelete\" data-dismiss=\"modal\">Delete</button>\n                    <button type=\"submit\" class=\"btn btn-primary\" id=\"submit\">Save changes</button>\n                </div>\n              </form>\n          </div>\n          \n      </div>\n  </div>\n</div>\n\n<!-- Modal Delete -->\n<div id=\"modalDelete\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog modal-dialog-centered\">\n      <div class=\"modal-content\" style=\"padding: 30px;\">\n          <input type=\"hidden\" id=\"id-delete\" name=\"id-delete\" />\n          <h2 class=\"h2-waring text-center\">Are You Sure?</h2>\n          <p class=\"lead text-muted text-center\" style=\"display: block;\">You\n              will not restore this data!</p>\n          <div class=\"sa-button-container\" style=\"text-align: center\">\n            <button class=\"confirm btn-lg btn-info\" data-dismiss=\"modal\" id=\"delete\" style=\"margin-right: 10px\" (click)=\"destroy()\">\n                Agree\n            </button>\n              <button class=\"cancel btn-lg btn-default\"\n                      style=\"border: 1px solid #ccc\" data-dismiss=\"modal\">Cancel</button>\n          </div>\n      </div>\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/dish/dish.component.scss":
/*!************************************************!*\
  !*** ./src/app/admin/dish/dish.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL2Rpc2gvZGlzaC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/dish/dish.component.ts":
/*!**********************************************!*\
  !*** ./src/app/admin/dish/dish.component.ts ***!
  \**********************************************/
/*! exports provided: DishComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DishComponent", function() { return DishComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/x/http/toast-notification.service */ "./src/x/http/toast-notification.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/common/shared/shared.service */ "./src/app/common/shared/shared.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DishComponent = /** @class */ (function () {
    function DishComponent(api, toast, router, shared) {
        this.api = api;
        this.toast = toast;
        this.router = router;
        this.shared = shared;
        this.dish_response = [];
        this.arr_country = [];
        this.origin = "0";
        this.time = "0";
        this.imageUrl = "assets/img/no-img.jpg";
    }
    DishComponent.prototype.ngOnInit = function () {
        this.loadAllRecord();
        this.loadOrigin();
        this.origin = "0";
        this.time = "0";
    };
    DishComponent.prototype.openModalAdd = function () {
        this.modalAction = "add";
        $('#name').val("");
        $('#price').val("");
        $('#purchase-price').val("");
        $('#ration').val("");
        this.origin = "0";
        this.time = "0";
        $("#modalAddEdit").modal('show');
        $('#image').prop("value", "");
        $("#preview").attr("src", "assets/img/no-img.jpg");
    };
    DishComponent.prototype.openModalEdit = function (item) {
        this.modalAction = "edit";
        this.idDish = item.id;
        $('#name').val(item.name);
        $('#price').val(item.price);
        $('#ration').val(item.ration);
        $('#purchase-price').val(item.purchase_price);
        $('#image').prop("value", "");
        this.origin = item.origin.id;
        this.time = item.time;
        $("#preview").attr("src", item.image);
        $("#modalAddEdit").modal('show');
    };
    /**
   * open model delete Category
   */
    DishComponent.prototype.onDeleteModel = function (idModel) {
        $('#modalDelete').modal('show');
        this.idDish = idModel;
    };
    DishComponent.prototype.onSubmit = function (f, image) {
        if ($('#name').val().trim() == "" || $('#price').val().trim() == "" || $('#purchase-price').val().trim() == "" || f.value.time == "0" || f.value.origin == "0") {
            this.toast.error("Không được để trống trường truyền lên!");
            return;
        }
        switch (this.modalAction) {
            case "add":
                this.create(f, image);
                break;
            case "edit":
                this.update(f, image);
                break;
            default:
            // code block
        }
    };
    DishComponent.prototype.create = function (f, image) {
        var _this = this;
        var formData = new FormData();
        if (!image.files[0]) {
            this.toast.error("Không được để trống trường truyền lên!");
            return;
        }
        formData.set('image', image.files[0]);
        formData.set('name', f.value.name);
        formData.set('price', f.value.price);
        formData.set('ration', f.value.ration);
        formData.set('purchase_price', f.value.purchase_price);
        formData.set('origin', f.value.origin);
        formData.set('time', f.value.time);
        this.api.onAddDish(formData).subscribe(function (res) {
            if (res.code_status == 200) {
                $("#modalAddEdit").modal('hide');
                _this.toast.success(res.message);
                _this.loadAllRecord();
            }
        });
    };
    ;
    DishComponent.prototype.update = function (f, image) {
        var _this = this;
        var formData = new FormData();
        if (image.files[0]) {
            formData.set('image', image.files[0]);
        }
        formData.set('id', JSON.stringify(this.idDish));
        formData.set('name', $('#name').val());
        formData.set('price', $('#price').val());
        formData.set('ration', $('#ration').val());
        formData.set('purchase_price', $('#purchase-price').val());
        formData.set('origin', $('#origin').val());
        formData.set('time', $('#time').val());
        this.api.onUpdateDish(formData).subscribe(function (res) {
            if (res.code_status == 200) {
                $("#modalAddEdit").modal('hide');
                _this.toast.success(res.message);
                _this.loadAllRecord();
            }
        });
    };
    DishComponent.prototype.destroy = function () {
        var _this = this;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpParams"]();
        params = params.append('id', JSON.stringify(this.idDish));
        this.api.onDeleteDish(params).subscribe(function (res) {
            _this.loadAllRecord();
            _this.toast.success(res['message']);
            $('#modalDelete').modal('hide');
        });
    };
    DishComponent.prototype.loadAllRecord = function () {
        var _this = this;
        this.api.onGetDish().subscribe(function (res) {
            _this.dish_response = res.data;
        });
    };
    DishComponent.prototype.loadOrigin = function () {
        var _this = this;
        this.api.ongetAllCountry().subscribe(function (res) {
            _this.arr_country = res.data;
        });
    };
    /**
   * function preview images
   * @param event
   */
    DishComponent.prototype.onFileChanged = function (event) {
        this.shared.previewImage(event, 'preview');
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('f'),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_4__["NgForm"])
    ], DishComponent.prototype, "mytemplateForm", void 0);
    DishComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dish',
            template: __webpack_require__(/*! ./dish.component.html */ "./src/app/admin/dish/dish.component.html"),
            styles: [__webpack_require__(/*! ./dish.component.scss */ "./src/app/admin/dish/dish.component.scss")],
        }),
        __metadata("design:paramtypes", [src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__["ToastNotificationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_5__["SharedService"]])
    ], DishComponent);
    return DishComponent;
}());



/***/ }),

/***/ "./src/app/admin/home/home.component.html":
/*!************************************************!*\
  !*** ./src/app/admin/home/home.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n      <div class=\"row clearfix\">\n          <div class=\"col-lg-3 col-md-6 col-sm-12\">\n              <div class=\"widget\">\n                  <div class=\"widget-body\">\n                      <div class=\"d-flex justify-content-between align-items-center\">\n                          <div class=\"state\">\n                              <h6>Bookmarks</h6>\n                              <h2>1,410</h2>\n                          </div>\n                          <div class=\"icon\">\n                              <i class=\"ik ik-award\"></i>\n                          </div>\n                      </div>\n                      <small class=\"text-small mt-10 d-block\">6% higher than last month</small>\n                  </div>\n                  <div class=\"progress progress-sm\">\n                      <div class=\"progress-bar bg-danger\" role=\"progressbar\" aria-valuenow=\"62\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 62%;\"></div>\n                  </div>\n              </div>\n          </div>\n          <div class=\"col-lg-3 col-md-6 col-sm-12\">\n              <div class=\"widget\">\n                  <div class=\"widget-body\">\n                      <div class=\"d-flex justify-content-between align-items-center\">\n                          <div class=\"state\">\n                              <h6>Likes</h6>\n                              <h2>41,410</h2>\n                          </div>\n                          <div class=\"icon\">\n                              <i class=\"ik ik-thumbs-up\"></i>\n                          </div>\n                      </div>\n                      <small class=\"text-small mt-10 d-block\">61% higher than last month</small>\n                  </div>\n                  <div class=\"progress progress-sm\">\n                      <div class=\"progress-bar bg-success\" role=\"progressbar\" aria-valuenow=\"78\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 78%;\"></div>\n                  </div>\n              </div>\n          </div>\n          <div class=\"col-lg-3 col-md-6 col-sm-12\">\n              <div class=\"widget\">\n                  <div class=\"widget-body\">\n                      <div class=\"d-flex justify-content-between align-items-center\">\n                          <div class=\"state\">\n                              <h6>Events</h6>\n                              <h2>410</h2>\n                          </div>\n                          <div class=\"icon\">\n                              <i class=\"ik ik-calendar\"></i>\n                          </div>\n                      </div>\n                      <small class=\"text-small mt-10 d-block\">Total Events</small>\n                  </div>\n                  <div class=\"progress progress-sm\">\n                      <div class=\"progress-bar bg-warning\" role=\"progressbar\" aria-valuenow=\"31\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 31%;\"></div>\n                  </div>\n              </div>\n          </div>\n          <div class=\"col-lg-3 col-md-6 col-sm-12\">\n              <div class=\"widget\">\n                  <div class=\"widget-body\">\n                      <div class=\"d-flex justify-content-between align-items-center\">\n                          <div class=\"state\">\n                              <h6>Comments</h6>\n                              <h2>41,410</h2>\n                          </div>\n                          <div class=\"icon\">\n                              <i class=\"ik ik-message-square\"></i>\n                          </div>\n                      </div>\n                      <small class=\"text-small mt-10 d-block\">Total Comments</small>\n                  </div>\n                  <div class=\"progress progress-sm\">\n                      <div class=\"progress-bar bg-info\" role=\"progressbar\" aria-valuenow=\"20\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: 20%;\"></div>\n                  </div>\n              </div>\n          </div>\n      </div>\n      <div class=\"row\">\n          <div class=\"col-md-8\">\n              <div class=\"card\">\n                  <div class=\"card-body\">\n                      <div class=\"row align-items-center\">\n                          <div class=\"col-lg-8 col-md-12\">\n                              <h3 class=\"card-title\">Visitors By Countries</h3>\n                              <div id=\"visitfromworld\" style=\"width:100%; height:350px\"></div>\n                          </div>\n                          <div class=\"col-lg-4 col-md-12\">\n                              <div class=\"row mb-15\">\n                                  <div class=\"col-9\">India</div>\n                                  <div class=\"col-3 text-right\">28%</div>\n                                  <div class=\"col-12\">\n                                      <div class=\"progress progress-sm mt-5\">\n                                          <div class=\"progress-bar bg-green\" role=\"progressbar\" style=\"width: 48%\" aria-valuenow=\"48\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                                      </div>\n                                  </div>\n                              </div>\n                              <div class=\"row mb-15\">\n                                  <div class=\"col-9\"> UK</div>\n                                  <div class=\"col-3 text-right\">21%</div>\n                                  <div class=\"col-12\">\n                                      <div class=\"progress progress-sm mt-5\">\n                                          <div class=\"progress-bar bg-aqua\" role=\"progressbar\" style=\"width: 33%\" aria-valuenow=\"33\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                                      </div>\n                                  </div>\n                              </div>\n                              <div class=\"row mb-15\">\n                                  <div class=\"col-9\"> USA</div>\n                                  <div class=\"col-3 text-right\">18%</div>\n                                  <div class=\"col-12\">\n                                      <div class=\"progress progress-sm mt-5\">\n                                          <div class=\"progress-bar bg-purple\" role=\"progressbar\" style=\"width: 40%\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                                      </div>\n                                  </div>\n                              </div>\n                              <div class=\"row\">\n                                  <div class=\"col-9\">China</div>\n                                  <div class=\"col-3 text-right\">12%</div>\n                                  <div class=\"col-12\">\n                                      <div class=\"progress progress-sm mt-5\">\n                                          <div class=\"progress-bar bg-danger\" role=\"progressbar\" style=\"width: 15%\" aria-valuenow=\"15\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                                      </div>\n                                  </div>\n                              </div>\n                          </div>\n                      </div>\n                  </div>\n              </div>\n          </div>\n          <div class=\"col-md-4\">\n              <div class=\"card\" style=\"min-height: 422px;\">\n                  <div class=\"card-header\"><h3>Donut chart</h3></div>\n                  <div class=\"card-body\">\n                      <div id=\"c3-donut-chart\"></div>\n                  </div>\n              </div>\n          </div>\n      </div>\n\n      <div class=\"row\">\n          <div class=\"col-md-4\">\n              <div class=\"card\">\n                  <div class=\"card-header\">\n                      <h3>Recent Chat</h3>\n                      <div class=\"card-header-right\">\n                          <ul class=\"list-unstyled card-option\">\n                              <li><i class=\"ik ik-chevron-left action-toggle\"></i></li>\n                              <li><i class=\"ik ik-minus minimize-card\"></i></li>\n                              <li><i class=\"ik ik-x close-card\"></i></li>\n                          </ul>\n                      </div>\n                  </div>\n                  <div class=\"card-body chat-box scrollable\" style=\"height:300px;\">\n                      <ul class=\"chat-list\">\n                          <li class=\"chat-item\">\n                              <div class=\"chat-img\"><img src=\"assets/img/users/1.jpg\" alt=\"user\"></div>\n                              <div class=\"chat-content\">\n                                  <h6 class=\"font-medium\">James Anderson</h6>\n                                  <div class=\"box bg-light-info\">Lorem Ipsum is simply dummy text of the printing &amp; type setting industry.</div>\n                              </div>\n                              <div class=\"chat-time\">10:56 am</div>\n                          </li>\n                          <li class=\"chat-item\">\n                              <div class=\"chat-img\"><img src=\"assets/img/users/2.jpg\" alt=\"user\"></div>\n                              <div class=\"chat-content\">\n                                  <h6 class=\"font-medium\">Bianca Doe</h6>\n                                  <div class=\"box bg-light-info\">It’s Great opportunity to work.</div>\n                              </div>\n                              <div class=\"chat-time\">10:57 am</div>\n                          </li>\n                          <li class=\"odd chat-item\">\n                              <div class=\"chat-content\">\n                                  <div class=\"box bg-light-inverse\">I would love to join the team.</div>\n                                  <br>\n                              </div>\n                          </li>\n                          <li class=\"odd chat-item\">\n                              <div class=\"chat-content\">\n                                  <div class=\"box bg-light-inverse\">Whats budget of the new project.</div>\n                                  <br>\n                              </div>\n                              <div class=\"chat-time\">10:59 am</div>\n                          </li>\n                          <li class=\"chat-item\">\n                              <div class=\"chat-img\"><img src=\"assets/img/users/3.jpg\" alt=\"user\"></div>\n                              <div class=\"chat-content\">\n                                  <h6 class=\"font-medium\">Angelina Rhodes</h6>\n                                  <div class=\"box bg-light-info\">Well we have good budget for the project</div>\n                              </div>\n                              <div class=\"chat-time\">11:00 am</div>\n                          </li>\n                      </ul>\n                  </div>\n                  <div class=\"card-footer chat-footer\">\n                      <div class=\"input-wrap\">\n                          <input type=\"text\" placeholder=\"Type and enter\" class=\"form-control\">\n                      </div>\n                      <button type=\"button\" class=\"btn btn-icon btn-theme\"><i class=\"fa fa-paper-plane\"></i></button>\n                  </div>\n              </div>\n          </div>\n          <div class=\"col-md-4\">\n              <div class=\"card\">\n                  <div class=\"card-body\">\n                      <div class=\"d-flex\">\n                          <h4 class=\"card-title\">Weather Report</h4>\n                          <select class=\"form-control w-25 ml-auto\">\n                              <option selected=\"\">Today</option>\n                              <option value=\"1\">Weekly</option>\n                          </select>\n                      </div>\n                      <div class=\"d-flex align-items-center flex-row mt-30\">\n                          <div class=\"p-2 f-50 text-info\"><i class=\"wi wi-day-showers\"></i> <span>23<sup>°</sup></span></div>\n                          <div class=\"p-2\">\n                          <h3 class=\"mb-0\">Saturday</h3><small>Banglore, India</small></div>\n                      </div>\n                      <table class=\"table table-borderless\">\n                          <tbody>\n                              <tr>\n                                  <td>Wind</td>\n                                  <td class=\"font-medium\">ESE 17 mph</td>\n                              </tr>\n                              <tr>\n                                  <td>Humidity</td>\n                                  <td class=\"font-medium\">83%</td>\n                              </tr>\n                              <tr>\n                                  <td>Pressure</td>\n                                  <td class=\"font-medium\">28.56 in</td>\n                              </tr>\n                          </tbody>\n                      </table>\n                      <hr>\n                      <ul class=\"list-unstyled row text-center city-weather-days mb-0 mt-20\">\n                          <li class=\"col\"><i class=\"wi wi-day-sunny mr-5\"></i><span>09:30</span><h3>20<sup>°</sup></h3></li>\n                          <li class=\"col\"><i class=\"wi wi-day-cloudy mr-5\"></i><span>11:30</span><h3>22<sup>°</sup></h3></li>\n                          <li class=\"col\"><i class=\"wi wi-day-hail mr-5\"></i><span>13:30</span><h3>25<sup>°</sup></h3></li>\n                      </ul>\n                  </div>\n              </div>\n          </div>\n          <div class=\"col-md-4\">\n              <div class=\"card\" style=\"min-height: 422px;\">\n                  <div class=\"card-header\">\n                      <h3>Timeline</h3>\n                      <div class=\"card-header-right\">\n                          <ul class=\"list-unstyled card-option\">\n                              <li><i class=\"ik ik-chevron-left action-toggle\"></i></li>\n                              <li><i class=\"ik ik-minus minimize-card\"></i></li>\n                              <li><i class=\"ik ik-x close-card\"></i></li>\n                          </ul>\n                      </div>\n                  </div>\n                  <div class=\"card-body timeline\">\n                      <div class=\"header bg-theme\" style=\"background-image: url('assets/img/placeholder/placeimg_400_200_nature.jpg')\">\n                          <div class=\"color-overlay d-flex align-items-center\">\n                              <div class=\"day-number\">8</div>\n                              <div class=\"date-right\">\n                                  <div class=\"day-name\">Monday</div>\n                                  <div class=\"month\">February 2018</div>\n                              </div>\n                          </div>                                \n                      </div>\n                      <ul>\n                          <li>\n                              <div class=\"bullet bg-pink\"></div>\n                              <div class=\"time\">11am</div>\n                              <div class=\"desc\">\n                                  <h3>Attendance</h3>\n                                  <h4>Computer Class</h4>\n                              </div>\n                          </li>\n                          <li>\n                              <div class=\"bullet bg-green\"></div>\n                              <div class=\"time\">12pm</div>\n                              <div class=\"desc\">\n                                  <h3>Design Team</h3>\n                                  <h4>Hangouts</h4>\n                              </div>\n                          </li>\n                          <li>\n                              <div class=\"bullet bg-orange\"></div>\n                              <div class=\"time\">2pm</div>\n                              <div class=\"desc\">\n                                  <h3>Finish</h3>\n                                  <h4>Go to Home</h4>\n                              </div>\n                          </li>\n                      </ul>\n                  </div>\n              </div>\n          </div>\n      </div>\n\n\n      <div class=\"card\">\n          <div class=\"card-header row\">\n              <div class=\"col col-sm-3\">\n                  <div class=\"dropdown d-inline-block\">\n                      <a class=\"btn-icon checkbox-dropdown dropdown-toggle\" href=\"#\" id=\"moreDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"></a>\n                      <div class=\"dropdown-menu\" aria-labelledby=\"moreDropdown\">\n                          <a class=\"dropdown-item\" id=\"checkbox_select_all\" href=\"javascript:void(0);\">Select All</a>\n                          <a class=\"dropdown-item\" id=\"checkbox_deselect_all\" href=\"javascript:void(0);\">Deselect All</a>\n                      </div>\n                  </div>\n                  <div class=\"card-options d-inline-block\">\n                      <a href=\"#\"><i class=\"ik ik-inbox\"></i></a>\n                      <a href=\"#\"><i class=\"ik ik-plus\"></i></a>\n                      <a href=\"#\"><i class=\"ik ik-rotate-cw\"></i></a>\n                      <div class=\"dropdown d-inline-block\">\n                          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"moreDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"ik ik-more-horizontal\"></i></a>\n                          <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"moreDropdown\">\n                              <a class=\"dropdown-item\" href=\"#\">Action</a>\n                              <a class=\"dropdown-item\" href=\"#\">More Action</a>\n                          </div>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"col col-sm-6\">\n                  <div class=\"card-search with-adv-search dropdown\">\n                      <form action=\"\">\n                          <input type=\"text\" class=\"form-control\" placeholder=\"Search..\" required>\n                          <button type=\"submit\" class=\"btn btn-icon\"><i class=\"ik ik-search\"></i></button>\n                          <button type=\"button\" id=\"adv_wrap_toggler\" class=\"adv-btn ik ik-chevron-down dropdown-toggle\" data-toggle=\"dropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"></button>\n                          <div class=\"adv-search-wrap dropdown-menu dropdown-menu-right\" aria-labelledby=\"adv_wrap_toggler\">\n                              <div class=\"form-group\">\n                                  <input type=\"text\" class=\"form-control\" placeholder=\"Full Name\">\n                              </div>\n                              <div class=\"form-group\">\n                                  <input type=\"email\" class=\"form-control\" placeholder=\"Email\">\n                              </div>\n                              <button class=\"btn btn-theme\">Search</button>\n                          </div>\n                      </form>\n                  </div>\n              </div>\n              <div class=\"col col-sm-3\">\n                  <div class=\"card-options text-right\">\n                      <span class=\"mr-5\">1 - 50 of 2,500</span>\n                      <a href=\"#\"><i class=\"ik ik-chevron-left\"></i></a>\n                      <a href=\"#\"><i class=\"ik ik-chevron-right\"></i></a>\n                  </div>\n              </div>\n          </div>\n          <div class=\"card-body p-0\">\n              <div class=\"list-item-wrap\">\n                  <div class=\"list-item\">\n                      <div class=\"item-inner\">\n                          <label class=\"custom-control custom-checkbox\">\n                              <input type=\"checkbox\" class=\"custom-control-input\" id=\"item_checkbox\" name=\"item_checkbox\" value=\"option1\">\n                              <span class=\"custom-control-label\">&nbsp;</span>\n                          </label>\n                          <div class=\"list-title\"><a href=\"javascript:void(0)\">Lorem Ipsum is simply dumm dummy text of the printing and typesetting industry.</a></div>\n                          <div class=\"list-actions\">\n                              <a href=\"#\"><i class=\"ik ik-eye\"></i></a>\n                              <a href=\"#\"><i class=\"ik ik-inbox\"></i></a>\n                              <a href=\"#\"><i class=\"ik ik-edit-2\"></i></a>\n                              <a href=\"#\"><i class=\"ik ik-trash-2\"></i></a>\n                          </div>\n                      </div>\n\n                      <div class=\"qickview-wrap\">\n                          <div class=\"desc\">\n                              <p>Fusce suscipit turpis a dolor posuere ornare at a ante. Quisque nec libero facilisis, egestas tortor eget, mattis dui. Curabitur viverra laoreet ligula at hendrerit. Nullam sollicitudin maximus leo, vel pulvinar orci semper id. Donec vehicula tempus enim a facilisis. Proin dignissim porttitor sem, sed pulvinar tortor gravida vitae.</p>\n                          </div>\n                      </div>\n                  </div>\n                  <div class=\"list-item\">\n                      <div class=\"item-inner\">\n                          <label class=\"custom-control custom-checkbox\">\n                              <input type=\"checkbox\" class=\"custom-control-input\" id=\"item_checkbox\" name=\"item_checkbox\" value=\"option2\">\n                              <span class=\"custom-control-label\">&nbsp;</span>\n                          </label>\n                          <div class=\"list-title\"><a href=\"javascript:void(0)\">Aenean eu pharetra arcu, vitae elementum sem. Sed non ligula molestie, finibus lacus at, suscipit mi. Nunc luctus lacus vel felis blandit, eu finibus augue tincidunt.</a></div>\n                          <div class=\"list-actions\">\n                              <a href=\"#\"><i class=\"ik ik-eye\"></i></a>\n                              <a href=\"#\"><i class=\"ik ik-inbox\"></i></a>\n                              <a href=\"#\"><i class=\"ik ik-edit-2\"></i></a>\n                              <a href=\"#\"><i class=\"ik ik-trash-2\"></i></a>\n                          </div>\n                      </div>\n                      <div class=\"qickview-wrap\">\n                          <div class=\"desc\">\n                              <p>Fusce suscipit turpis a dolor posuere ornare at a ante. Quisque nec libero facilisis, egestas tortor eget, mattis dui. Curabitur viverra laoreet ligula at hendrerit. Nullam sollicitudin maximus leo, vel pulvinar orci semper id. Donec vehicula tempus enim a facilisis. Proin dignissim porttitor sem, sed pulvinar tortor gravida vitae.</p>\n                          </div>\n                      </div>\n                  </div>\n                  <div class=\"list-item\">\n                      <div class=\"item-inner\">\n                          <label class=\"custom-control custom-checkbox\">\n                              <input type=\"checkbox\" class=\"custom-control-input\" id=\"item_checkbox\" name=\"item_checkbox\" value=\"option3\">\n                              <span class=\"custom-control-label\">&nbsp;</span>\n                          </label>\n                          <div class=\"list-title\"><a href=\"javascript:void(0)\">Donec lectus augue, suscipit in sodales sit amet, semper sit amet enim. Duis pretium, nisi id pretium ornare, tortor nibh sodales tellus.</a></div>\n                          <div class=\"list-actions\">\n                              <a href=\"#\"><i class=\"ik ik-eye\"></i></a>\n                              <a href=\"#\"><i class=\"ik ik-inbox\"></i></a>\n                              <a href=\"#\"><i class=\"ik ik-edit-2\"></i></a>\n                              <a href=\"#\"><i class=\"ik ik-trash-2\"></i></a>\n                          </div>\n                      </div>\n                      <div class=\"qickview-wrap\">\n                          <div class=\"desc\">\n                              <p>Fusce suscipit turpis a dolor posuere ornare at a ante. Quisque nec libero facilisis, egestas tortor eget, mattis dui. Curabitur viverra laoreet ligula at hendrerit. Nullam sollicitudin maximus leo, vel pulvinar orci semper id. Donec vehicula tempus enim a facilisis. Proin dignissim porttitor sem, sed pulvinar tortor gravida vitae.</p>\n                          </div>\n                      </div>\n                  </div>\n              </div>\n          </div>\n      </div>\n      <div class=\"card\">\n          <div class=\"card-header row\">\n              <div class=\"col col-sm-3\">\n                  <div class=\"card-options d-inline-block\">\n                      <a href=\"#\"><i class=\"ik ik-inbox\"></i></a>\n                      <a href=\"#\"><i class=\"ik ik-plus\"></i></a>\n                      <a href=\"#\"><i class=\"ik ik-rotate-cw\"></i></a>\n                      <div class=\"dropdown d-inline-block\">\n                          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"moreDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"ik ik-more-horizontal\"></i></a>\n                          <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"moreDropdown\">\n                              <a class=\"dropdown-item\" href=\"#\">Action</a>\n                              <a class=\"dropdown-item\" href=\"#\">More Action</a>\n                          </div>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"col col-sm-6\">\n                  <div class=\"card-search with-adv-search dropdown\">\n                      <form action=\"\">\n                          <input type=\"text\" class=\"form-control global_filter\" id=\"global_filter\" placeholder=\"Search..\" required>\n                          <button type=\"submit\" class=\"btn btn-icon\"><i class=\"ik ik-search\"></i></button>\n                          <button type=\"button\" id=\"adv_wrap_toggler\" class=\"adv-btn ik ik-chevron-down dropdown-toggle\" data-toggle=\"dropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"></button>\n                          <div class=\"adv-search-wrap dropdown-menu dropdown-menu-right\" aria-labelledby=\"adv_wrap_toggler\">\n                              <div class=\"row\">\n                                  <div class=\"col-md-12\">\n                                      <div class=\"form-group\">\n                                          <input type=\"text\" class=\"form-control column_filter\" id=\"col0_filter\" placeholder=\"Name\" data-column=\"0\">\n                                      </div>\n                                  </div>\n                                  <div class=\"col-md-6\">\n                                      <div class=\"form-group\">\n                                          <input type=\"text\" class=\"form-control column_filter\" id=\"col1_filter\" placeholder=\"Position\" data-column=\"1\">\n                                      </div>\n                                  </div>\n                                  <div class=\"col-md-6\">\n                                      <div class=\"form-group\">\n                                          <input type=\"text\" class=\"form-control column_filter\" id=\"col2_filter\" placeholder=\"Office\" data-column=\"2\">\n                                      </div>\n                                  </div>\n                                  <div class=\"col-md-4\">\n                                      <div class=\"form-group\">\n                                          <input type=\"text\" class=\"form-control column_filter\" id=\"col3_filter\" placeholder=\"Age\" data-column=\"3\">\n                                      </div>\n                                  </div>\n                                  <div class=\"col-md-4\">\n                                      <div class=\"form-group\">\n                                          <input type=\"text\" class=\"form-control column_filter\" id=\"col4_filter\" placeholder=\"Start date\" data-column=\"4\">\n                                      </div>\n                                  </div>\n                                  <div class=\"col-md-4\">\n                                      <div class=\"form-group\">\n                                          <input type=\"text\" class=\"form-control column_filter\" id=\"col5_filter\" placeholder=\"Salary\" data-column=\"5\">\n                                      </div>\n                                  </div>\n                              </div>\n                              <button class=\"btn btn-theme\">Search</button>\n                          </div>\n                      </form>\n                  </div>\n              </div>\n              <div class=\"col col-sm-3\">\n                  <div class=\"card-options text-right\">\n                      <span class=\"mr-5\" id=\"top\">1 - 50 of 2,500</span>\n                      <a href=\"#\"><i class=\"ik ik-chevron-left\"></i></a>\n                      <a href=\"#\"><i class=\"ik ik-chevron-right\"></i></a>\n                  </div>\n              </div>\n          </div>\n          <div class=\"card-body\">\n              <table id=\"advanced_table\" class=\"table\">\n                  <thead>\n                      <tr>\n                          <th class=\"nosort\" width=\"10\">\n                              <label class=\"custom-control custom-checkbox m-0\">\n                                  <input type=\"checkbox\" class=\"custom-control-input\" id=\"selectall\" name=\"\" value=\"option2\">\n                                  <span class=\"custom-control-label\">&nbsp;</span>\n                              </label>\n                          </th>\n                          <th class=\"nosort\">Avatar</th>\n                          <th>Name</th>\n                          <th>Position</th>\n                          <th>Office</th>\n                          <th>Age</th>\n                          <th>Start date</th>\n                          <th>Salary</th>\n                      </tr>\n                  </thead>\n                  <tbody>\n                      <tr>\n                          <td>\n                              <label class=\"custom-control custom-checkbox\">\n                                  <input type=\"checkbox\" class=\"custom-control-input select_all_child\" id=\"\" name=\"\" value=\"option2\">\n                                  <span class=\"custom-control-label\">&nbsp;</span>\n                              </label>\n                          </td>\n                          <td><img src=\"assets/img/users/1.jpg\" class=\"table-user-thumb\" alt=\"\"></td>\n                          <td>Tiger Nixon</td>\n                          <td>System Architect</td>\n                          <td>Edinburgh</td>\n                          <td>61</td>\n                          <td>2011/04/25</td>\n                          <td>$320,800</td>\n                      </tr>\n                      <tr>\n                          <td>\n                              <label class=\"custom-control custom-checkbox\">\n                                  <input type=\"checkbox\" class=\"custom-control-input select_all_child\" id=\"\" name=\"\" value=\"option2\">\n                                  <span class=\"custom-control-label\">&nbsp;</span>\n                              </label>\n                          </td>\n                          <td><img src=\"assets/img/users/2.jpg\" class=\"table-user-thumb\" alt=\"\"></td>\n                          <td>Garrett Winters</td>\n                          <td>Accountant</td>\n                          <td>Tokyo</td>\n                          <td>63</td>\n                          <td>2011/07/25</td>\n                          <td>$170,750</td>\n                      </tr>\n                      <tr>\n                          <td>\n                              <label class=\"custom-control custom-checkbox\">\n                                  <input type=\"checkbox\" class=\"custom-control-input select_all_child\" id=\"\" name=\"\" value=\"option2\">\n                                  <span class=\"custom-control-label\">&nbsp;</span>\n                              </label>\n                          </td>\n                          <td><img src=\"assets/img/users/3.jpg\" class=\"table-user-thumb\" alt=\"\"></td>\n                          <td>Ashton Cox</td>\n                          <td>Junior Technical Author</td>\n                          <td>San Francisco</td>\n                          <td>66</td>\n                          <td>2009/01/12</td>\n                          <td>$86,000</td>\n                      </tr>\n                      <tr>\n                          <td>\n                              <label class=\"custom-control custom-checkbox\">\n                                  <input type=\"checkbox\" class=\"custom-control-input select_all_child\" id=\"\" name=\"\" value=\"option2\">\n                                  <span class=\"custom-control-label\">&nbsp;</span>\n                              </label>\n                          </td>\n                          <td><img src=\"assets/img/users/4.jpg\" class=\"table-user-thumb\" alt=\"\"></td>\n                          <td>Cedric Kelly</td>\n                          <td>Senior Javascript Developer</td>\n                          <td>Edinburgh</td>\n                          <td>22</td>\n                          <td>2012/03/29</td>\n                          <td>$433,060</td>\n                      </tr>\n                      <tr>\n                          <td>\n                              <label class=\"custom-control custom-checkbox\">\n                                  <input type=\"checkbox\" class=\"custom-control-input select_all_child\" id=\"\" name=\"\" value=\"option2\">\n                                  <span class=\"custom-control-label\">&nbsp;</span>\n                              </label>\n                          </td>\n                          <td><img src=\"assets/img/users/5.jpg\" class=\"table-user-thumb\" alt=\"\"></td>\n                          <td>Airi Satou</td>\n                          <td>Accountant</td>\n                          <td>Tokyo</td>\n                          <td>33</td>\n                          <td>2008/11/28</td>\n                          <td>$162,700</td>\n                      </tr>\n                      <tr>\n                          <td>\n                              <label class=\"custom-control custom-checkbox\">\n                                  <input type=\"checkbox\" class=\"custom-control-input select_all_child\" id=\"\" name=\"\" value=\"option2\">\n                                  <span class=\"custom-control-label\">&nbsp;</span>\n                              </label>\n                          </td>\n                          <td><img src=\"assets/img/users/1.jpg\" class=\"table-user-thumb\" alt=\"\"></td>\n                          <td>Brielle Williamson</td>\n                          <td>Integration Specialist</td>\n                          <td>New York</td>\n                          <td>61</td>\n                          <td>2012/12/02</td>\n                          <td>$372,000</td>\n                      </tr>\n                      <tr>\n                          <td>\n                              <label class=\"custom-control custom-checkbox\">\n                                  <input type=\"checkbox\" class=\"custom-control-input select_all_child\" id=\"\" name=\"\" value=\"option2\">\n                                  <span class=\"custom-control-label\">&nbsp;</span>\n                              </label>\n                          </td>\n                          <td><img src=\"assets/img/users/2.jpg\" class=\"table-user-thumb\" alt=\"\"></td>\n                          <td>Herrod Chandler</td>\n                          <td>Sales Assistant</td>\n                          <td>San Francisco</td>\n                          <td>59</td>\n                          <td>2012/08/06</td>\n                          <td>$137,500</td>\n                      </tr>\n                      <tr>\n                          <td>\n                              <label class=\"custom-control custom-checkbox\">\n                                  <input type=\"checkbox\" class=\"custom-control-input select_all_child\" id=\"\" name=\"\" value=\"option2\">\n                                  <span class=\"custom-control-label\">&nbsp;</span>\n                              </label>\n                          </td>\n                          <td><img src=\"assets/img/users/3.jpg\" class=\"table-user-thumb\" alt=\"\"></td>\n                          <td>Rhona Davidson</td>\n                          <td>Integration Specialist</td>\n                          <td>Tokyo</td>\n                          <td>55</td>\n                          <td>2010/10/14</td>\n                          <td>$327,900</td>\n                      </tr>\n                      <tr>\n                          <td>\n                              <label class=\"custom-control custom-checkbox\">\n                                  <input type=\"checkbox\" class=\"custom-control-input select_all_child\" id=\"\" name=\"\" value=\"option2\">\n                                  <span class=\"custom-control-label\">&nbsp;</span>\n                              </label>\n                          </td>\n                          <td><img src=\"assets/img/users/4.jpg\" class=\"table-user-thumb\" alt=\"\"></td>\n                          <td>Colleen Hurst</td>\n                          <td>Javascript Developer</td>\n                          <td>San Francisco</td>\n                          <td>39</td>\n                          <td>2009/09/15</td>\n                          <td>$205,500</td>\n                      </tr>\n                  </tbody>\n              </table>\n          </div>\n      </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/home/home.component.scss":
/*!************************************************!*\
  !*** ./src/app/admin/home/home.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".img-fluid {\n  width: 200px;\n  height: 200px;\n  -o-object-fit: cover;\n     object-fit: cover;\n  border-radius: 6px;\n  border: 1px solid #fff; }\n\n.bg-image-full {\n  background: no-repeat center center scroll;\n  background-size: cover;\n  -o-background-size: cover; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uZ3V5ZW5xdWFuZy9Eb2N1bWVudHMvUUFMQVRUL2Nob3BwZWQvc3JjL2FwcC9hZG1pbi9ob21lL2hvbWUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFZO0VBQ1osY0FBYTtFQUNiLHFCQUFpQjtLQUFqQixrQkFBaUI7RUFDakIsbUJBQWtCO0VBQ2xCLHVCQUFzQixFQUN6Qjs7QUFDRDtFQUNJLDJDQUEwQztFQUcxQyx1QkFBc0I7RUFDdEIsMEJBQXlCLEVBQzFCIiwiZmlsZSI6InNyYy9hcHAvYWRtaW4vaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmltZy1mbHVpZHtcbiAgICB3aWR0aDogMjAwcHg7XG4gICAgaGVpZ2h0OiAyMDBweDtcbiAgICBvYmplY3QtZml0OiBjb3ZlcjtcbiAgICBib3JkZXItcmFkaXVzOiA2cHg7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ZmZjtcbn1cbi5iZy1pbWFnZS1mdWxsIHtcbiAgICBiYWNrZ3JvdW5kOiBuby1yZXBlYXQgY2VudGVyIGNlbnRlciBzY3JvbGw7XG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIC1vLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIH1cbiAgIl19 */"

/***/ }),

/***/ "./src/app/admin/home/home.component.ts":
/*!**********************************************!*\
  !*** ./src/app/admin/home/home.component.ts ***!
  \**********************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/x/http/toast-notification.service */ "./src/x/http/toast-notification.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeComponent = /** @class */ (function () {
    function HomeComponent(apiService, toastService) {
        this.apiService = apiService;
        this.toastService = toastService;
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/admin/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/admin/home/home.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__["ToastNotificationService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/admin/material-category/material-category.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/admin/material-category/material-category.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n      <div class=\"page-header\">\n          <div class=\"row align-items-end\">\n              <div class=\"col-lg-8\">\n                  <div class=\"page-header-title\">\n                      <i class=\"ik ik-inbox bg-blue\"></i>\n                      <div class=\"d-inline\">\n                          <h5>Data Table</h5>\n                          <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"col-lg-4\">\n                  <nav class=\"breadcrumb-container\" aria-label=\"breadcrumb\">\n                      <ol class=\"breadcrumb\">\n                          <li class=\"breadcrumb-item\">\n                              <a href=\"../index.html\"><i class=\"ik ik-home\"></i></a>\n                          </li>\n                          <li class=\"breadcrumb-item\">\n                              <a href=\"#\">Tables</a>\n                          </li>\n                          <li class=\"breadcrumb-item active\" aria-current=\"page\">Data Table</li>\n                      </ol>\n                  </nav>\n              </div>\n          </div>\n      </div>\n\n\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <div class=\"card\">\n                  <div class=\"card-header d-block\">\n                      <h3 style=\"float: left;\">Quản lý Danh mục nguyên liệu</h3>\n                      <div class=\"input-group\" style=\"margin-bottom: 0 !important; float: right; width: unset;\">\n                          <button class=\"btn btn-info buttonAddNew\" type=\"submit\" style=\"height: 35px;\" (click)= \"openModalAdd()\">Create</button>\n                      </div>\n                  </div>\n                  <div class=\"card-body\">\n                      <table class=\"table\"> <!-- data_table -->\n                          <thead>\n                              <tr>\n                                  <th>#</th>\n                                  <th>Tên danh mục</th>\n                                  <th>Nguyên liệu</th>\n                                  <th class=\"nosort\">Thao tác</th>\n                              </tr>\n                          </thead>\n                          <tbody>\n                              <tr *ngFor=\"let item of category_response\">\n                                <td>{{item.id}}</td>\n                                <td>{{item.name}}</td>\n                                <td>\n                                    <a routerLink=\"/admin/category/{{item.id}}/material\">go to material</a>\n                                </td>\n                                <td>\n                                    <div class=\"table-actions\" style=\"text-align: left;\">\n                                        <a (click)=\"openModalEdit(item)\"><i class=\"ik ik-eye\"></i></a>\n                                        <a (click)=\"onDeleteModel(item.id)\"><i class=\"ik ik-trash-2\"></i></a>\n                                    </div>\n                                </td>\n                              </tr>\n                          </tbody>\n                      </table>\n                  </div>\n              </div>\n          </div>\n      </div>\n  </div>\n</div>\n\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"modalAddEdit\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n          <div class=\"modal-header\">\n              <h5 class=\"modal-title\" id=\"modalTitle\"></h5>\n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                  <span aria-hidden=\"true\">&times;</span>\n              </button>\n          </div>\n          <div class=\"card-body\">\n              <form class=\"forms-sample\" (ngSubmit)=\"onSubmit(f,image)\" #f=\"ngForm\">\n                <div class=\"form-group\">\n                    <label for=\"name\">Tên danh mục</label>\n                    <input type=\"text\" class=\"form-control\" id=\"name\" name=\"name\" placeholder=\"Tên danh mục\" ngModel>\n                </div>\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-secondary btnDelete\" data-dismiss=\"modal\">Delete</button>\n                    <button type=\"submit\" class=\"btn btn-primary\" id=\"submit\">Save changes</button>\n                </div>\n              </form>\n          </div>\n          \n      </div>\n  </div>\n</div>\n\n<!-- Modal Delete -->\n<div id=\"modalDelete\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog modal-dialog-centered\">\n      <div class=\"modal-content\" style=\"padding: 30px;\">\n          <input type=\"hidden\" id=\"id-delete\" name=\"id-delete\" />\n          <h2 class=\"h2-waring text-center\">Are You Sure?</h2>\n          <p class=\"lead text-muted text-center\" style=\"display: block;\">You\n              will not restore this data!</p>\n          <div class=\"sa-button-container\" style=\"text-align: center\">\n            <button class=\"confirm btn-lg btn-info\" data-dismiss=\"modal\" id=\"delete\" style=\"margin-right: 10px\" (click)=\"destroy()\">\n                Agree\n            </button>\n              <button class=\"cancel btn-lg btn-default\"\n                      style=\"border: 1px solid #ccc\" data-dismiss=\"modal\">Cancel</button>\n          </div>\n      </div>\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/material-category/material-category.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/admin/material-category/material-category.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL21hdGVyaWFsLWNhdGVnb3J5L21hdGVyaWFsLWNhdGVnb3J5LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/material-category/material-category.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/admin/material-category/material-category.component.ts ***!
  \************************************************************************/
/*! exports provided: MaterialCategoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialCategoryComponent", function() { return MaterialCategoryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/x/http/toast-notification.service */ "./src/x/http/toast-notification.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/shared/shared.service */ "./src/app/common/shared/shared.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MaterialCategoryComponent = /** @class */ (function () {
    function MaterialCategoryComponent(api, toast, router, shared) {
        this.api = api;
        this.toast = toast;
        this.router = router;
        this.shared = shared;
        this.category_response = [];
    }
    MaterialCategoryComponent.prototype.ngOnInit = function () {
        this.getData();
    };
    MaterialCategoryComponent.prototype.openModalAdd = function () {
        this.modalAction = "add";
        $('#name').val("");
        $("#modalAddEdit").modal('show');
    };
    MaterialCategoryComponent.prototype.openModalEdit = function (item) {
        this.modalAction = "edit";
        this.idCategory = item.id;
        $('#name').val(item.name);
        $("#modalAddEdit").modal('show');
    };
    MaterialCategoryComponent.prototype.onDeleteModel = function (idModel) {
        $('#modalDelete').modal('show');
        this.idCategory = idModel;
    };
    MaterialCategoryComponent.prototype.onSubmit = function (f, image) {
        if ($('#name').val().trim() == "") {
            this.toast.error("Không được để trống trường truyền lên!");
            return;
        }
        switch (this.modalAction) {
            case "add":
                this.create(f);
                break;
            case "edit":
                this.update(f);
                break;
            default:
            // code block
        }
    };
    MaterialCategoryComponent.prototype.create = function (f) {
        var _this = this;
        this.api.onAddCategory({ name: f.value.name }).subscribe(function (res) {
            if (res.code_status == 200) {
                _this.getData();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    MaterialCategoryComponent.prototype.update = function (f) {
        var _this = this;
        var body = {
            id: this.idCategory,
            name: f.value.name
        };
        this.api.onUpdateCategory(body).subscribe(function (res) {
            if (res.code_status == 200) {
                _this.getData();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    MaterialCategoryComponent.prototype.destroy = function () {
        var _this = this;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]();
        params = params.append('id', JSON.stringify(this.idCategory));
        this.api.onDeleteCategory(params).subscribe(function (res) {
            _this.getData();
            _this.toast.success(res['message']);
            $('#modalDelete').modal('hide');
        });
    };
    MaterialCategoryComponent.prototype.getData = function () {
        var _this = this;
        this.api.onGetAllCategory().subscribe(function (res) {
            _this.category_response = res.data;
        });
    };
    MaterialCategoryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-material-category',
            template: __webpack_require__(/*! ./material-category.component.html */ "./src/app/admin/material-category/material-category.component.html"),
            styles: [__webpack_require__(/*! ./material-category.component.scss */ "./src/app/admin/material-category/material-category.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__["ToastNotificationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__["SharedService"]])
    ], MaterialCategoryComponent);
    return MaterialCategoryComponent;
}());



/***/ }),

/***/ "./src/app/admin/material-dish/material-dish.component.html":
/*!******************************************************************!*\
  !*** ./src/app/admin/material-dish/material-dish.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n      <div class=\"page-header\">\n          <div class=\"row align-items-end\">\n              <div class=\"col-lg-8\">\n                  <div class=\"page-header-title\">\n                      <i class=\"ik ik-inbox bg-blue\"></i>\n                      <div class=\"d-inline\">\n                          <h5>Data Table</h5>\n                          <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"col-lg-4\">\n                  <nav class=\"breadcrumb-container\" aria-label=\"breadcrumb\">\n                      <ol class=\"breadcrumb\">\n                          <li class=\"breadcrumb-item\">\n                              <a href=\"../index.html\"><i class=\"ik ik-home\"></i></a>\n                          </li>\n                          <li class=\"breadcrumb-item\">\n                              <a href=\"#\">Tables</a>\n                          </li>\n                          <li class=\"breadcrumb-item active\" aria-current=\"page\">Data Table</li>\n                      </ol>\n                  </nav>\n              </div>\n          </div>\n      </div>\n\n      <div class=\"row\">\n        <div class=\"col-lg-12\">\n            <nav aria-label=\"breadcrumb\">\n                <ol class=\"breadcrumb\">\n                  <li class=\"breadcrumb-item active\">Nguyên liệu</li>\n                  <li class=\"breadcrumb-item\" aria-current=\"page\" (click)=\"goToSpice()\">Gia vị</li>\n                </ol>\n            </nav>\n        </div>\n      </div>\n\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <div class=\"card\">\n                  <div class=\"card-header d-block\">\n                      <h3 style=\"float: left;\">Quản lý nguyên liệu</h3>\n                      <div class=\"input-group\" style=\"margin-bottom: 0 !important; float: right; width: unset;\">\n                          <button class=\"btn btn-info buttonAddNew\" type=\"submit\" style=\"height: 35px;\" (click)= \"openModalAdd()\">Create</button>\n                      </div>\n                  </div>\n                  <div class=\"card-body\">\n                      <table class=\"table\"> <!-- data_table -->\n                          <thead>\n                              <tr>\n                                  <th>#</th>\n                                  <th>Nguyên liệu</th>\n                                  <th>Giá mua</th>\n                                  <th class=\"nosort\">Ảnh</th>\n                                  <th class=\"nosort\">Thao tác</th>\n                              </tr>\n                          </thead>\n                          <tbody>\n                              <tr *ngFor=\"let item of material_response\">\n                                  <td>{{item.id}}</td>\n                                  <td>{{item.name}}</td>\n                                  <td>{{item.purchase_price}}</td>\n                                  <td><img src=\"{{item.image}}\" class=\"table-user-thumb text-left\" alt=\"{{item.name}}\"></td>\n                                  <td>\n                                      <div class=\"table-actions\" style=\"text-align: left;\">\n                                          <a (click)=\"onDeleteModel(item.id)\"><i class=\"ik ik-trash-2\"></i></a>\n                                      </div>\n                                  </td>\n                              </tr>\n                          </tbody>\n                      </table>\n                  </div>\n              </div>\n          </div>\n      </div>\n  </div>\n</div>\n\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"modalAddEdit\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\" style=\"max-width: 650px;\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\" id=\"modalTitle\"></h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                    <span aria-hidden=\"true\">&times;</span>\n                </button>\n            </div>\n            <div class=\"card-body\">\n                <form class=\"forms-sample\" (ngSubmit)=\"onSubmit(f,image)\" #f=\"ngForm\">\n                    <ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">\n                        <li class=\"nav-item\" *ngFor=\"let item of materialCustom\">\n                        <a class=\"nav-link\"\n                            [class.active]=\"item.name == checkFirstArray\"\n                            id=\"home-tab\" \n                            data-toggle=\"tab\" \n                            href=\"#home{{item.id}}\" \n                            role=\"tab\" \n                            aria-controls=\"home\" \n                            aria-selected=\"true\">{{item.name}}\n                            </a>\n                        </li>\n                    </ul>\n\n                    <div class=\"tab-content\" id=\"myTabContent\">\n                        <div class=\"tab-pane fade show\" \n                            [class.active]=\"item1.name == checkFirstArray\"\n                            id=\"home{{item1.id}}\" \n                            role=\"tabpanel\" \n                            aria-labelledby=\"home-tab\" *ngFor=\"let item1 of materialCustom\">\n                            \n                            <input type=\"text\" \n                                id=\"mySearch{{item1.id}}\" \n                                (keyup)=\"FuncSearch(item1.id)\" \n                                placeholder=\"Search..\" \n                                title=\"Type in a category\"\n                                class=\"mySearch\"\n                            >\n                            <table class=\"table table-bordered\" id=\"myMenu{{item1.id}}\">\n                                <tr>\n                                    <th scope=\"col\">#</th>\n                                    <th scope=\"col\">Image</th>\n                                    <th scope=\"col\">Name</th>\n                                    <th scope=\"col\">Count</th>\n                                </tr>\n                                <tr *ngFor=\"let item of item1.data\" \n                                (click)=\"action(item.id)\" \n                                [class.demo]=\"item.status == 1\"\n                                [class.demo]=\"item.status == 2\"\n                                >\n                                    <td scope=\"row\">{{item.id}}</td>\n                                    <td><img _ngcontent-c5=\"\" class=\"table-user-thumb text-left\" src=\"{{item.image}}\" alt=\"{{item.name}}\"></td>\n                                    <td><a >{{item.name}}</a></td>\n                                    <td style=\"width: 100px\"><input type=\"number\" class=\"form-control\" value=\"1\" id=\"count{{item.id}}\"></td>\n                                </tr>\n                            </table>                            \n                        </div>\n                    </div>\n\n                </form>\n            </div>\n\n            <div class=\"modal-footer\">\n                <button type=\"submit\" class=\"btn btn-primary\" id=\"submit\" (click)=\"save()\">Save changes</button>\n            </div>\n            \n        </div>\n    </div>\n</div>\n\n<!-- Modal Delete -->\n<div id=\"modalDelete\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog modal-dialog-centered\">\n      <div class=\"modal-content\" style=\"padding: 30px;\">\n          <input type=\"hidden\" id=\"id-delete\" name=\"id-delete\" />\n          <h2 class=\"h2-waring text-center\">Are You Sure?</h2>\n          <p class=\"lead text-muted text-center\" style=\"display: block;\">You\n              will not restore this data!</p>\n          <div class=\"sa-button-container\" style=\"text-align: center\">\n            <button class=\"confirm btn-lg btn-info\" data-dismiss=\"modal\" id=\"delete\" style=\"margin-right: 10px\" (click)=\"destroy()\">\n                Agree\n            </button>\n              <button class=\"cancel btn-lg btn-default\"\n                      style=\"border: 1px solid #ccc\" data-dismiss=\"modal\">Cancel</button>\n          </div>\n      </div>\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/material-dish/material-dish.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/admin/material-dish/material-dish.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mySearch {\n  width: 100%;\n  font-size: 18px;\n  padding: 11px;\n  border: 1px solid #ddd;\n  margin-top: 16px;\n  margin-bottom: 10px;\n  border-radius: 10px; }\n\n/* Style the navigation menu */\n\n.myMenu {\n  list-style-type: none;\n  padding: 0;\n  margin: 0;\n  height: 300px;\n  overflow: auto; }\n\n/* Style the navigation links */\n\n.myMenu li {\n  padding: 12px;\n  text-decoration: none;\n  color: black;\n  display: block;\n  border-top: 2px solid #ddd;\n  border-left: 2px solid #ddd;\n  border-right: 2px solid #ddd;\n  border-bottom: 2px solid #ddd;\n  margin-bottom: 2px;\n  cursor: pointer; }\n\n.myMenu li a {\n  padding-left: 30px;\n  text-decoration: none;\n  color: black; }\n\n.demo {\n  background-color: #ddd; }\n\n.selected {\n  background-color: #eee; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uZ3V5ZW5xdWFuZy9Eb2N1bWVudHMvUUFMQVRUL2Nob3BwZWQvc3JjL2FwcC9hZG1pbi9tYXRlcmlhbC1kaXNoL21hdGVyaWFsLWRpc2guY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFXO0VBQ1gsZ0JBQWU7RUFDZixjQUFhO0VBQ2IsdUJBQXNCO0VBQ3RCLGlCQUFnQjtFQUNoQixvQkFBbUI7RUFDbkIsb0JBQW1CLEVBQ3RCOztBQUVELCtCQUErQjs7QUFDL0I7RUFDSSxzQkFBcUI7RUFDckIsV0FBVTtFQUNWLFVBQVM7RUFDVCxjQUFhO0VBQ2IsZUFDSixFQUFDOztBQUVELGdDQUFnQzs7QUFDaEM7RUFDSSxjQUFhO0VBQ2Isc0JBQXFCO0VBQ3JCLGFBQVk7RUFDWixlQUFjO0VBRWQsMkJBQTBCO0VBQzFCLDRCQUEyQjtFQUMzQiw2QkFBNEI7RUFDNUIsOEJBQTZCO0VBQzdCLG1CQUFrQjtFQUNsQixnQkFBZSxFQUNsQjs7QUFFRDtFQUNJLG1CQUFrQjtFQUNsQixzQkFBcUI7RUFDckIsYUFBWSxFQUNmOztBQVNEO0VBQ0ksdUJBQXNCLEVBQ3pCOztBQUNEO0VBQ0ksdUJBQXNCLEVBQ3pCIiwiZmlsZSI6InNyYy9hcHAvYWRtaW4vbWF0ZXJpYWwtZGlzaC9tYXRlcmlhbC1kaXNoLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm15U2VhcmNoIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgcGFkZGluZzogMTFweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZGRkO1xuICAgIG1hcmdpbi10b3A6IDE2cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuICBcbi8qIFN0eWxlIHRoZSBuYXZpZ2F0aW9uIG1lbnUgKi9cbi5teU1lbnUge1xuICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgICBwYWRkaW5nOiAwO1xuICAgIG1hcmdpbjogMDtcbiAgICBoZWlnaHQ6IDMwMHB4O1xuICAgIG92ZXJmbG93OiBhdXRvXG59XG4gIFxuLyogU3R5bGUgdGhlIG5hdmlnYXRpb24gbGlua3MgKi9cbi5teU1lbnUgbGkgIHtcbiAgICBwYWRkaW5nOiAxMnB4O1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBjb2xvcjogYmxhY2s7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgLy8gYm9yZGVyOiAycHggc29saWQgI2RkZDtcbiAgICBib3JkZXItdG9wOiAycHggc29saWQgI2RkZDtcbiAgICBib3JkZXItbGVmdDogMnB4IHNvbGlkICNkZGQ7XG4gICAgYm9yZGVyLXJpZ2h0OiAycHggc29saWQgI2RkZDtcbiAgICBib3JkZXItYm90dG9tOiAycHggc29saWQgI2RkZDtcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4ubXlNZW51IGxpIGEge1xuICAgIHBhZGRpbmctbGVmdDogMzBweDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgY29sb3I6IGJsYWNrO1xufVxuXG4vLyAubXlNZW51IGxpOmxhc3QtY2hpbGQge1xuLy8gICAgIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjZGRkO1xuLy8gfVxuICBcbi8vIC5teU1lbnUgbGk6aG92ZXIge1xuLy8gICAgIGJhY2tncm91bmQtY29sb3I6ICNkZGQ7XG4vLyB9XG4uZGVtb3tcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGRkO1xufVxuLnNlbGVjdGVkIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/admin/material-dish/material-dish.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/admin/material-dish/material-dish.component.ts ***!
  \****************************************************************/
/*! exports provided: MaterialDishComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialDishComponent", function() { return MaterialDishComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/x/http/toast-notification.service */ "./src/x/http/toast-notification.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/shared/shared.service */ "./src/app/common/shared/shared.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MaterialDishComponent = /** @class */ (function () {
    function MaterialDishComponent(api, toast, router, shared, route) {
        this.api = api;
        this.toast = toast;
        this.router = router;
        this.shared = shared;
        this.route = route;
        this.material_response = [];
        this.category_response = [];
        this.dish_response = [];
        this.materialCustom = [];
        this.material = [];
        this.selectedArray = [];
        this.unit = "0";
        this.imageUrl = "assets/img/no-img.jpg";
        this.checkFirstArray = "";
        this.translateEmpty = { id: null, name: '', data: [] };
    }
    MaterialDishComponent.prototype.ngOnInit = function () {
        this.loadAllMaterial();
        this.loadAllMaterialCategory();
    };
    MaterialDishComponent.prototype.openModalAdd = function () {
        var _this = this;
        this.materialCustom.forEach(function (element) {
            element.data.forEach(function (element1) {
                element1.status = 0;
                _this.material_response.forEach(function (item) {
                    if (item.id == element1.id) {
                        element1.status = 2;
                    }
                });
            });
        });
        this.modalAction = "add";
        $("#modalAddEdit").modal('show');
    };
    MaterialDishComponent.prototype.save = function () {
        var _this = this;
        var materialsAnswer = [];
        this.materialCustom.forEach(function (element) {
            element.data.forEach(function (element1) {
                if (element1.status == 1) {
                    var amountValue = "#count" + element1.id;
                    materialsAnswer.push({ material_id: element1.id, amount: $(amountValue).val() });
                }
            });
        });
        var body = {
            dish_id: this.route.snapshot.paramMap.get('id'),
            materials: materialsAnswer,
        };
        console.log(body);
        this.api.onAdd(body).subscribe(function (res) {
            if (res.code_status == 200) {
                _this.loadAllMaterial();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    MaterialDishComponent.prototype.loadAllMaterial = function () {
        var _this = this;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]();
        params = params.append('id', this.route.snapshot.paramMap.get('id'));
        this.api.onGetAllMaterialsDish(params).subscribe(function (res) {
            _this.material_response = res.data.materials;
        });
    };
    MaterialDishComponent.prototype.loadAllMaterialCategory = function () {
        var _this = this;
        this.materialCustom = [];
        this.api.onGetAllCategory().subscribe(function (res) {
            _this.category_response = res.data;
            _this.checkFirstArray = _this.category_response[0].name;
            _this.category_response.forEach(function (element) {
                var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]();
                params = params.append('material_category_id', element.id.toString());
                _this.api.onGetAllMaterial(params).subscribe(function (res) {
                    _this.material = res.data;
                    var dataCustom = [];
                    _this.material.forEach(function (element1) {
                        dataCustom.push({ id: element1.id, name: element1.name, image: element1.image, status: 0 });
                    });
                    _this.materialCustom.push({ id: element.id, name: element.name, data: dataCustom });
                });
            });
        });
    };
    MaterialDishComponent.prototype.action = function (value) {
        this.materialCustom.forEach(function (element) {
            element.data.forEach(function (element1) {
                if (element1.id == value) {
                    if (element1.status == 2) {
                        console.log("Đã chọn");
                    }
                    else {
                        var statusValue = element1.status == 1 ? 0 : 1;
                        element1.status = statusValue;
                    }
                }
            });
        });
    };
    MaterialDishComponent.prototype.onDeleteModel = function (idModel) {
        $('#modalDelete').modal('show');
        this.Category_id = idModel;
    };
    MaterialDishComponent.prototype.destroy = function () {
        var _this = this;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]();
        params = params.append('material_id', this.Category_id);
        params = params.append('dish_id', this.route.snapshot.paramMap.get('id'));
        this.api.onDelete(params).subscribe(function (res) {
            _this.loadAllMaterial();
            _this.toast.success(res['message']);
            $('#modalDelete').modal('hide');
        });
    };
    MaterialDishComponent.prototype.FuncSearch = function (value) {
        var idInput = "mySearch" + value;
        var idUl = "myMenu" + value;
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById(idInput);
        filter = input.value.toUpperCase();
        table = document.getElementById(idUl);
        tr = table.getElementsByTagName("tr");
        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[2];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                }
                else {
                    tr[i].style.display = "none";
                }
            }
        }
    };
    MaterialDishComponent.prototype.goToSpice = function () {
        this.router.navigate(["/admin/dish/", this.route.snapshot.paramMap.get('id'), "spice"]);
    };
    MaterialDishComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-material-dish',
            template: __webpack_require__(/*! ./material-dish.component.html */ "./src/app/admin/material-dish/material-dish.component.html"),
            styles: [__webpack_require__(/*! ./material-dish.component.scss */ "./src/app/admin/material-dish/material-dish.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__["ToastNotificationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__["SharedService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], MaterialDishComponent);
    return MaterialDishComponent;
}());



/***/ }),

/***/ "./src/app/admin/material/material.component.html":
/*!********************************************************!*\
  !*** ./src/app/admin/material/material.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n      <div class=\"page-header\">\n          <div class=\"row align-items-end\">\n              <div class=\"col-lg-8\">\n                  <div class=\"page-header-title\">\n                      <i class=\"ik ik-inbox bg-blue\"></i>\n                      <div class=\"d-inline\">\n                          <h5>Data Table</h5>\n                          <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"col-lg-4\">\n                  <nav class=\"breadcrumb-container\" aria-label=\"breadcrumb\">\n                      <ol class=\"breadcrumb\">\n                          <li class=\"breadcrumb-item\">\n                              <a href=\"../index.html\"><i class=\"ik ik-home\"></i></a>\n                          </li>\n                          <li class=\"breadcrumb-item\">\n                              <a href=\"#\">Tables</a>\n                          </li>\n                          <li class=\"breadcrumb-item active\" aria-current=\"page\">Data Table</li>\n                      </ol>\n                  </nav>\n              </div>\n          </div>\n      </div>\n\n\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <div class=\"card\">\n                  <div class=\"card-header d-block\">\n                      <h3 style=\"float: left;\">Quản lý nguyên liệu</h3>\n                      <div class=\"input-group\" style=\"margin-bottom: 0 !important; float: right; width: unset;\">\n                          <button class=\"btn btn-info buttonAddNew\" type=\"submit\" style=\"height: 35px;\" (click)= \"openModalAdd()\">Create</button>\n                      </div>\n                  </div>\n                  <div class=\"card-body\">\n                      <table class=\"table\"> <!-- data_table -->\n                          <thead>\n                              <tr>\n                                  <th>#</th>\n                                  <th>Nguyên liệu</th>\n                                  <th>Giá mua</th>\n                                  <th class=\"nosort\">Ảnh</th>\n                                  <th class=\"nosort\">Thao tác</th>\n                              </tr>\n                          </thead>\n                          <tbody>\n                              <tr *ngFor=\"let item of material_response\">\n                                  <td>{{item.id}}</td>\n                                  <td>{{item.name}}</td>\n                                  <td>{{item.purchase_price}}</td>\n                                  <td><img src=\"{{item.image}}\" class=\"table-user-thumb text-left\" alt=\"{{item.name}}\"></td>\n                                  <td>\n                                      <div class=\"table-actions\" style=\"text-align: left;\">\n                                          <a (click)=\"openModalEdit(item)\"><i class=\"ik ik-eye\"></i></a>\n                                          <a (click)=\"onDeleteModel(item.id)\"><i class=\"ik ik-trash-2\"></i></a>\n                                      </div>\n                                  </td>\n                              </tr>\n                          </tbody>\n                      </table>\n                  </div>\n              </div>\n          </div>\n      </div>\n  </div>\n</div>\n\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"modalAddEdit\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n          <div class=\"modal-header\">\n              <h5 class=\"modal-title\" id=\"modalTitle\"></h5>\n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                  <span aria-hidden=\"true\">&times;</span>\n              </button>\n          </div>\n          <div class=\"card-body\">\n              <form class=\"forms-sample\" (ngSubmit)=\"onSubmit(f,image)\" #f=\"ngForm\">\n                <div class=\"form-group\">\n                    <label for=\"name\">Tên nguyên liệu</label>\n                    <input \n                        type=\"text\" \n                        class=\"form-control\" \n                        id=\"name\" \n                        name=\"name\" \n                        placeholder=\"Tên nguyên liệu\" ngModel>\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"purchase-price\">Giá mua(VNĐ)</label>\n                    <input \n                        type=\"number\" \n                        class=\"form-control\" \n                        id=\"purchase-price\" \n                        placeholder=\"Giá mua(VNĐ)\" \n                        name=\"purchase_price\" ngModel>\n                </div>\n\n\n                <div class=\"form-group\">\n                    <label for=\"unit\">Đơn vị</label>\n                    <select class=\"form-control\" id=\"unit\" name=\"unit\" [(ngModel)] = \"unit\" >\n                        <option value=\"0\">--Chọn đơn vị--</option>\n                        <option value=\"kg\">kg</option>\n                        <option value=\"gram\">gram</option>\n                    </select>\n                </div>\n\n                <div class=\"form-group\">\n                    <label for=\"image\">Ảnh mô tả</label>\n                    <input type=\"file\" id=\"image\" class=\"form-control\" #image (change)=\"onFileChanged($event)\">\n                    <img [src]=\"imageUrl\" alt=\"no-image\" class=\"no-img\" id=\"preview\">\n                </div>\n\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-secondary btnDelete\" data-dismiss=\"modal\">Delete</button>\n                    <button type=\"submit\" class=\"btn btn-primary\" id=\"submit\">Save changes</button>\n                </div>\n              </form>\n          </div>\n          \n      </div>\n  </div>\n</div>\n\n<!-- Modal Delete -->\n<div id=\"modalDelete\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog modal-dialog-centered\">\n      <div class=\"modal-content\" style=\"padding: 30px;\">\n          <input type=\"hidden\" id=\"id-delete\" name=\"id-delete\" />\n          <h2 class=\"h2-waring text-center\">Are You Sure?</h2>\n          <p class=\"lead text-muted text-center\" style=\"display: block;\">You\n              will not restore this data!</p>\n          <div class=\"sa-button-container\" style=\"text-align: center\">\n            <button class=\"confirm btn-lg btn-info\" data-dismiss=\"modal\" id=\"delete\" style=\"margin-right: 10px\" (click)=\"destroy()\">\n                Agree\n            </button>\n              <button class=\"cancel btn-lg btn-default\"\n                      style=\"border: 1px solid #ccc\" data-dismiss=\"modal\">Cancel</button>\n          </div>\n      </div>\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/material/material.component.scss":
/*!********************************************************!*\
  !*** ./src/app/admin/material/material.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL21hdGVyaWFsL21hdGVyaWFsLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/material/material.component.ts":
/*!******************************************************!*\
  !*** ./src/app/admin/material/material.component.ts ***!
  \******************************************************/
/*! exports provided: MaterialComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialComponent", function() { return MaterialComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/x/http/toast-notification.service */ "./src/x/http/toast-notification.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/shared/shared.service */ "./src/app/common/shared/shared.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MaterialComponent = /** @class */ (function () {
    function MaterialComponent(api, toast, router, shared, route) {
        this.api = api;
        this.toast = toast;
        this.router = router;
        this.shared = shared;
        this.route = route;
        this.material_response = [];
        this.unit = "0";
        this.imageUrl = "assets/img/no-img.jpg";
    }
    MaterialComponent.prototype.ngOnInit = function () {
        this.Category_id = this.route.snapshot.paramMap.get('id');
        this.loadAllMaterial();
    };
    MaterialComponent.prototype.openModalAdd = function () {
        this.modalAction = "add";
        $('#name').val("");
        $('#price').val("");
        $('#purchase-price').val("");
        this.unit = "0";
        $("#modalAddEdit").modal('show');
        $('#image').prop("value", "");
        $("#preview").attr("src", "assets/img/no-img.jpg");
    };
    MaterialComponent.prototype.openModalEdit = function (item) {
        this.modalAction = "edit";
        this.idMaterial = item.id;
        $('#name').val(item.name);
        this.checkName = item.name;
        $('#purchase-price').val(item.purchase_price);
        this.unit = item.unit;
        $("#preview").attr("src", item.image);
        $('#image').prop("value", "");
        $("#modalAddEdit").modal('show');
    };
    /**
     * open model delete Category
     */
    MaterialComponent.prototype.onDeleteModel = function (idModel) {
        $('#modalDelete').modal('show');
        this.idMaterial = idModel;
    };
    MaterialComponent.prototype.onSubmit = function (f, image) {
        if ($('#name').val().trim() == "" || $('#purchase-price').val().trim() == "" || f.value.unit == "0") {
            this.toast.error("Không được để trống trường truyền lên!");
            return;
        }
        switch (this.modalAction) {
            case "add":
                this.create(f, image);
                break;
            case "edit":
                this.update(f, image);
                break;
            default:
            // code block
        }
    };
    MaterialComponent.prototype.create = function (f, image) {
        var _this = this;
        var formData = new FormData();
        if (!image.files[0]) {
            this.toast.error("Không được để trống trường truyền lên!");
            return;
        }
        formData.set('material_category_id', this.Category_id);
        formData.set('image', image.files[0]);
        formData.set('name', f.value.name);
        formData.set('purchase_price', f.value.purchase_price);
        formData.set('unit', f.value.unit);
        this.api.onAddMaterial(formData).subscribe(function (res) {
            if (res.code_status == 200) {
                _this.loadAllMaterial();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    ;
    MaterialComponent.prototype.update = function (f, image) {
        var _this = this;
        var formData = new FormData();
        if (image.files[0]) {
            formData.set('image', image.files[0]);
        }
        if (this.checkName != $('#name').val()) {
            formData.set('name', $('#name').val());
        }
        formData.set('id', JSON.stringify(this.idMaterial));
        formData.set('purchase_price', $('#purchase-price').val());
        formData.set('unit', f.value.unit);
        this.api.onUpdateMaterial(formData).subscribe(function (res) {
            console.log(res);
            if (res.code_status == 200) {
                _this.loadAllMaterial();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    MaterialComponent.prototype.destroy = function () {
        var _this = this;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]();
        params = params.append('id', JSON.stringify(this.idMaterial));
        this.api.onDeleteMaterial(params).subscribe(function (res) {
            _this.loadAllMaterial();
            _this.toast.success(res['message']);
            $('#modalDelete').modal('hide');
        });
    };
    MaterialComponent.prototype.loadAllMaterial = function () {
        var _this = this;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]();
        params = params.append('material_category_id', this.Category_id);
        this.api.onGetAllMaterial(params).subscribe(function (res) {
            _this.material_response = res.data;
            console.log(_this.material_response);
        });
    };
    /**
     * function preview images
     * @param event
     */
    MaterialComponent.prototype.onFileChanged = function (event) {
        this.shared.previewImage(event, 'preview');
    };
    MaterialComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-material',
            template: __webpack_require__(/*! ./material.component.html */ "./src/app/admin/material/material.component.html"),
            styles: [__webpack_require__(/*! ./material.component.scss */ "./src/app/admin/material/material.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__["ToastNotificationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__["SharedService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], MaterialComponent);
    return MaterialComponent;
}());



/***/ }),

/***/ "./src/app/admin/spice-dish/spice-dish.component.html":
/*!************************************************************!*\
  !*** ./src/app/admin/spice-dish/spice-dish.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"page-header\">\n            <div class=\"row align-items-end\">\n                <div class=\"col-lg-8\">\n                    <div class=\"page-header-title\">\n                        <i class=\"ik ik-inbox bg-blue\"></i>\n                        <div class=\"d-inline\">\n                            <h5>Data Table</h5>\n                            <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-lg-4\">\n                    <nav class=\"breadcrumb-container\" aria-label=\"breadcrumb\">\n                        <ol class=\"breadcrumb\">\n                            <li class=\"breadcrumb-item\">\n                                <a href=\"../index.html\"><i class=\"ik ik-home\"></i></a>\n                            </li>\n                            <li class=\"breadcrumb-item\">\n                                <a href=\"#\">Tables</a>\n                            </li>\n                            <li class=\"breadcrumb-item active\" aria-current=\"page\">Data Table</li>\n                        </ol>\n                    </nav>\n                </div>\n            </div>\n        </div>\n  \n        <div class=\"row\">\n          <div class=\"col-lg-12\">\n              <nav aria-label=\"breadcrumb\">\n                  <ol class=\"breadcrumb\">\n                    <li class=\"breadcrumb-item\" (click)=\"goToSpice()\">Nguyên liệu</li>\n                    <li class=\"breadcrumb-item active\" aria-current=\"page\">Gia vị</li>\n                  </ol>\n              </nav>\n          </div>\n        </div>\n  \n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header d-block\">\n                        <h3 style=\"float: left;\">Quản lý nguyên liệu</h3>\n                        <div class=\"input-group\" style=\"margin-bottom: 0 !important; float: right; width: unset;\">\n                            <button class=\"btn btn-info buttonAddNew\" type=\"submit\" style=\"height: 35px;\" (click)= \"openModalAdd()\">Create</button>\n                        </div>\n                    </div>\n                    <div class=\"card-body\">\n                        <table class=\"table\"> <!-- data_table -->\n                            <thead>\n                                <tr>\n                                    <th>#</th>\n                                    <th>Tên gia vị</th>\n                                    <th>Ảnh mô tả</th>\n                                    <th class=\"nosort\">Thao tác</th>\n                                </tr>\n                            </thead>\n                            <tbody>\n                                <tr *ngFor=\"let item of spice_dishresponse\">\n                                    <td>{{item.id}}</td>\n                                    <td>{{item.name}}</td>\n                                    <td><img src=\"{{item.image}}\" class=\"table-user-thumb text-left\" alt=\"{{item.name}}\"></td>\n                                    <td>\n                                        <div class=\"table-actions\" style=\"text-align: left;\">\n                                            <a (click)=\"onDeleteModel(item.id)\"><i class=\"ik ik-trash-2\"></i></a>\n                                        </div>\n                                    </td>\n                                </tr>\n                            </tbody>\n                        </table>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n  </div>\n  \n  \n  <!-- Modal -->\n  <div class=\"modal fade\" id=\"modalAddEdit\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n      <div class=\"modal-dialog modal-dialog-centered\" role=\"document\" style=\"max-width: 650px;\">\n          <div class=\"modal-content\">\n              <div class=\"modal-header\">\n                  <h5 class=\"modal-title\" id=\"modalTitle\"></h5>\n                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                      <span aria-hidden=\"true\">&times;</span>\n                  </button>\n              </div>\n              <div class=\"card-body\">\n                  <form class=\"forms-sample\" (ngSubmit)=\"onSubmit()\" #f=\"ngForm\">\n                    <input type=\"text\" \n                      id=\"mySearch\" \n                      (keyup)=\"FuncSearch()\" \n                      placeholder=\"Search..\" \n                      title=\"Type in a category\"\n                      class=\"mySearch\"\n                    >\n                    <table class=\"table table-bordered\" id=\"myMenu\">\n                      <tr>\n                          <th scope=\"col\">#</th>\n                          <th scope=\"col\">Tên gia vị</th>\n                          <th scope=\"col\">Ảnh mô tả</th>\n                          <th scope=\"col\">Số lượng</th>\n                      </tr>\n                      <tr *ngFor=\"let item of spice_response\" \n                      (click)=\"action(item.id)\" \n                      [class.demo]=\"item.status == 1\"\n                      [class.demo]=\"item.status == 2\"\n                      >\n                          <td scope=\"row\">{{item.id}}</td>\n                          <td><a >{{item.name}}</a></td>\n                          <td><img _ngcontent-c5=\"\" class=\"table-user-thumb text-left\" src=\"{{item.image}}\" alt=\"{{item.name}}\"></td>\n                          <td style=\"width: 100px\"><input type=\"number\" class=\"form-control\" value=\"1\" id=\"count{{item.id}}\"></td>\n                      </tr>\n                    </table>  \n                  </form>\n              </div>\n  \n              <div class=\"modal-footer\">\n                  <button type=\"submit\" class=\"btn btn-primary\" id=\"submit\" (click)=\"save()\">Save changes</button>\n              </div>\n              \n          </div>\n      </div>\n  </div>\n  \n  <!-- Modal Delete -->\n  <div id=\"modalDelete\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog modal-dialog-centered\">\n        <div class=\"modal-content\" style=\"padding: 30px;\">\n            <input type=\"hidden\" id=\"id-delete\" name=\"id-delete\" />\n            <h2 class=\"h2-waring text-center\">Are You Sure?</h2>\n            <p class=\"lead text-muted text-center\" style=\"display: block;\">You\n                will not restore this data!</p>\n            <div class=\"sa-button-container\" style=\"text-align: center\">\n              <button class=\"confirm btn-lg btn-info\" data-dismiss=\"modal\" id=\"delete\" style=\"margin-right: 10px\" (click)=\"destroy()\">\n                  Agree\n              </button>\n                <button class=\"cancel btn-lg btn-default\"\n                        style=\"border: 1px solid #ccc\" data-dismiss=\"modal\">Cancel</button>\n            </div>\n        </div>\n  \n    </div>\n  </div>"

/***/ }),

/***/ "./src/app/admin/spice-dish/spice-dish.component.scss":
/*!************************************************************!*\
  !*** ./src/app/admin/spice-dish/spice-dish.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mySearch {\n  width: 100%;\n  font-size: 18px;\n  padding: 11px;\n  border: 1px solid #ddd;\n  margin-top: 16px;\n  margin-bottom: 10px;\n  border-radius: 10px; }\n\n/* Style the navigation menu */\n\n.myMenu {\n  list-style-type: none;\n  padding: 0;\n  margin: 0;\n  height: 300px;\n  overflow: auto; }\n\n/* Style the navigation links */\n\n.myMenu li {\n  padding: 12px;\n  text-decoration: none;\n  color: black;\n  display: block;\n  border-top: 2px solid #ddd;\n  border-left: 2px solid #ddd;\n  border-right: 2px solid #ddd;\n  border-bottom: 2px solid #ddd;\n  margin-bottom: 2px;\n  cursor: pointer; }\n\n.myMenu li a {\n  padding-left: 30px;\n  text-decoration: none;\n  color: black; }\n\n.demo {\n  background-color: #ddd; }\n\n.selected {\n  background-color: #eee; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9uZ3V5ZW5xdWFuZy9Eb2N1bWVudHMvUUFMQVRUL2Nob3BwZWQvc3JjL2FwcC9hZG1pbi9zcGljZS1kaXNoL3NwaWNlLWRpc2guY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxZQUFXO0VBQ1gsZ0JBQWU7RUFDZixjQUFhO0VBQ2IsdUJBQXNCO0VBQ3RCLGlCQUFnQjtFQUNoQixvQkFBbUI7RUFDbkIsb0JBQW1CLEVBQ3RCOztBQUVELCtCQUErQjs7QUFDL0I7RUFDSSxzQkFBcUI7RUFDckIsV0FBVTtFQUNWLFVBQVM7RUFDVCxjQUFhO0VBQ2IsZUFDSixFQUFDOztBQUVELGdDQUFnQzs7QUFDaEM7RUFDSSxjQUFhO0VBQ2Isc0JBQXFCO0VBQ3JCLGFBQVk7RUFDWixlQUFjO0VBRWQsMkJBQTBCO0VBQzFCLDRCQUEyQjtFQUMzQiw2QkFBNEI7RUFDNUIsOEJBQTZCO0VBQzdCLG1CQUFrQjtFQUNsQixnQkFBZSxFQUNsQjs7QUFFRDtFQUNJLG1CQUFrQjtFQUNsQixzQkFBcUI7RUFDckIsYUFBWSxFQUNmOztBQVNEO0VBQ0ksdUJBQXNCLEVBQ3pCOztBQUNEO0VBQ0ksdUJBQXNCLEVBQ3pCIiwiZmlsZSI6InNyYy9hcHAvYWRtaW4vc3BpY2UtZGlzaC9zcGljZS1kaXNoLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm15U2VhcmNoIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgcGFkZGluZzogMTFweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZGRkO1xuICAgIG1hcmdpbi10b3A6IDE2cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuICBcbi8qIFN0eWxlIHRoZSBuYXZpZ2F0aW9uIG1lbnUgKi9cbi5teU1lbnUge1xuICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgICBwYWRkaW5nOiAwO1xuICAgIG1hcmdpbjogMDtcbiAgICBoZWlnaHQ6IDMwMHB4O1xuICAgIG92ZXJmbG93OiBhdXRvXG59XG4gIFxuLyogU3R5bGUgdGhlIG5hdmlnYXRpb24gbGlua3MgKi9cbi5teU1lbnUgbGkgIHtcbiAgICBwYWRkaW5nOiAxMnB4O1xuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICBjb2xvcjogYmxhY2s7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgLy8gYm9yZGVyOiAycHggc29saWQgI2RkZDtcbiAgICBib3JkZXItdG9wOiAycHggc29saWQgI2RkZDtcbiAgICBib3JkZXItbGVmdDogMnB4IHNvbGlkICNkZGQ7XG4gICAgYm9yZGVyLXJpZ2h0OiAycHggc29saWQgI2RkZDtcbiAgICBib3JkZXItYm90dG9tOiAycHggc29saWQgI2RkZDtcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XG4gICAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4ubXlNZW51IGxpIGEge1xuICAgIHBhZGRpbmctbGVmdDogMzBweDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgY29sb3I6IGJsYWNrO1xufVxuXG4vLyAubXlNZW51IGxpOmxhc3QtY2hpbGQge1xuLy8gICAgIGJvcmRlci1ib3R0b206IDJweCBzb2xpZCAjZGRkO1xuLy8gfVxuICBcbi8vIC5teU1lbnUgbGk6aG92ZXIge1xuLy8gICAgIGJhY2tncm91bmQtY29sb3I6ICNkZGQ7XG4vLyB9XG4uZGVtb3tcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGRkO1xufVxuLnNlbGVjdGVkIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/admin/spice-dish/spice-dish.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/admin/spice-dish/spice-dish.component.ts ***!
  \**********************************************************/
/*! exports provided: SpiceDishComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpiceDishComponent", function() { return SpiceDishComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/x/http/toast-notification.service */ "./src/x/http/toast-notification.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/shared/shared.service */ "./src/app/common/shared/shared.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SpiceDishComponent = /** @class */ (function () {
    function SpiceDishComponent(api, toast, router, shared, route) {
        this.api = api;
        this.toast = toast;
        this.router = router;
        this.shared = shared;
        this.route = route;
        this.spice_response = [];
        this.spice_dishresponse = [];
    }
    SpiceDishComponent.prototype.ngOnInit = function () {
        this.loadSpice();
        this.loadAllSpice();
    };
    SpiceDishComponent.prototype.openModalAdd = function () {
        // this.spice_response.forEach(element => {
        //   this.spice_dishresponse.forEach(element1  => {
        //     if (element.id == element1.id) {
        //       element.status = 2
        //     } 
        //   });
        // });
        $("#modalAddEdit").modal('show');
    };
    SpiceDishComponent.prototype.loadAllSpice = function () {
        var _this = this;
        this.api.onGetAllSpice().subscribe(function (res) {
            _this.spice_response = res.data;
        });
    };
    SpiceDishComponent.prototype.loadSpice = function () {
        var _this = this;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]();
        params = params.append('dish_id', this.route.snapshot.paramMap.get('id'));
        this.api.onGetAllSpiceDish(params).subscribe(function (res) {
            _this.spice_dishresponse = res.data;
        });
    };
    SpiceDishComponent.prototype.action = function (id) {
        this.spice_response.forEach(function (element) {
            if (element.id == id) {
                var statusValue = element.status == 1 ? 0 : 1;
                element.status = statusValue;
                // if (element.status == 2) {
                //   console.log("Đã chọn")
                // } else {
                //   var statusValue = element.status == 1 ? 0 : 1
                //   element.status = statusValue            
                // }
            }
        });
    };
    SpiceDishComponent.prototype.save = function () {
        var _this = this;
        var spiceAnswer = [];
        this.spice_response.forEach(function (element) {
            if (element.status == 1) {
                spiceAnswer.push(element.id);
            }
        });
        var body = {
            "dish_id": this.route.snapshot.paramMap.get('id'),
            "spice_ids": spiceAnswer
        };
        this.api.onAddSpiceDish(body).subscribe(function (res) {
            if (res.code_status == 200) {
                _this.loadSpice();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    SpiceDishComponent.prototype.onDeleteModel = function (idModel) {
        $('#modalDelete').modal('show');
        this.Category_id = idModel;
    };
    SpiceDishComponent.prototype.destroy = function () {
        var _this = this;
        var body = {
            "dish_id": this.route.snapshot.paramMap.get('id'),
            "spice_id": this.Category_id
        };
        this.api.onDeleteSpiceDish(body).subscribe(function (res) {
            if (res.code_status == 200) {
                _this.loadSpice();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    SpiceDishComponent.prototype.goToSpice = function () {
        this.router.navigate(["/admin/dish/", this.route.snapshot.paramMap.get('id'), "material"]);
    };
    SpiceDishComponent.prototype.FuncSearch = function () {
        var idInput = "mySearch";
        var idUl = "myMenu";
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById(idInput);
        filter = input.value.toUpperCase();
        table = document.getElementById(idUl);
        tr = table.getElementsByTagName("tr");
        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[1];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                }
                else {
                    tr[i].style.display = "none";
                }
            }
        }
    };
    SpiceDishComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-spice-dish',
            template: __webpack_require__(/*! ./spice-dish.component.html */ "./src/app/admin/spice-dish/spice-dish.component.html"),
            styles: [__webpack_require__(/*! ./spice-dish.component.scss */ "./src/app/admin/spice-dish/spice-dish.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__["ToastNotificationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__["SharedService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], SpiceDishComponent);
    return SpiceDishComponent;
}());



/***/ }),

/***/ "./src/app/admin/spice/spice.component.html":
/*!**************************************************!*\
  !*** ./src/app/admin/spice/spice.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"page-header\">\n        <div class=\"row align-items-end\">\n            <div class=\"col-lg-8\">\n                <div class=\"page-header-title\">\n                    <i class=\"ik ik-inbox bg-blue\"></i>\n                    <div class=\"d-inline\">\n                        <h5>Data Table</h5>\n                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>\n                    </div>\n                </div>\n            </div>\n            <div class=\"col-lg-4\">\n                <nav class=\"breadcrumb-container\" aria-label=\"breadcrumb\">\n                    <ol class=\"breadcrumb\">\n                        <li class=\"breadcrumb-item\">\n                            <a href=\"../index.html\"><i class=\"ik ik-home\"></i></a>\n                        </li>\n                        <li class=\"breadcrumb-item\">\n                            <a href=\"#\">Tables</a>\n                        </li>\n                        <li class=\"breadcrumb-item active\" aria-current=\"page\">Data Table</li>\n                    </ol>\n                </nav>\n            </div>\n        </div>\n        </div>\n\n\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header d-block\">\n                        <h3 style=\"float: left;\">Quản lý gia vị</h3>\n                        <div class=\"input-group\" style=\"margin-bottom: 0 !important; float: right; width: unset;\">\n                            <button class=\"btn btn-info buttonAddNew\" type=\"submit\" style=\"height: 35px;\" (click)= \"openModalAdd()\">Create</button>\n                        </div>\n                    </div>\n                    <div class=\"card-body\">\n                        <table class=\"table\"> <!-- data_table -->\n                            <thead>\n                                <tr>\n                                    <th>#</th>\n                                    <th>Gia vị</th>\n                                    <th>Giá mua</th>\n                                    <th>Mô tả</th>\n                                    <th class=\"nosort\">Ảnh</th>\n                                    <th class=\"nosort\">Thao tác</th>\n                                </tr>\n                            </thead>\n                            <tbody>\n                                <tr *ngFor=\"let item of spice_response\">\n                                <td>{{item.id}}</td>\n                                <td>{{item.name}}</td>\n                                <td>{{item.purchase_price}}</td>\n                                <td>{{item.description}}</td>\n                                <td><img src=\"{{item.image}}\" class=\"table-user-thumb text-left\" alt=\"{{item.name}}\"></td>\n                                <td>\n                                    <div class=\"table-actions\" style=\"text-align: left;\">\n                                        <a (click)=\"openModalEdit(item)\"><i class=\"ik ik-eye\"></i></a>\n                                        <a (click)=\"onDeleteModel(item.id)\"><i class=\"ik ik-trash-2\"></i></a>\n                                    </div>\n                                </td>\n                                </tr>\n                             </tbody>\n                        </table>\n                    </div>\n                </div>\n            </div>\n        </div>\n   </div>\n</div>\n\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"modalAddEdit\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n    <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n        <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h5 class=\"modal-title\" id=\"modalTitle\"></h5>\n                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                    <span aria-hidden=\"true\">&times;</span>\n                </button>\n            </div>\n            <div class=\"card-body\">\n                <form class=\"forms-sample\" (ngSubmit)=\"onSubmit(f,image)\" #f=\"ngForm\">\n                <div class=\"form-group\">\n                    <label for=\"name\">Tên gia vị</label>\n                    <input \n                        type=\"text\" \n                        class=\"form-control\" \n                        id=\"name\" \n                        name=\"name\" \n                        placeholder=\"Tên nguyên liệu\" ngModel>\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"purchase-price\">Giá mua(VNĐ)</label>\n                    <input \n                        type=\"number\" \n                        class=\"form-control\" \n                        id=\"purchase-price\" \n                        placeholder=\"Giá mua(VNĐ)\" \n                        name=\"purchase_price\" ngModel>\n                </div>\n\n                <div class=\"form-group\">\n                    <label for=\"name\">Mô tả</label>\n                    <input \n                        type=\"text\" \n                        class=\"form-control\" \n                        id=\"description\" \n                        name=\"description\" \n                        placeholder=\"Mô tả\" ngModel>\n                </div>\n\n                <div class=\"form-group\">\n                    <label for=\"image\">Ảnh mô tả</label>\n                    <input type=\"file\" id=\"image\" class=\"form-control\" #image (change)=\"onFileChanged($event)\">\n                    <img [src]=\"imageUrl\" alt=\"no-image\" class=\"no-img\" id=\"preview\">\n                </div>\n\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-secondary btnDelete\" data-dismiss=\"modal\">Delete</button>\n                    <button type=\"submit\" class=\"btn btn-primary\" id=\"submit\">Save changes</button>\n                </div>\n                </form>\n            </div>\n          \n        </div>\n    </div>\n</div>\n\n<!-- Modal Delete -->\n<div id=\"modalDelete\" class=\"modal fade\" role=\"dialog\">\n    <div class=\"modal-dialog modal-dialog-centered\">\n        <div class=\"modal-content\" style=\"padding: 30px;\">\n            <input type=\"hidden\" id=\"id-delete\" name=\"id-delete\" />\n            <h2 class=\"h2-waring text-center\">Are You Sure?</h2>\n            <p class=\"lead text-muted text-center\" style=\"display: block;\">You\n              will not restore this data!</p>\n            <div class=\"sa-button-container\" style=\"text-align: center\">  \n                <button class=\"confirm btn-lg btn-info\" data-dismiss=\"modal\" id=\"delete\" style=\"margin-right: 10px\" (click)=\"destroy()\">\n                    Agree\n                </button>\n                <button class=\"cancel btn-lg btn-default\"\n                      style=\"border: 1px solid #ccc\" data-dismiss=\"modal\">Cancel</button>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/spice/spice.component.scss":
/*!**************************************************!*\
  !*** ./src/app/admin/spice/spice.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3NwaWNlL3NwaWNlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/spice/spice.component.ts":
/*!************************************************!*\
  !*** ./src/app/admin/spice/spice.component.ts ***!
  \************************************************/
/*! exports provided: SpiceComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SpiceComponent", function() { return SpiceComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/x/http/toast-notification.service */ "./src/x/http/toast-notification.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/shared/shared.service */ "./src/app/common/shared/shared.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SpiceComponent = /** @class */ (function () {
    function SpiceComponent(api, toast, router, shared, route) {
        this.api = api;
        this.toast = toast;
        this.router = router;
        this.shared = shared;
        this.route = route;
        this.spice_response = [];
        this.imageUrl = "assets/img/no-img.jpg";
    }
    SpiceComponent.prototype.ngOnInit = function () {
        this.loadAllSpice();
    };
    SpiceComponent.prototype.openModalAdd = function () {
        this.modalAction = "add";
        $('#name').val("");
        $('#purchase-price').val("");
        $('#description').val("");
        $('#image').prop("value", "");
        $("#modalAddEdit").modal('show');
        $("#preview").attr("src", "assets/img/no-img.jpg");
    };
    SpiceComponent.prototype.openModalEdit = function (item) {
        this.modalAction = "edit";
        this.idSpice = item.id;
        $('#name').val(item.name);
        this.checkName = item.name;
        $('#purchase-price').val(item.purchase_price);
        $('#description').val(item.description);
        $("#preview").attr("src", item.image);
        $('#image').prop("value", "");
        $("#modalAddEdit").modal('show');
    };
    /**
     * open model delete Category
     */
    SpiceComponent.prototype.onDeleteModel = function (idModel) {
        $('#modalDelete').modal('show');
        this.idSpice = idModel;
    };
    SpiceComponent.prototype.onSubmit = function (f, image) {
        if ($('#name').val().trim() == "" || $('#purchase-price').val().trim() == "" || $('#description').val().trim() == "") {
            this.toast.error("Không được để trống trường truyền lên!");
            return;
        }
        switch (this.modalAction) {
            case "add":
                this.create(f, image);
                break;
            case "edit":
                this.update(f, image);
                break;
            default:
            // code block
        }
    };
    SpiceComponent.prototype.create = function (f, image) {
        var _this = this;
        var formData = new FormData();
        if (!image.files[0]) {
            this.toast.error("Không được để trống trường truyền lên!");
            return;
        }
        formData.set('image', image.files[0]);
        formData.set('name', f.value.name);
        formData.set('purchase_price', f.value.purchase_price);
        formData.set('description', f.value.description);
        this.api.onAddSpice(formData).subscribe(function (res) {
            if (res.code_status == 200) {
                _this.loadAllSpice();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    ;
    SpiceComponent.prototype.update = function (f, image) {
        var _this = this;
        var formData = new FormData();
        if (image.files[0]) {
            formData.set('image', image.files[0]);
        }
        if (this.checkName != $('#name').val()) {
            formData.set('name', $('#name').val());
        }
        formData.set('id', JSON.stringify(this.idSpice));
        formData.set('purchase_price', $('#purchase-price').val());
        formData.set('description', $("#description").val());
        this.api.onUpdateSpice(formData).subscribe(function (res) {
            console.log(res);
            if (res.code_status == 200) {
                _this.loadAllSpice();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    SpiceComponent.prototype.destroy = function () {
        var _this = this;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]();
        params = params.append('id', JSON.stringify(this.idSpice));
        this.api.onDeleteSpice(params).subscribe(function (res) {
            _this.loadAllSpice();
            _this.toast.success(res['message']);
            $('#modalDelete').modal('hide');
        });
    };
    SpiceComponent.prototype.loadAllSpice = function () {
        var _this = this;
        this.api.onGetAllSpice().subscribe(function (res) {
            _this.spice_response = res.data;
        });
    };
    /**
     * function preview images
     * @param event
     */
    SpiceComponent.prototype.onFileChanged = function (event) {
        this.shared.previewImage(event, 'preview');
    };
    SpiceComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-spice',
            template: __webpack_require__(/*! ./spice.component.html */ "./src/app/admin/spice/spice.component.html"),
            styles: [__webpack_require__(/*! ./spice.component.scss */ "./src/app/admin/spice/spice.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__["ToastNotificationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__["SharedService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], SpiceComponent);
    return SpiceComponent;
}());



/***/ }),

/***/ "./src/app/admin/timeline/timeline.component.html":
/*!********************************************************!*\
  !*** ./src/app/admin/timeline/timeline.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n    <div class=\"container-fluid\">\n        <div class=\"page-header\">\n            <div class=\"row align-items-end\">\n                <div class=\"col-lg-8\">\n                    <div class=\"page-header-title\">\n                        <i class=\"ik ik-inbox bg-blue\"></i>\n                        <div class=\"d-inline\">\n                            <h5>Data Table</h5>\n                            <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-lg-4\">\n                    <nav class=\"breadcrumb-container\" aria-label=\"breadcrumb\">\n                        <ol class=\"breadcrumb\">\n                            <li class=\"breadcrumb-item\">\n                                <a href=\"../index.html\"><i class=\"ik ik-home\"></i></a>\n                            </li>\n                            <li class=\"breadcrumb-item\">\n                                <a href=\"#\">Tables</a>\n                            </li>\n                            <li class=\"breadcrumb-item active\" aria-current=\"page\">Data Table</li>\n                        </ol>\n                    </nav>\n              </div>\n            </div>\n        </div>\n\n\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <div class=\"card\">\n                    <div class=\"card-header d-block\">\n                        <h3 style=\"float: left;\">Quản lý Công thức nấu ăn</h3>\n                        <div class=\"input-group\" style=\"margin-bottom: 0 !important; float: right; width: unset;\">\n                            <button class=\"btn btn-info buttonAddNew\" type=\"submit\" style=\"height: 35px;\" (click)= \"openModalAdd()\">Create</button>\n                        </div>\n                    </div>\n                    <div class=\"card-body\">\n                        <table class=\"table\"> <!-- data_table -->\n                            <thead>\n                                <tr>\n                                    <th>Thứ tự</th>\n                                    <th>Bước</th>\n                                    <th>Mô tả</th>\n                                    <th>Thời gian</th>\n                                    <th class=\"nosort\">Thao tác</th>\n                                </tr>\n                            </thead>\n                            <tbody>\n                                <tr *ngFor=\"let item of recipe_response\">\n                                    <td>{{item.index}}</td>\n                                    <td>{{item.name}}</td>\n                                    <td>{{item.description}}</td>\n                                    <td>{{item.time}}</td>\n                                    <td>\n                                        <div class=\"table-actions\" style=\"text-align: left;\">\n                                            <a (click)=\"openModalEdit(item)\"><i class=\"ik ik-eye\"></i></a>\n                                            <a (click)=\"onDeleteModel(item.id)\"><i class=\"ik ik-trash-2\"></i></a>\n                                        </div>\n                                    </td>\n                                </tr>\n                            </tbody>\n                        </table>\n                      </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"modalAddEdit\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n          <div class=\"modal-header\">\n              <h5 class=\"modal-title\" id=\"modalTitle\"></h5>\n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                  <span aria-hidden=\"true\">&times;</span>\n              </button>\n          </div>\n          <div class=\"card-body\">\n              <form class=\"forms-sample\" (ngSubmit)=\"onSubmit(f,image)\" #f=\"ngForm\">\n                <div class=\"form-group\">\n                    <label for=\"name\">Bước</label>\n                    <input type=\"text\" class=\"form-control\" id=\"name\" name=\"name\" placeholder=\"Tên danh mục\" ngModel>\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"name\">Mô tả</label>\n                    <input type=\"text\" class=\"form-control\" id=\"description\" name=\"description\" placeholder=\"Mô tả\" ngModel>\n                </div>\n                <div class=\"form-group\">\n                    <label for=\"time\">Thời gian nấu</label>\n                    <select class=\"form-control\" id=\"time\" name=\"time\" [(ngModel)] = \"time\" >\n                        <option value=\"0\">--Chọn thời gian nấu--</option>\n                        <option value=\"900\">15 Phút</option>\n                        <option value=\"1800\">30 Phút</option>\n                        <option value=\"2700\">45 Phút</option>\n                    </select>\n                </div>\n                <div class=\"form-group\" id=\"index-show\">\n                    <label for=\"index\">Thứ tự</label>\n                    <input type=\"number\" class=\"form-control\" id=\"index\" name=\"index\" placeholder=\"Thứ tự\" ngModel >\n                </div>\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-secondary btnDelete\" data-dismiss=\"modal\">Delete</button>\n                    <button type=\"submit\" class=\"btn btn-primary\" id=\"submit\">Save changes</button>\n                </div>\n              </form>\n          </div>\n          \n      </div>\n  </div>\n</div>\n\n<!-- Modal Delete -->\n<div id=\"modalDelete\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog modal-dialog-centered\">\n      <div class=\"modal-content\" style=\"padding: 30px;\">\n          <input type=\"hidden\" id=\"id-delete\" name=\"id-delete\" />\n          <h2 class=\"h2-waring text-center\">Are You Sure?</h2>\n          <p class=\"lead text-muted text-center\" style=\"display: block;\">You\n              will not restore this data!</p>\n          <div class=\"sa-button-container\" style=\"text-align: center\">\n            <button class=\"confirm btn-lg btn-info\" data-dismiss=\"modal\" id=\"delete\" style=\"margin-right: 10px\" (click)=\"destroy()\">\n                Agree\n            </button>\n              <button class=\"cancel btn-lg btn-default\"\n                      style=\"border: 1px solid #ccc\" data-dismiss=\"modal\">Cancel</button>\n          </div>\n      </div>\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/timeline/timeline.component.scss":
/*!********************************************************!*\
  !*** ./src/app/admin/timeline/timeline.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3RpbWVsaW5lL3RpbWVsaW5lLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/admin/timeline/timeline.component.ts":
/*!******************************************************!*\
  !*** ./src/app/admin/timeline/timeline.component.ts ***!
  \******************************************************/
/*! exports provided: TimelineComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimelineComponent", function() { return TimelineComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/x/http/toast-notification.service */ "./src/x/http/toast-notification.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/shared/shared.service */ "./src/app/common/shared/shared.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TimelineComponent = /** @class */ (function () {
    function TimelineComponent(api, toast, router, shared, route) {
        this.api = api;
        this.toast = toast;
        this.router = router;
        this.shared = shared;
        this.route = route;
        this.recipe_response = [];
        this.time = "0";
    }
    TimelineComponent.prototype.ngOnInit = function () {
        this.time = "0";
        this.loadAllRecipe();
    };
    TimelineComponent.prototype.openModalAdd = function () {
        $("#index-show").hide();
        this.modalAction = "add";
        $('#name').val("");
        $('#description').val("");
        this.time = "0";
        $("#modalAddEdit").modal('show');
    };
    TimelineComponent.prototype.openModalEdit = function (item) {
        this.modalAction = "edit";
        $("#index-show").show();
        this.idRecipe = item.id;
        $('#name').val(item.name);
        $('#description').val(item.description);
        this.checkName = item.name;
        $('#index').val(item.index);
        this.time = item.time;
        $("#modalAddEdit").modal('show');
    };
    TimelineComponent.prototype.onDeleteModel = function (idModel) {
        $('#modalDelete').modal('show');
        this.idRecipe = idModel;
    };
    TimelineComponent.prototype.onSubmit = function (f) {
        console.log(f.value.time);
        if ($('#name').val().trim() == "" || $('#description').val().trim() == "" || f.value.time == "0") {
            this.toast.error("Không được để trống trường truyền lên!");
            return;
        }
        switch (this.modalAction) {
            case "add":
                this.create(f);
                break;
            case "edit":
                this.update(f);
                break;
            default:
            // code block
        }
    };
    TimelineComponent.prototype.create = function (f) {
        var _this = this;
        var body = {
            dish_id: this.route.snapshot.paramMap.get('id'),
            name: f.value.name,
            description: f.value.description,
            time: f.value.time
        };
        this.api.onAddRecipe(body).subscribe(function (res) {
            if (res.code_status == 200) {
                _this.loadAllRecipe();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    TimelineComponent.prototype.update = function (f) {
        var _this = this;
        var body;
        if (this.checkName != $('#name').val()) {
            body = {
                id: this.idRecipe,
                name: $("#name").val(),
                description: $("#description").val(),
                time: $("#time").val(),
                index: $("#index").val(),
            };
        }
        else {
            body = {
                id: this.idRecipe,
                description: $("#description").val(),
                time: $("#time").val(),
                index: $("#index").val(),
            };
        }
        this.api.onUpdateRecipe(body).subscribe(function (res) {
            if (res.code_status == 200) {
                _this.loadAllRecipe();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    TimelineComponent.prototype.destroy = function () {
        var _this = this;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]();
        params = params.append('id', JSON.stringify(this.idRecipe));
        this.api.onDeleteRecipe(params).subscribe(function (res) {
            _this.loadAllRecipe();
            _this.toast.success(res['message']);
            $('#modalDelete').modal('hide');
        });
    };
    TimelineComponent.prototype.loadAllRecipe = function () {
        var _this = this;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]();
        params = params.append('dish_id', this.route.snapshot.paramMap.get('id'));
        this.api.onGetAllRecipe(params).subscribe(function (res) {
            _this.recipe_response = res.data;
            console.log(_this.recipe_response);
        });
    };
    TimelineComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-timeline',
            template: __webpack_require__(/*! ./timeline.component.html */ "./src/app/admin/timeline/timeline.component.html"),
            styles: [__webpack_require__(/*! ./timeline.component.scss */ "./src/app/admin/timeline/timeline.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__["ToastNotificationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__["SharedService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], TimelineComponent);
    return TimelineComponent;
}());



/***/ }),

/***/ "./src/app/admin/tool/tool.component.html":
/*!************************************************!*\
  !*** ./src/app/admin/tool/tool.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\n  <div class=\"container-fluid\">\n      <div class=\"page-header\">\n          <div class=\"row align-items-end\">\n              <div class=\"col-lg-8\">\n                  <div class=\"page-header-title\">\n                      <i class=\"ik ik-inbox bg-blue\"></i>\n                      <div class=\"d-inline\">\n                          <h5>Data Table</h5>\n                          <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>\n                      </div>\n                  </div>\n              </div>\n              <div class=\"col-lg-4\">\n                  <nav class=\"breadcrumb-container\" aria-label=\"breadcrumb\">\n                      <ol class=\"breadcrumb\">\n                          <li class=\"breadcrumb-item\">\n                              <a href=\"../index.html\"><i class=\"ik ik-home\"></i></a>\n                          </li>\n                          <li class=\"breadcrumb-item\">\n                              <a href=\"#\">Tables</a>\n                          </li>\n                          <li class=\"breadcrumb-item active\" aria-current=\"page\">Data Table</li>\n                      </ol>\n                  </nav>\n              </div>\n          </div>\n      </div>\n\n\n      <div class=\"row\">\n          <div class=\"col-md-12\">\n              <div class=\"card\">\n                  <div class=\"card-header d-block\">\n                      <h3 style=\"float: left;\">Quản lý công cụ</h3>\n                      <div class=\"input-group\" style=\"margin-bottom: 0 !important; float: right; width: unset;\">\n                          <button class=\"btn btn-info buttonAddNew\" type=\"submit\" style=\"height: 35px;\" (click)= \"openModalAdd()\">Create</button>\n                      </div>\n                  </div>\n                  <div class=\"card-body\">\n                      <table class=\"table\"> <!-- data_table -->\n                          <thead>\n                              <tr>\n                                  <th>#</th>\n                                  <th>Tên công cụ</th>\n                                  <th class=\"nosort\">Ảnh</th>\n                                  <th class=\"nosort\">Thao tác</th>\n                              </tr>\n                          </thead>\n                          <tbody>\n                              <tr *ngFor=\"let item of tool_response\">\n                                  <td>{{item.id}}</td>\n                                  <td>{{item.name}}</td>\n                                  <td><img src=\"{{item.image}}\" class=\"table-user-thumb text-left\" alt=\"{{item.name}}\"></td>\n                                  <td>\n                                      <div class=\"table-actions\" style=\"text-align: left;\">\n                                          <a (click)=\"openModalEdit(item)\"><i class=\"ik ik-eye\"></i></a>\n                                          <a (click)=\"onDeleteModel(item.id)\"><i class=\"ik ik-trash-2\"></i></a>\n                                      </div>\n                                  </td>\n                              </tr>\n                          </tbody>\n                      </table>\n                  </div>\n              </div>\n          </div>\n      </div>\n  </div>\n</div>\n\n\n<!-- Modal -->\n<div class=\"modal fade\" id=\"modalAddEdit\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">\n  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">\n      <div class=\"modal-content\">\n          <div class=\"modal-header\">\n              <h5 class=\"modal-title\" id=\"modalTitle\"></h5>\n              <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">\n                  <span aria-hidden=\"true\">&times;</span>\n              </button>\n          </div>\n          <div class=\"card-body\">\n              <form class=\"forms-sample\" (ngSubmit)=\"onSubmit(f,image)\" #f=\"ngForm\">\n                <div class=\"form-group\">\n                    <label for=\"name\">Tên công cụ</label>\n                    <input \n                        type=\"text\" \n                        class=\"form-control\" \n                        id=\"name\" \n                        name=\"name\" \n                        placeholder=\"Tên nguyên liệu\" ngModel>\n                </div>\n\n                <div class=\"form-group\">\n                    <label for=\"image\">Ảnh mô tả</label>\n                    <input type=\"file\" id=\"image\" class=\"form-control\" #image (change)=\"onFileChanged($event)\">\n                    <img src=\"\" alt=\"no-image\" class=\"no-img\" id=\"preview\">\n                </div>\n\n                <div class=\"modal-footer\">\n                    <button type=\"button\" class=\"btn btn-secondary btnDelete\" data-dismiss=\"modal\">Delete</button>\n                    <button type=\"submit\" class=\"btn btn-primary\" id=\"submit\">Save changes</button>\n                </div>\n              </form>\n          </div>\n          \n      </div>\n  </div>\n</div>\n\n<!-- Modal Delete -->\n<div id=\"modalDelete\" class=\"modal fade\" role=\"dialog\">\n  <div class=\"modal-dialog modal-dialog-centered\">\n      <div class=\"modal-content\" style=\"padding: 30px;\">\n          <input type=\"hidden\" id=\"id-delete\" name=\"id-delete\" />\n          <h2 class=\"h2-waring text-center\">Are You Sure?</h2>\n          <p class=\"lead text-muted text-center\" style=\"display: block;\">You\n              will not restore this data!</p>\n          <div class=\"sa-button-container\" style=\"text-align: center\">\n            <button class=\"confirm btn-lg btn-info\" data-dismiss=\"modal\" id=\"delete\" style=\"margin-right: 10px\" (click)=\"destroy()\">\n                Agree\n            </button>\n              <button class=\"cancel btn-lg btn-default\"\n                      style=\"border: 1px solid #ccc\" data-dismiss=\"modal\">Cancel</button>\n          </div>\n      </div>\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/admin/tool/tool.component.scss":
/*!************************************************!*\
  !*** ./src/app/admin/tool/tool.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FkbWluL3Rvb2wvdG9vbC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/admin/tool/tool.component.ts":
/*!**********************************************!*\
  !*** ./src/app/admin/tool/tool.component.ts ***!
  \**********************************************/
/*! exports provided: ToolComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToolComponent", function() { return ToolComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/service/api.service */ "./src/app/service/api.service.ts");
/* harmony import */ var src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/x/http/toast-notification.service */ "./src/x/http/toast-notification.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/common/shared/shared.service */ "./src/app/common/shared/shared.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ToolComponent = /** @class */ (function () {
    function ToolComponent(api, toast, router, shared) {
        this.api = api;
        this.toast = toast;
        this.router = router;
        this.shared = shared;
        this.imageUrl = "assets/img/no-img.jpg";
        this.tool_response = [];
    }
    ToolComponent.prototype.ngOnInit = function () {
        this.getData();
    };
    ToolComponent.prototype.openModalAdd = function () {
        this.modalAction = "add";
        $('#image').prop("value", "");
        $("#preview").attr('src', "assets/img/no-img.jpg");
        this.imageUrl =
            $('#name').val("");
        $("#modalAddEdit").modal('show');
    };
    ToolComponent.prototype.openModalEdit = function (item) {
        this.modalAction = "edit";
        this.idTool = item.id;
        this.checkName = item.name;
        $('#name').val(item.name);
        $("#preview").attr('src', item.image);
        $('#image').prop("value", "");
        $("#modalAddEdit").modal('show');
    };
    ToolComponent.prototype.onDeleteModel = function (idModel) {
        $('#modalDelete').modal('show');
        this.idTool = idModel;
    };
    ToolComponent.prototype.onSubmit = function (f, image) {
        if ($('#name').val().trim() == "") {
            this.toast.error("Không được để trống trường truyền lên!");
            return;
        }
        switch (this.modalAction) {
            case "add":
                this.create(f, image);
                break;
            case "edit":
                this.update(f, image);
                break;
            default:
            // code block
        }
    };
    ToolComponent.prototype.create = function (f, image) {
        var _this = this;
        var formData = new FormData();
        if (!image.files[0]) {
            this.toast.error("Không được để trống trường truyền lên!");
            return;
        }
        formData.set('image', image.files[0]);
        formData.set('name', f.value.name);
        this.api.onAddTool(formData).subscribe(function (res) {
            if (res.code_status == 200) {
                _this.getData();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    ToolComponent.prototype.update = function (f, image) {
        var _this = this;
        var formData = new FormData();
        formData.set('id', JSON.stringify(this.idTool));
        if (image.files[0]) {
            formData.set('image', image.files[0]);
        }
        if (this.checkName != $('#name').val()) {
            formData.set('name', $('#name').val());
        }
        this.api.onUpdateTool(formData).subscribe(function (res) {
            if (res.code_status == 200) {
                _this.getData();
                _this.toast.success(res.message);
                $("#modalAddEdit").modal('hide');
            }
            else if (res.code_status == 400) {
                _this.toast.error("Name is exist!");
            }
        });
    };
    ToolComponent.prototype.destroy = function () {
        var _this = this;
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpParams"]();
        params = params.append('id', JSON.stringify(this.idTool));
        this.api.onDeleteTool(params).subscribe(function (res) {
            _this.getData();
            _this.toast.success(res['message']);
            $('#modalDelete').modal('hide');
        });
    };
    ToolComponent.prototype.getData = function () {
        var _this = this;
        this.api.onGetAllTool().subscribe(function (res) {
            _this.tool_response = res.data;
            console.log(_this.tool_response);
        });
    };
    /**
    * function preview images
    * @param event
    */
    ToolComponent.prototype.onFileChanged = function (event) {
        this.shared.previewImage(event, 'preview');
    };
    ToolComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-tool',
            template: __webpack_require__(/*! ./tool.component.html */ "./src/app/admin/tool/tool.component.html"),
            styles: [__webpack_require__(/*! ./tool.component.scss */ "./src/app/admin/tool/tool.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_service_api_service__WEBPACK_IMPORTED_MODULE_1__["ApiService"],
            src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_2__["ToastNotificationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            src_app_common_shared_shared_service__WEBPACK_IMPORTED_MODULE_4__["SharedService"]])
    ], ToolComponent);
    return ToolComponent;
}());



/***/ }),

/***/ "./src/app/common/shared/shared.service.ts":
/*!*************************************************!*\
  !*** ./src/app/common/shared/shared.service.ts ***!
  \*************************************************/
/*! exports provided: SharedService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedService", function() { return SharedService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SharedService = /** @class */ (function () {
    function SharedService() {
    }
    /**
   * function preview images
   * @param event
   * @param inputId
   */
    SharedService.prototype.previewImage = function (event, inputId) {
        if (event.target.files && event.target.files[0]) {
            var Extension = event.target.files[0].type.split('/')[1];
            var size = event.target.files[0].size;
            var inputId = '#' + inputId;
            if (Extension == "png" || Extension == "jpeg" || Extension == "jpg") {
                if (size > 5000000) {
                    $('#image').prop("value", "");
                    alert("size không quá 5 MB");
                }
                else {
                    var file = event.target.files[0];
                    var reader_1 = new FileReader();
                    reader_1.onload = function (e) {
                        $(inputId).attr('src', reader_1.result);
                    };
                    reader_1.readAsDataURL(file);
                }
            }
            else {
                $('#image').prop("value", "");
                alert("Ảnh chỉ cho phép các loại tệp PNG, JPG, JPEG.");
            }
        }
    };
    SharedService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], SharedService);
    return SharedService;
}());



/***/ })

}]);
//# sourceMappingURL=admin-admin-module.js.map