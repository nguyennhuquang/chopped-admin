(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./admin/admin.module": [
		"./src/app/admin/admin.module.ts",
		"default~admin-admin-module~auth-auth-module",
		"admin-admin-module"
	],
	"./auth/auth.module": [
		"./src/app/auth/auth.module.ts",
		"default~admin-admin-module~auth-auth-module",
		"auth-auth-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var routes = [
    {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthModule'
    },
    {
        path: 'admin',
        loadChildren: './admin/admin.module#AdminModule'
    },
    {
        path: 'admin/dashboard',
        redirectTo: 'admin',
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: '/admin/dashboard'
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, { useHash: true })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'master';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _core_core_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./core/core.module */ "./src/app/core/core.module.ts");
/* harmony import */ var src_x_http_http_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/x/http/http.service */ "./src/x/http/http.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                _core_core_module__WEBPACK_IMPORTED_MODULE_4__["CoreModule"],
            ],
            providers: [src_x_http_http_service__WEBPACK_IMPORTED_MODULE_5__["HttpService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/core/bav-bar/bav-bar.component.html":
/*!*****************************************************!*\
  !*** ./src/app/core/bav-bar/bav-bar.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  bav-bar works!\n</p>\n"

/***/ }),

/***/ "./src/app/core/bav-bar/bav-bar.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/core/bav-bar/bav-bar.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvYmF2LWJhci9iYXYtYmFyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/core/bav-bar/bav-bar.component.ts":
/*!***************************************************!*\
  !*** ./src/app/core/bav-bar/bav-bar.component.ts ***!
  \***************************************************/
/*! exports provided: BavBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BavBarComponent", function() { return BavBarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var BavBarComponent = /** @class */ (function () {
    function BavBarComponent() {
    }
    BavBarComponent.prototype.ngOnInit = function () {
    };
    BavBarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bav-bar',
            template: __webpack_require__(/*! ./bav-bar.component.html */ "./src/app/core/bav-bar/bav-bar.component.html"),
            styles: [__webpack_require__(/*! ./bav-bar.component.scss */ "./src/app/core/bav-bar/bav-bar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], BavBarComponent);
    return BavBarComponent;
}());



/***/ }),

/***/ "./src/app/core/core.module.ts":
/*!*************************************!*\
  !*** ./src/app/core/core.module.ts ***!
  \*************************************/
/*! exports provided: CoreModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CoreModule", function() { return CoreModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var ng_snotify__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng-snotify */ "./node_modules/ng-snotify/index.js");
/* harmony import */ var src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/x/http/toast-notification.service */ "./src/x/http/toast-notification.service.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/core/footer/footer.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./header/header.component */ "./src/app/core/header/header.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_x_http_http_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/x/http/http.service */ "./src/x/http/http.service.ts");
/* harmony import */ var _bav_bar_bav_bar_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./bav-bar/bav-bar.component */ "./src/app/core/bav-bar/bav-bar.component.ts");
/* harmony import */ var _nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./nav-bar/nav-bar.component */ "./src/app/core/nav-bar/nav-bar.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var CoreModule = /** @class */ (function () {
    function CoreModule() {
    }
    CoreModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [_footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"], _header_header_component__WEBPACK_IMPORTED_MODULE_5__["HeaderComponent"], _bav_bar_bav_bar_component__WEBPACK_IMPORTED_MODULE_8__["BavBarComponent"], _nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_9__["NavBarComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                ng_snotify__WEBPACK_IMPORTED_MODULE_2__["SnotifyModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_10__["RouterModule"]
            ],
            providers: [
                src_x_http_toast_notification_service__WEBPACK_IMPORTED_MODULE_3__["ToastNotificationService"],
                { provide: 'SnotifyToastConfig', useValue: ng_snotify__WEBPACK_IMPORTED_MODULE_2__["ToastDefaults"] },
                ng_snotify__WEBPACK_IMPORTED_MODULE_2__["SnotifyService"],
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HTTP_INTERCEPTORS"],
                    useClass: src_x_http_http_service__WEBPACK_IMPORTED_MODULE_7__["HttpErrorService"],
                    multi: true
                },
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HTTP_INTERCEPTORS"],
                    useClass: src_x_http_http_service__WEBPACK_IMPORTED_MODULE_7__["AuthHttpService"],
                    multi: true
                }
            ],
            exports: [
                ng_snotify__WEBPACK_IMPORTED_MODULE_2__["SnotifyModule"], _footer_footer_component__WEBPACK_IMPORTED_MODULE_4__["FooterComponent"], _header_header_component__WEBPACK_IMPORTED_MODULE_5__["HeaderComponent"], _nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_9__["NavBarComponent"]
            ]
        })
    ], CoreModule);
    return CoreModule;
}());



/***/ }),

/***/ "./src/app/core/footer/footer.component.html":
/*!***************************************************!*\
  !*** ./src/app/core/footer/footer.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"footer\">\n  <div class=\"w-100 clearfix\">\n      <span class=\"text-center text-sm-left d-md-inline-block\">Copyright © 2018 ThemeKit v2.0. All Rights Reserved.</span>\n      <span class=\"float-none float-sm-right mt-1 mt-sm-0 text-center\">Crafted with <i class=\"fa fa-heart text-danger\"></i> by <a href=\"http://lavalite.org/\" class=\"text-dark\" target=\"_blank\">Lavalite</a></span>\n  </div>\n</footer>"

/***/ }),

/***/ "./src/app/core/footer/footer.component.scss":
/*!***************************************************!*\
  !*** ./src/app/core/footer/footer.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/core/footer/footer.component.ts":
/*!*************************************************!*\
  !*** ./src/app/core/footer/footer.component.ts ***!
  \*************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/core/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/core/footer/footer.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/core/header/header.component.html":
/*!***************************************************!*\
  !*** ./src/app/core/header/header.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"header-top\" header-theme=\"light\">\n  <div class=\"container-fluid\">\n      <div class=\"d-flex justify-content-between\">\n          <div class=\"top-menu d-flex align-items-center\">\n              <button type=\"button\" class=\"btn-icon mobile-nav-toggle d-lg-none\"><span></span></button>\n              <div class=\"header-search\">\n                  <div class=\"input-group\">\n                      <span class=\"input-group-addon search-close\"><i class=\"ik ik-x\"></i></span>\n                      <input type=\"text\" class=\"form-control\">\n                      <span class=\"input-group-addon search-btn\"><i class=\"ik ik-search\"></i></span>\n                  </div>\n              </div>\n              <button type=\"button\" id=\"navbar-fullscreen\" class=\"nav-link\"><i class=\"ik ik-maximize\"></i></button>\n          </div>\n          <div class=\"top-menu d-flex align-items-center\">\n              <div class=\"dropdown\">\n                  <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"notiDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"ik ik-bell\"></i><span class=\"badge bg-danger\">3</span></a>\n                  <div class=\"dropdown-menu dropdown-menu-right notification-dropdown\" aria-labelledby=\"notiDropdown\">\n                      <h4 class=\"header\">Notifications</h4>\n                      <div class=\"notifications-wrap\">\n                          <a href=\"#\" class=\"media\">\n                              <span class=\"d-flex\">\n                                  <i class=\"ik ik-check\"></i> \n                              </span>\n                              <span class=\"media-body\">\n                                  <span class=\"heading-font-family media-heading\">Invitation accepted</span> \n                                  <span class=\"media-content\">Your have been Invited ...</span>\n                              </span>\n                          </a>\n                          <a href=\"#\" class=\"media\">\n                              <span class=\"d-flex\">\n                                  <img src=\"assets/img/users/1.jpg\" class=\"rounded-circle\" alt=\"\">\n                              </span>\n                              <span class=\"media-body\">\n                                  <span class=\"heading-font-family media-heading\">Steve Smith</span> \n                                  <span class=\"media-content\">I slowly updated projects</span>\n                              </span>\n                          </a>\n                          <a href=\"#\" class=\"media\">\n                              <span class=\"d-flex\">\n                                  <i class=\"ik ik-calendar\"></i> \n                              </span>\n                              <span class=\"media-body\">\n                                  <span class=\"heading-font-family media-heading\">To Do</span> \n                                  <span class=\"media-content\">Meeting with Nathan on Friday 8 AM ...</span>\n                              </span>\n                          </a>\n                      </div>\n                      <div class=\"footer\"><a href=\"javascript:void(0);\">See all activity</a></div>\n                  </div>\n              </div>\n              <button type=\"button\" class=\"nav-link ml-10 right-sidebar-toggle\"><i class=\"ik ik-message-square\"></i><span class=\"badge bg-success\">3</span></button>\n              <div class=\"dropdown\">\n                  <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"menuDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><i class=\"ik ik-plus\"></i></a>\n                  <div class=\"dropdown-menu dropdown-menu-right menu-grid\" aria-labelledby=\"menuDropdown\">\n                      <a class=\"dropdown-item\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Dashboard\"><i class=\"ik ik-bar-chart-2\"></i></a>\n                      <a class=\"dropdown-item\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Message\"><i class=\"ik ik-mail\"></i></a>\n                      <a class=\"dropdown-item\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Accounts\"><i class=\"ik ik-users\"></i></a>\n                      <a class=\"dropdown-item\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Sales\"><i class=\"ik ik-shopping-cart\"></i></a>\n                      <a class=\"dropdown-item\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Purchase\"><i class=\"ik ik-briefcase\"></i></a>\n                      <a class=\"dropdown-item\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Pages\"><i class=\"ik ik-clipboard\"></i></a>\n                      <a class=\"dropdown-item\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Chats\"><i class=\"ik ik-message-square\"></i></a>\n                      <a class=\"dropdown-item\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Contacts\"><i class=\"ik ik-map-pin\"></i></a>\n                      <a class=\"dropdown-item\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Blocks\"><i class=\"ik ik-inbox\"></i></a>\n                      <a class=\"dropdown-item\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Events\"><i class=\"ik ik-calendar\"></i></a>\n                      <a class=\"dropdown-item\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Notifications\"><i class=\"ik ik-bell\"></i></a>\n                      <a class=\"dropdown-item\" href=\"#\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"More\"><i class=\"ik ik-more-horizontal\"></i></a>\n                  </div>\n              </div>\n              <button type=\"button\" class=\"nav-link ml-10\" id=\"apps_modal_btn\" data-toggle=\"modal\" data-target=\"#appsModal\"><i class=\"ik ik-grid\"></i></button>\n              <div class=\"dropdown\">\n                  <a class=\"dropdown-toggle\" href=\"#\" id=\"userDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><img class=\"avatar\" src=\"assets/img/user.jpg\" alt=\"\"></a>\n                  <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"userDropdown\">\n                      <a class=\"dropdown-item\" href=\"profile.html\"><i class=\"ik ik-user dropdown-icon\"></i> Profile</a>\n                      <a class=\"dropdown-item\" href=\"#\"><i class=\"ik ik-settings dropdown-icon\"></i> Settings</a>\n                      <a class=\"dropdown-item\" href=\"#\"><span class=\"float-right\"><span class=\"badge badge-primary\">6</span></span><i class=\"ik ik-mail dropdown-icon\"></i> Inbox</a>\n                      <a class=\"dropdown-item\" href=\"#\"><i class=\"ik ik-navigation dropdown-icon\"></i> Message</a>\n                      <a class=\"dropdown-item\" (click)=\"logout()\"><i class=\"ik ik-power dropdown-icon\"></i> Logout</a>\n                  </div>\n              </div>\n\n          </div>\n      </div>\n  </div>\n</header>"

/***/ }),

/***/ "./src/app/core/header/header.component.scss":
/*!***************************************************!*\
  !*** ./src/app/core/header/header.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/core/header/header.component.ts":
/*!*************************************************!*\
  !*** ./src/app/core/header/header.component.ts ***!
  \*************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(router) {
        this.router = router;
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent.prototype.logout = function () {
        localStorage.clear();
        this.router.navigate(["/auth/signin"]);
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/core/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/core/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/core/nav-bar/nav-bar.component.html":
/*!*****************************************************!*\
  !*** ./src/app/core/nav-bar/nav-bar.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app-sidebar colored\">\n  <div class=\"sidebar-header\">\n      <a class=\"header-brand\" href=\"index.html\">\n          <div class=\"logo-img\">\n             <img src=\"assets/src/img/brand-white.svg\" class=\"header-brand-img\" alt=\"lavalite\"> \n          </div>\n          <span class=\"text\">ThemeKit</span>\n      </a>\n      <button type=\"button\" class=\"nav-toggle\"><i data-toggle=\"expanded\" class=\"ik ik-toggle-right toggle-icon\"></i></button>\n      <button id=\"sidebarClose\" class=\"nav-close\"><i class=\"ik ik-x\"></i></button>\n  </div>\n  \n  <div class=\"sidebar-content\">\n      <div class=\"nav-container\">\n          <nav id=\"main-menu-navigation\" class=\"navigation-main\">\n              <div class=\"nav-lavel\">Dashboard</div>\n              <div class=\"nav-item\">\n                  <a [routerLink]=\"['./dashboard']\"\n                  routerLinkActive=\"active\"><i class=\"ik ik-bar-chart-2\"></i><span>Dashboard</span></a>\n              </div>\n              <!-- <div class=\"nav-item\">\n                  <a href=\"pages/navbar.html\"><i class=\"ik ik-menu\"></i><span>Navigation</span> <span class=\"badge badge-success\">New</span></a>\n              </div>\n              <div class=\"nav-item has-sub\">\n                  <a href=\"javascript:void(0)\"><i class=\"ik ik-layers\"></i><span>Widgets</span> <span class=\"badge badge-danger\">150+</span></a>\n                  <div class=\"submenu-content\">\n                      <a href=\"pages/widgets.html\" class=\"menu-item\">Basic</a>\n                      <a href=\"pages/widget-statistic.html\" class=\"menu-item\">Statistic</a>\n                      <a href=\"pages/widget-data.html\" class=\"menu-item\">Data</a>\n                      <a href=\"pages/widget-chart.html\" class=\"menu-item\">Chart Widget</a>\n                  </div>\n              </div> -->\n              <!-- <div class=\"nav-lavel\">User</div>\n              <div class=\"nav-item has-sub\">\n                  <a href=\"#\"><i class=\"ik ik-box\"></i><span>Basic</span></a>\n                  <div class=\"submenu-content\">\n                      <a href=\"pages/ui/alerts.html\" class=\"menu-item\">Alerts</a>\n                      <a href=\"pages/ui/badges.html\" class=\"menu-item\">Badges</a>\n                      <a href=\"pages/ui/buttons.html\" class=\"menu-item\">Buttons</a>\n                      <a href=\"pages/ui/navigation.html\" class=\"menu-item\">Navigation</a>\n                  </div>\n              </div>\n              <div class=\"nav-item has-sub\">\n                  <a href=\"#\"><i class=\"ik ik-gitlab\"></i><span>Advance</span> <span class=\"badge badge-success\">New</span></a>\n                  <div class=\"submenu-content\">\n                      <a href=\"pages/ui/modals.html\" class=\"menu-item\">Modals</a>\n                      <a href=\"pages/ui/notifications.html\" class=\"menu-item\">Notifications</a>\n                      <a href=\"pages/ui/carousel.html\" class=\"menu-item\">Slider</a>\n                      <a href=\"pages/ui/range-slider.html\" class=\"menu-item\">Range Slider</a>\n                      <a href=\"pages/ui/rating.html\" class=\"menu-item\">Rating</a>\n                  </div>\n              </div>\n              <div class=\"nav-item has-sub\">\n                  <a href=\"#\"><i class=\"ik ik-package\"></i><span>Extra</span></a>\n                  <div class=\"submenu-content\">\n                      <a href=\"pages/ui/session-timeout.html\" class=\"menu-item\">Session Timeout</a>\n                  </div>\n              </div>\n              <div class=\"nav-item\">\n                  <a href=\"pages/ui/icons.html\"><i class=\"ik ik-command\"></i><span>Icons</span></a>\n              </div> -->\n\n              <div class=\"nav-lavel\">Country</div>\n              <div class=\"nav-item\">\n                  <a [routerLink]=\"['./country']\" routerLinkActive=\"active\"><i class=\"ik ik-box\"></i><span>Quốc gia</span></a>\n              </div>\n              \n              <div class=\"nav-lavel\">Dish</div>\n              <div class=\"nav-item\">\n                  <a [routerLink]=\"['./dish']\" routerLinkActive=\"active\"><i class=\"ik ik-box\"></i><span>Món ăn</span></a>\n              </div>\n\n              <div class=\"nav-lavel\">Material Category</div>\n              <div class=\"nav-item\">\n                  <a [routerLink]=\"['./category']\" routerLinkActive=\"active\"><i class=\"ik ik-credit-card\"></i><span>Danh mục nguyên liệu</span></a>\n              </div>\n\n              <div class=\"nav-lavel\">Tool</div>\n              <div class=\"nav-item\">\n                  <a [routerLink]=\"['./tool']\" routerLinkActive=\"active\"><i class=\"ik ik-pie-chart\"></i><span>Công cụ</span></a>\n              </div>\n\n              <div class=\"nav-lavel\">Spice</div>\n              <div class=\"nav-item\">\n                  <a [routerLink]=\"['./spice']\" routerLinkActive=\"active\"><i class=\"ik ik-calendar\"></i><span>Gia vị</span></a>\n              </div>\n          </nav>\n      </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/core/nav-bar/nav-bar.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/core/nav-bar/nav-bar.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvbmF2LWJhci9uYXYtYmFyLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/core/nav-bar/nav-bar.component.ts":
/*!***************************************************!*\
  !*** ./src/app/core/nav-bar/nav-bar.component.ts ***!
  \***************************************************/
/*! exports provided: NavBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavBarComponent", function() { return NavBarComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavBarComponent = /** @class */ (function () {
    function NavBarComponent() {
    }
    NavBarComponent.prototype.ngOnInit = function () {
    };
    NavBarComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-nav-bar',
            template: __webpack_require__(/*! ./nav-bar.component.html */ "./src/app/core/nav-bar/nav-bar.component.html"),
            styles: [__webpack_require__(/*! ./nav-bar.component.scss */ "./src/app/core/nav-bar/nav-bar.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], NavBarComponent);
    return NavBarComponent;
}());



/***/ }),

/***/ "./src/app/service/auth.service.ts":
/*!*****************************************!*\
  !*** ./src/app/service/auth.service.ts ***!
  \*****************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AuthService = /** @class */ (function () {
    function AuthService() {
    }
    AuthService.prototype.getToken = function () {
        return localStorage.getItem('token');
    };
    AuthService.prototype.setToken = function (value) {
        return localStorage.setItem('token', value);
    };
    AuthService.prototype.getItem = function (key) {
        localStorage.getItem(key);
    };
    AuthService.prototype.setItem = function (key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    baseURL: 'http://chopped.petngao.com'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ "./src/x/http/http.service.ts":
/*!************************************!*\
  !*** ./src/x/http/http.service.ts ***!
  \************************************/
/*! exports provided: HttpService, AuthHttpService, HttpErrorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpService", function() { return HttpService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthHttpService", function() { return AuthHttpService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpErrorService", function() { return HttpErrorService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _toast_notification_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./toast-notification.service */ "./src/x/http/toast-notification.service.ts");
/* harmony import */ var src_app_service_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/service/auth.service */ "./src/app/service/auth.service.ts");
/* harmony import */ var rxjs_add_operator_finally__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/operator/finally */ "./node_modules/rxjs-compat/_esm5/add/operator/finally.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/catch */ "./node_modules/rxjs-compat/_esm5/add/operator/catch.js");
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/add/operator/map */ "./node_modules/rxjs-compat/_esm5/add/operator/map.js");
/* harmony import */ var rxjs_add_operator_do__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/add/operator/do */ "./node_modules/rxjs-compat/_esm5/add/operator/do.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HttpService = /** @class */ (function () {
    function HttpService(http, toastService) {
        this.http = http;
        this.toastService = toastService;
    }
    HttpService.prototype.Get = function (url, params) {
        return this.http.get(url, { params: params }).map(function (res) {
            return res ? res : [];
        }).finally(function () {
        });
    };
    HttpService.prototype.Post = function (url, body) {
        return this.http.post(url, body).map(function (res) {
            return res ? res : [];
        }).catch(function (err) {
            return rxjs__WEBPACK_IMPORTED_MODULE_2__["Observable"].throw(err);
        }).finally(function () {
        });
    };
    HttpService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _toast_notification_service__WEBPACK_IMPORTED_MODULE_3__["ToastNotificationService"]])
    ], HttpService);
    return HttpService;
}());

var AuthHttpService = /** @class */ (function () {
    function AuthHttpService(authService) {
        this.authService = authService;
    }
    AuthHttpService.prototype.intercept = function (req, next) {
        req = req.clone({
            setHeaders: {
                Authorization: "Bearer " + this.authService.getToken()
            },
        });
        return next.handle(req);
    };
    AuthHttpService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [src_app_service_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]])
    ], AuthHttpService);
    return AuthHttpService;
}());

var HttpErrorService = /** @class */ (function () {
    function HttpErrorService(toastService) {
        this.toastService = toastService;
    }
    HttpErrorService.prototype.intercept = function (req, next) {
        // return next.handle(req).catch((err: HttpErrorResponse) => {
        //     console.log(err)
        //     if (err.error instanceof Error) {
        //         console.error('An error accured', err.error.message);
        //     } else {
        //         this.toastService.error(err.error['error']);
        //     }
        //     return Observable.throw(err);
        // });
        var _this = this;
        return next.handle(req).do(function (event) { }, function (err) {
            if (err instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpErrorResponse"]) {
                // do error handling here
                if (err.status == 0) {
                    _this.toastService.error('Không kết nối được đến server');
                }
                else {
                    _this.toastService.error(err.error['error']);
                }
            }
        });
    };
    HttpErrorService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_toast_notification_service__WEBPACK_IMPORTED_MODULE_3__["ToastNotificationService"]])
    ], HttpErrorService);
    return HttpErrorService;
}());



/***/ }),

/***/ "./src/x/http/toast-notification.service.ts":
/*!**************************************************!*\
  !*** ./src/x/http/toast-notification.service.ts ***!
  \**************************************************/
/*! exports provided: ToastNotificationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastNotificationService", function() { return ToastNotificationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng_snotify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng-snotify */ "./node_modules/ng-snotify/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ToastNotificationService = /** @class */ (function () {
    function ToastNotificationService(snotifyService) {
        this.snotifyService = snotifyService;
    }
    ToastNotificationService.prototype.clear = function () {
        this.snotifyService.clear();
    };
    ToastNotificationService.prototype.success = function (message) {
        this.clear();
        this.snotifyService.success(message);
    };
    ToastNotificationService.prototype.error = function (message) {
        this.clear();
        this.snotifyService.error(message);
    };
    ToastNotificationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [ng_snotify__WEBPACK_IMPORTED_MODULE_1__["SnotifyService"]])
    ], ToastNotificationService);
    return ToastNotificationService;
}());



/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/nguyenquang/Documents/QALATT/chopped/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map