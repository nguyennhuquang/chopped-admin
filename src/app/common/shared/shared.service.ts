import { Injectable } from '@angular/core';
declare var $: any;
@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor() { }

    /**
   * function preview images
   * @param event 
   * @param inputId 
   */
  previewImage(event: { target: { files: any[]; }; },inputId:string){
    if (event.target.files && event.target.files[0]) {
      var Extension = event.target.files[0].type.split('/')[1]
      var size = event.target.files[0].size
      var inputId = '#'+inputId
      if(Extension == "png"  || Extension == "jpeg" || Extension == "jpg"){
        if(size > 5000000){
          $('#image').prop("value", "");
          alert("size không quá 5 MB");
        }else{
          const file = event.target.files[0];
          const reader = new FileReader();
          reader.onload = function(e) {
            $(inputId).attr('src', reader.result);
          }    
          reader.readAsDataURL(file);
        }
      }else{
        $('#image').prop("value", "");
        alert("Ảnh chỉ cho phép các loại tệp PNG, JPG, JPEG.");
      }
    }
  }
}
