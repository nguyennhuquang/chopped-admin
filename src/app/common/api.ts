import { environment } from "src/environments/environment";

function getUrl(endpoint: string) {
    return environment.baseURL + '/api/backend/' + endpoint
}
export const API_URL = {
    signIn : getUrl('login'),
    getAllCountry: getUrl('get-all-country'),
    addCountry: getUrl('country/add'),
    updateCountry: getUrl('country/update'),
    deleteCountry: getUrl('country/delete'),
    /**
     * dish
     */
    dishGetAll: getUrl('dish/get-all'),
    dishAdd: getUrl('dish/add'),
    dishUpdate: getUrl('dish/update'),
    dishDelete: getUrl('dish/delete'),

    /**
     * material
     */
    materialGetAll: getUrl('material/get-all'),
    materialAdd: getUrl('material/add'),
    materialUpdate: getUrl('material/update'),
    materialDelete: getUrl('material/delete'),

    /**
     * category material
     */
    categoryGetAll: getUrl('material-category/get-all'),
    categoryAdd: getUrl('material-category/add'),
    categoryUpdate: getUrl('material-category/update'),
    categoryDelete: getUrl('material-category/delete'),

    /**
     *  tool
     */
    toolGetAll: getUrl('tool/get-all'),
    toolAdd: getUrl('tool/add'),
    toolUpdate: getUrl('tool/update'),
    toolDelete: getUrl('tool/delete'),


    /**
     * spice
     */
    spiceGetAll: getUrl('spice/get-all'),
    spiceAdd: getUrl('spice/add'),
    spiceUpdate: getUrl('spice/update'),
    spiceDelete: getUrl('spice/delete'),

    /**
     * recipe
     */
    recipeGetAll: getUrl('recipe/get'),
    recipeAdd: getUrl('recipe/add'),
    recipeUpdate: getUrl('recipe/update'),
    recipeDelete: getUrl('recipe/delete'),

    /**
     * recipe
     */
    materialsDishGet: getUrl('dish/get'),
    materialsDishAdd: getUrl('dish/add-many-material'),
    materialsDishUpdate: getUrl('dish/update-material'),
    materialsDishDelete: getUrl('dish/delete-material'),

    spicesDishGet: getUrl('dish/get-spices'),
    spicesDishAdd: getUrl('dish/add-spice'),
    spicesDishDelete: getUrl('dish/delete-spice')
}

