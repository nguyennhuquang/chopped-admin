export interface Categories {
    id: number;
    name: string;
}

export interface Category {
    code_status: number;
    message: string;
    data: Categories[];
}