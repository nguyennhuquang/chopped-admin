export interface Response {
    code_status: number;
    message: string;
    data: any[];
}