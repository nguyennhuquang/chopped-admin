export interface MaterialCategoryId {
    id: number;
    name: string;
}

export interface Materials {
    id: number;
    name: string;
    purchase_price: string;
    unit: string;
    image: string;
    thumbnail: string;
    material_category_id: MaterialCategoryId;
}

export interface Material {
    code_status: number;
    message: string;
    data: Materials[];
}