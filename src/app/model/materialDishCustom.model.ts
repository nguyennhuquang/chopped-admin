

export interface materialDishCustomData {
    id:number;
    name: string;
    image:string;
    status:number
}

export interface materialDishCustom {
    id: number;
    name: string;
    data: materialDishCustomData[];
}

export interface materialsAnswer {
    material_id: number;
    amount: string;
}

