export interface Country {
    id: number;
    code: string;
    name: string;
    created_at?: any;
    updated_at?: any;
}

export interface CountryResponse {
    code_status: number;
    message: string;
    data: Country[];
}