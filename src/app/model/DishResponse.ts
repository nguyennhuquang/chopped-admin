    export interface DataResponse {
        name: string;
        price: string;
        purchase_price: string;
        time: string;
        origin: string;
        image: string;
        id: number;
    }

    export interface DishResponse {
        code_status: number;
        message: string;
        data: DataResponse;
    }