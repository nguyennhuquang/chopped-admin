export interface Recipes {
    id: number;
    index: number;
    description: string;
    time: number;
    name: string;
}

export interface Recipe {
    code_status: number;
    message: string;
    data: Recipes[];
}