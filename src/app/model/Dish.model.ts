export interface Origin {
    id: number;
    code: string;
    name: string;
    created_at?: any;
    updated_at?: any;
}

export interface Dishes {
    id: number;
    name: string;
    purchase_price: string;
    origin: Origin;
    time: number;
    price: string;
    description: string;
    image: string;
    rating: string;
    total_material: string;
    total_review: string;
    total_cook: string;
    ratio?: any;
    ration:number;
}

export interface Dish {
    code_status: number;
    message: string;
    data: Dishes[];
}