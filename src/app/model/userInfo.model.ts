export interface Data {
    id: number;
    name: string;
    email: string;
    email_verified_at?: any;
    avatar: string;
    pro_version_purchase: number;
    stars: number;
    android_device_token: string;
    ios_device_token: string;
    created_at: string;
    updated_at: string;
    number_of_topics_done: string;
}

export interface UserInfo {
    code: number;
    message: string;
    data: Data;
}
