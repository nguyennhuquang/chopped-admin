export interface materialDish {
    id: number;
    name: string;
    purchase_price: string;
    image: string;
    unit: string;
    amount: string;
}

export interface RecipeDish {
    id: number;
    index: number;
    description: string;
    time: number;
    name: string;
}

export interface materialDishData {
    id: number;
    name: string;
    purchase_price: string;
    origin: string;
    time: number;
    price: string;
    description?: any;
    image: string;
    rating: string;
    total_material: string;
    total_review: string;
    total_cook: string;
    ratio?: any;
    materials: materialDish[];
    recipe: RecipeDish[];
}

export interface materialDishResponse {
    code_status: number;
    message: string;
    data: materialDishData;
}