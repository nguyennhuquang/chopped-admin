export interface Tools {
    id: number;
    name: string;
    image: string;
}

export interface Tool {
    code_status: number;
    message: string;
    data: Tools[];
}