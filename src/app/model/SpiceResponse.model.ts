export interface Spices {
    id: number;
    name: string;
    purchase_price: string;
    description: string;
    image: string;
    status: number;
}

export interface Spice {
    code_status: number;
    message: string;
    data: Spices[];
}