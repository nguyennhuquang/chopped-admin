import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToastDefaults, SnotifyService, SnotifyModule } from 'ng-snotify';
import { ToastNotificationService } from 'src/x/http/toast-notification.service';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HttpErrorService, AuthHttpService, HttpService } from 'src/x/http/http.service';
import { BavBarComponent } from './bav-bar/bav-bar.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { RouterModule} from '@angular/router'
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [FooterComponent, HeaderComponent, BavBarComponent, NavBarComponent],
  imports: [
    CommonModule,
    SnotifyModule,
    HttpClientModule,
    FormsModule,
    RouterModule
  ],
  providers:[
    ToastNotificationService,
    { provide: 'SnotifyToastConfig', useValue: ToastDefaults},
    SnotifyService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass:AuthHttpService ,
      multi: true
    }
  ], 
  exports: [
    SnotifyModule, FooterComponent, HeaderComponent, NavBarComponent
  ]
})
export class CoreModule { }
