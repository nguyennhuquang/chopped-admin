import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, from } from 'rxjs';
import { AuthService } from '../service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {

  constructor(
    private router      : Router,
    private authService : AuthService
  ){}

  canActivate() {
    if(!this.authService.getToken()) {
      this.router.navigate(["/auth/signin"]);
      return false;
    }
    return true;
  }
}

@Injectable({
  providedIn: 'root'
})
export class AdminAuthService {
  constructor(
    public router: Router,
    private authService : AuthService
  ) {}

  canActivateAuth(){
    if(this.authService.getToken()){
      this.router.navigate(["/admin/dashboard"]);
    }
  }

  // public logout() {}
}