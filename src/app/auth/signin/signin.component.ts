import { Component, OnInit } from '@angular/core';
import { AdminAuthService } from '../auth-guard.guard';
import { NgForm } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import { SignIn } from 'src/app/model/SignIn.model';
import { ToastNotificationService } from 'src/x/http/toast-notification.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
  providers:[ToastNotificationService]
})
export class SigninComponent implements OnInit {

  constructor(
    private AdminAuth : AdminAuthService,
    private api   : ApiService,
    private toast  : ToastNotificationService,
    private router : Router
  ) { }

  ngOnInit() {
    this.AdminAuth.canActivateAuth()
  }

  onSubmit(f:NgForm){
    this.api.onSignIn(f.value).subscribe(
      (res: SignIn) => {
        if(res.code_status == 200){
          localStorage.setItem('token', res.token)
          this.router.navigate(["/admin/dashboard"])
          this.toast.success("success!")
        }else{
          this.toast.error(res.msg)
        }
      }
    )
  }

}
