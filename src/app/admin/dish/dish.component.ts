import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { ToastNotificationService } from 'src/x/http/toast-notification.service';
import { Router } from '@angular/router';
import { Dish, Dishes } from 'src/app/model/Dish.model';
import { Country, CountryResponse } from 'src/app/model/Country.model';
import { NgForm } from '@angular/forms';
import { DishResponse } from 'src/app/model/DishResponse';
import { SharedService } from 'src/app/common/shared/shared.service';
import { HttpParams } from '@angular/common/http';
declare var $: any;
@Component({
  selector: 'app-dish',
  templateUrl: './dish.component.html',
  styleUrls: ['./dish.component.scss'],
})
export class DishComponent implements OnInit {
  @ViewChild('f') mytemplateForm : NgForm;
  dish_response  = [] as Dishes[]
  arr_country    = [] as Country[]
  origin:string = "0"
  time  :string = "0"
  modalAction: String;
  idDish: number
  imageUrl: String = "assets/img/no-img.jpg"

  constructor(
    private api   : ApiService,
    private toast  : ToastNotificationService,
    private router : Router,
    private shared  : SharedService
  ) { }

  ngOnInit() {
    this.loadAllRecord()
    this.loadOrigin()
    this.origin = "0"
    this.time = "0"
  }
  

  openModalAdd(){
    this.modalAction = "add";
    $('#name').val("")
    $('#price').val("")
    $('#purchase-price').val("")
    $('#ration').val("")
    this.origin = "0"
    $('#time').val("")
    $("#modalAddEdit").modal('show');
    $('#image').prop("value", "");
    $("#preview").attr("src","assets/img/no-img.jpg");
  }

  openModalEdit(item: { id: number; name: any; price: any; ration: any; purchase_price: any; origin: { id: string; }; time: string; image: any; }){
    this.modalAction = "edit";
    this.idDish = item.id
    $('#name').val(item.name)
    $('#price').val(item.price)
    $('#ration').val(item.ration)
    $('#time').val(item.time)
    $('#purchase-price').val(item.purchase_price)
    $('#image').prop("value", "");
    this.origin = item.origin.id
    $("#preview").attr("src",item.image);
    $("#modalAddEdit").modal('show');
  }

    /**
   * open model delete Category
   */
  onDeleteModel(idModel){
    $('#modalDelete').modal('show');
    this.idDish = idModel
  }

  onSubmit(f:NgForm, image:HTMLInputElement){
    if($('#name').val().trim() == "" || $('#price').val().trim() == "" || $('#purchase-price').val().trim() == "" || $('#time').val().trim() == "" || f.value.origin == "0"){
      this.toast.error("Không được để trống trường truyền lên!")
      return;
    }
    switch(this.modalAction) {
      case "add":
        this.create(f,image)
        break;
      case "edit":
        this.update(f,image)
        break;
      default:
        // code block
    }
  }


  create(f:NgForm, image:HTMLInputElement) {
    console.log("aaaaaa")
    var formData = new FormData()
    if(!image.files[0]){
      this.toast.error("Không được để trống trường truyền lên!")
      return;
    }
    formData.set('image', image.files[0])
    formData.set('name', f.value.name)
    formData.set('price', f.value.price)
    formData.set('ration', f.value.ration)
    formData.set('purchase_price', f.value.purchase_price)
    formData.set('origin', f.value.origin)
    formData.set('time', f.value.time)
    this.api.onAddDish(formData).subscribe(
      (res:DishResponse) => {
        if(res.code_status == 200){
          $("#modalAddEdit").modal('hide');
          this.toast.success(res.message)
          this.loadAllRecord()
        }
      }
    )
  };

  update(f:NgForm, image:HTMLInputElement){
    var formData = new FormData()
    if(image.files[0]){
      formData.set('image', image.files[0])
    }
    formData.set('id', JSON.stringify(this.idDish))
    formData.set('name', $('#name').val())
    formData.set('price', $('#price').val())
    formData.set('ration', $('#ration').val())
    formData.set('purchase_price', $('#purchase-price').val())
    formData.set('origin', $('#origin').val())
    formData.set('time', $('#time').val())
    this.api.onUpdateDish(formData).subscribe(
      (res:DishResponse) => {
        if(res.code_status == 200){
          $("#modalAddEdit").modal('hide');
          this.toast.success(res.message)
          this.loadAllRecord()
        }
      }
    )

  }

  destroy(){
    let params = new HttpParams();
    params = params.append('id', JSON.stringify(this.idDish));
    this.api.onDeleteDish(params).subscribe(
      (res: DishResponse) => {
        this.loadAllRecord();
        this.toast.success(res['message']);
        $('#modalDelete').modal('hide');
      }
    )
  }


  loadAllRecord(){
    this.api.onGetDish().subscribe(
      (res:Dish) => {
        this.dish_response = res.data
      }
    )
  }

  loadOrigin(){
    this.api.ongetAllCountry().subscribe(
      (res:CountryResponse) => {
        this.arr_country = res.data
      }
    )
  }

    /**
   * function preview images
   * @param event 
   */
  onFileChanged(event: any){
    this.shared.previewImage(event,'preview')
  }
}
