import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { ToastNotificationService } from 'src/x/http/toast-notification.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from 'src/app/common/shared/shared.service';
import { Spice, Spices } from 'src/app/model/SpiceResponse.model';
import { NgForm } from '@angular/forms';
import { Response } from 'src/app/model/Response.model';
import { HttpParams } from '@angular/common/http';
declare var $: any;
@Component({
  selector: 'app-spice',
  templateUrl: './spice.component.html',
  styleUrls: ['./spice.component.scss']
})
export class SpiceComponent implements OnInit {
  spice_response  = [] as Spices[]
  modalAction: String;
  imageUrl: String = "assets/img/no-img.jpg"
  idSpice: number
  checkName:String

  constructor(
    private api   : ApiService,
    private toast  : ToastNotificationService,
    private router : Router,
    private shared  : SharedService,
    private route  : ActivatedRoute
  ) { }

  ngOnInit() {
    this.loadAllSpice()
  }

  openModalAdd(){
    this.modalAction = "add";
    $('#name').val("")
    $('#purchase-price').val("")
    $('#description').val("")
    $('#image').prop("value", "");
    $("#modalAddEdit").modal('show');
    $("#preview").attr("src","assets/img/no-img.jpg");
  }

  openModalEdit(item){
    this.modalAction = "edit";
    this.idSpice = item.id
    $('#name').val(item.name)
    this.checkName = item.name
    $('#purchase-price').val(item.purchase_price)
    $('#description').val(item.description)
    $("#preview").attr("src",item.image);
    $('#image').prop("value", "");
    $("#modalAddEdit").modal('show');
  }

  /**
   * open model delete Category
   */
  onDeleteModel(idModel){
    $('#modalDelete').modal('show');
    this.idSpice = idModel
  }

  onSubmit(f:NgForm, image:HTMLInputElement){
    if($('#name').val().trim() == "" || $('#purchase-price').val().trim() == "" || $('#description').val().trim() == ""){
      this.toast.error("Không được để trống trường truyền lên!")
      return;
    }
    switch(this.modalAction) {
      case "add":
        this.create(f,image)
        break;
      case "edit":
        this.update(f,image)
        break;
      default:
        // code block
    }
  }

  create(f:NgForm, image:HTMLInputElement) {
    var formData = new FormData()
    if(!image.files[0]){
      this.toast.error("Không được để trống trường truyền lên!")
      return;
    }
    formData.set('image', image.files[0])
    formData.set('name', f.value.name)
    formData.set('purchase_price', f.value.purchase_price)
    formData.set('description', f.value.description)
    this.api.onAddSpice(formData).subscribe(
      (res:Response) => {
        if(res.code_status == 200){
          this.loadAllSpice()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      }
    )
  };

  update(f:NgForm, image:HTMLInputElement){
    var formData = new FormData()
    if(image.files[0]){
      formData.set('image', image.files[0])
    }
    if(this.checkName != $('#name').val()){
      formData.set('name', $('#name').val())
    }
    formData.set('id', JSON.stringify(this.idSpice))
    formData.set('purchase_price', $('#purchase-price').val())
    formData.set('description', $("#description").val())
    this.api.onUpdateSpice(formData).subscribe(
      (res:Response) => {
        console.log(res)
        if(res.code_status == 200){
          this.loadAllSpice()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      }
    )
  }

  destroy(){
    let params = new HttpParams();
    params = params.append('id', JSON.stringify(this.idSpice));
    this.api.onDeleteSpice(params).subscribe(
      (res: Response) => {
        this.loadAllSpice();
        this.toast.success(res['message']);
        $('#modalDelete').modal('hide');
      }
    )
  }

  loadAllSpice(){
    this.api.onGetAllSpice().subscribe(
      (res:Spice) => {
        this.spice_response = res.data
      }
    )
  }

  /**
   * function preview images
   * @param event 
   */
  onFileChanged(event: any){
    this.shared.previewImage(event,'preview')
  }

}
