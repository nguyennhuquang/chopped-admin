import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { ToastNotificationService } from 'src/x/http/toast-notification.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from 'src/app/common/shared/shared.service';
import { materialDishData , materialDish, materialDishResponse } from 'src/app/model/materialDish.model';
import { HttpParams } from '@angular/common/http';
import { Dish, Dishes } from 'src/app/model/Dish.model';
import { materialDishCustom, materialDishCustomData, materialsAnswer } from 'src/app/model/materialDishCustom.model';
import { element } from '@angular/core/src/render3';
import { Category, Categories } from 'src/app/model/Category.model';
import { Material, Materials } from 'src/app/model/Material.model';
import { Response } from 'src/app/model/Response.model';
declare var $: any;
@Component({
  selector: 'app-material-dish',
  templateUrl: './material-dish.component.html',
  styleUrls: ['./material-dish.component.scss']
})
export class MaterialDishComponent implements OnInit {

  material_response  = [] as materialDish[]
  category_response = [] as Categories[]
  dish_response  = [] as Dishes[]
  materialCustom = [] as materialDishCustom[]
  material  = [] as Materials[]
  selectedArray = [] as Number[]
  Category_id: string;
  unit  :string = "0"
  modalAction: String;
  idMaterial: number
  imageUrl: String = "assets/img/no-img.jpg"
  checkName:String
  checkFirstArray:String = ""
  translateEmpty = {id: null,name: '',data: []}

  constructor(
    private api   : ApiService,
    private toast  : ToastNotificationService,
    private router : Router,
    private shared  : SharedService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loadAllMaterial()
    this.loadAllMaterialCategory()
  }

  openModalAdd(){
    this.materialCustom.forEach(element => {
      element.data.forEach(element1 => {
        element1.status = 0
        this.material_response.forEach(item => {
          if (item.id == element1.id) {
            element1.status = 2
          }
        })
      });
    });
    this.modalAction = "add";
    $("#modalAddEdit").modal('show');
  }

  save() {
    let materialsAnswer = [] as materialsAnswer[]
    this.materialCustom.forEach(element => {
      element.data.forEach(element1 => {
        if (element1.status == 1) {
          let amountValue = "#count" + element1.id
          materialsAnswer.push({material_id:element1.id, amount: $(amountValue).val()})
        }
      });
    });
    var body = {
      dish_id: this.route.snapshot.paramMap.get('id'),
      materials: materialsAnswer,
    }
    console.log(body)
    this.api.onAdd(body).subscribe(
      (res:Response) => {
        if(res.code_status == 200){
          this.loadAllMaterial()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      }
    )
  }

  loadAllMaterial(){
    let params = new HttpParams();
    params = params.append('id', this.route.snapshot.paramMap.get('id'));
    this.api.onGetAllMaterialsDish(params).subscribe(
      (res:materialDishResponse) => {
        this.material_response = res.data.materials
      }
    )
  }

  loadAllMaterialCategory(){
    this.materialCustom = []
    this.api.onGetAllCategory().subscribe(
      (res:Category) => {
        this.category_response = res.data
        this.checkFirstArray = this.category_response[0].name
        this.category_response.forEach(element => {
          let params = new HttpParams();
          params = params.append('material_category_id', element.id.toString());
          this.api.onGetAllMaterial(params).subscribe(
            (res:Material) => {
              this.material = res.data
              var dataCustom = [] as materialDishCustomData[]
              this.material.forEach(element1 => {
                dataCustom.push({id:element1.id, name:element1.name, image:element1.image, status:0})
              });
              this.materialCustom.push({id:element.id, name:element.name, data: dataCustom})
            }
          )
        });
      }
    )
  }

  action(value) {
    this.materialCustom.forEach(element => {
      element.data.forEach(element1 => {
        if (element1.id == value) {
          if (element1.status == 2) {
            console.log("Đã chọn")
          } else {
            var statusValue = element1.status == 1 ? 0 : 1
            element1.status = statusValue            
          }
        }
      });
    });
  }

  onDeleteModel(idModel){
    $('#modalDelete').modal('show');
    this.Category_id = idModel
  }

  destroy(){
    let params = new HttpParams();
    params = params.append('material_id', this.Category_id);
    params = params.append('dish_id', this.route.snapshot.paramMap.get('id'));
    this.api.onDelete(params).subscribe(
      (res: Response) => {
        this.loadAllMaterial();
        this.toast.success(res['message']);
        $('#modalDelete').modal('hide');
      }
    )
  }

  FuncSearch(value){
    var idInput = "mySearch" + value
    var idUl = "myMenu" + value
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById(idInput);
    filter = input.value.toUpperCase();
    table = document.getElementById(idUl);
    tr = table.getElementsByTagName("tr");
  
    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[2];
      if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }
    }
  }

  goToSpice() {
    this.router.navigate(["/admin/dish/", this.route.snapshot.paramMap.get('id'), "spice"])
  }
}
