import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialDishComponent } from './material-dish.component';

describe('MaterialDishComponent', () => {
  let component: MaterialDishComponent;
  let fixture: ComponentFixture<MaterialDishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialDishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialDishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
