import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { HomeComponent } from './home/home.component';
import { CoreModule } from '../core/core.module';
import { DishComponent } from './dish/dish.component';
import { FormsModule } from '@angular/forms'
import { from } from 'rxjs';
import { MaterialComponent } from './material/material.component';
import { MaterialCategoryComponent } from './material-category/material-category.component';
import { ToolComponent } from './tool/tool.component';
import { SpiceComponent } from './spice/spice.component';
import { TimelineComponent } from './timeline/timeline.component';
import { MaterialDishComponent } from './material-dish/material-dish.component';
import { CountryComponent } from './country/country.component';
import { SpiceDishComponent } from './spice-dish/spice-dish.component';

@NgModule({
  declarations: [AdminComponent, HomeComponent, DishComponent, MaterialComponent, MaterialCategoryComponent, ToolComponent, SpiceComponent, TimelineComponent, MaterialDishComponent, CountryComponent, SpiceDishComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    CoreModule,
    FormsModule
  ],
  providers: [
  
  ]
})
export class AdminModule { }
