import { Component, OnInit } from '@angular/core';
import { Material, Materials } from 'src/app/model/Material.model';
import { ApiService } from 'src/app/service/api.service';
import { ToastNotificationService } from 'src/x/http/toast-notification.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from 'src/app/common/shared/shared.service';
import { NgForm } from '@angular/forms';
import { HttpParams } from '@angular/common/http';
import { Response } from 'src/app/model/Response.model';
declare var $: any;
@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.scss']
})
export class MaterialComponent implements OnInit {
  material_response  = [] as Materials[]
  Category_id: string;
  unit  :string = "0"
  modalAction: String;
  idMaterial: number
  imageUrl: String = "assets/img/no-img.jpg"
  checkName:String
  
  constructor(
    private api   : ApiService,
    private toast  : ToastNotificationService,
    private router : Router,
    private shared  : SharedService,
    private route  : ActivatedRoute
  ) { }

  ngOnInit() {
    this.Category_id = this.route.snapshot.paramMap.get('id');
    this.loadAllMaterial()
  }

  openModalAdd(){
    this.modalAction = "add";
    $('#name').val("")
    $('#price').val("")
    $('#purchase-price').val("")
    $('#unit').val("")
    $("#modalAddEdit").modal('show');
    $('#image').prop("value", "");
    $('#thumbnail').prop("value", "");
    $("#preview").attr("src","assets/img/no-img.jpg");
    $("#preview1").attr("src","assets/img/no-img.jpg");
  }

  openModalEdit(item: { id: number; name: String; purchase_price: any; unit: any; image: any; thumbnail: any; }){
    this.modalAction = "edit";
    console.log(item.thumbnail)
    this.idMaterial = item.id
    $('#name').val(item.name)
    this.checkName = item.name
    $('#purchase-price').val(item.purchase_price)
    $('#unit').val(item.unit)
    $("#preview").attr("src",item.image);
    $("#preview1").attr("src",item.thumbnail);
    $('#image').prop("value", "");
    $('#thumbnail').prop("value", "");
    $("#modalAddEdit").modal('show');
  }

  /**
   * open model delete Category
   */
  onDeleteModel(idModel){
    $('#modalDelete').modal('show');
    this.idMaterial = idModel
  }

  onSubmit(f:NgForm, image:HTMLInputElement, thumbnail:HTMLInputElement){
    if($('#name').val().trim() == "" || $('#purchase-price').val().trim() == "" || $('#unit').val().trim() == ""){
      this.toast.error("Không được để trống trường truyền lên!")
      return;
    }
    switch(this.modalAction) {
      case "add":
        this.create(f,image, thumbnail)
        break;
      case "edit":
        this.update(f,image, thumbnail)
        break;
      default:
        // code block
    }
  }


  create(f:NgForm, image:HTMLInputElement, thumbnail:HTMLInputElement) {
    var formData = new FormData()
    if(!image.files[0]){
      this.toast.error("Không được để trống trường truyền lên!")
      return;
    }
    if(!thumbnail.files[0]){
      this.toast.error("Không được để trống trường truyền lên!")
      return;
    }
    formData.set('material_category_id', this.Category_id)
    formData.set('image', image.files[0])
    formData.set('thumbnail', thumbnail.files[0])
    formData.set('name', f.value.name)
    formData.set('purchase_price', f.value.purchase_price)
    formData.set('unit', f.value.unit)
    this.api.onAddMaterial(formData).subscribe(
      (res:Response) => {
        if(res.code_status == 200){
          this.loadAllMaterial()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      }
    )
  };

  update(f:NgForm, image:HTMLInputElement, thumbnail:HTMLInputElement){
    var formData = new FormData()
    if(image.files[0]){
      formData.set('image', image.files[0])
    }
    if(thumbnail.files[0]){
      formData.set('thumbnail', thumbnail.files[0])
    }
    if(this.checkName != $('#name').val()){
      formData.set('name', $('#name').val())
    }
    formData.set('id', JSON.stringify(this.idMaterial))
    formData.set('purchase_price', $('#purchase-price').val())
    formData.set('unit', $('#unit').val())
    this.api.onUpdateMaterial(formData).subscribe(
      (res:Response) => {
        console.log(res)
        if(res.code_status == 200){
          this.loadAllMaterial()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      }
    )

  }

  destroy(){
    let params = new HttpParams();
    params = params.append('id', JSON.stringify(this.idMaterial));
    this.api.onDeleteMaterial(params).subscribe(
      (res: Response) => {
        this.loadAllMaterial();
        this.toast.success(res['message']);
        $('#modalDelete').modal('hide');
      }
    )
  }


  loadAllMaterial(){
    let params = new HttpParams();
    params = params.append('material_category_id', this.Category_id);
    this.api.onGetAllMaterial(params).subscribe(
      (res:Material) => {
        this.material_response = res.data
        console.log(this.material_response)
      }
    )
  }

  /**
   * function preview images
   * @param event 
   */
  onFileChanged(event: any){
    this.shared.previewImage(event,'preview')
  }

    /**
   * function preview images
   * @param event 
   */
  onFileChanged1(event: any){
    this.shared.previewImage(event,'preview1')
  }
}
