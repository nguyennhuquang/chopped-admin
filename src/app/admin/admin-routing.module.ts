import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { HomeComponent } from './home/home.component';
import { AuthGuardGuard } from '../auth/auth-guard.guard';
import { DishComponent } from './dish/dish.component';
import { MaterialComponent } from './material/material.component';
import { MaterialCategoryComponent } from './material-category/material-category.component';
import { ToolComponent } from './tool/tool.component';
import { SpiceComponent } from './spice/spice.component';
import { TimelineComponent } from './timeline/timeline.component';
import { MaterialDishComponent } from './material-dish/material-dish.component';
import { CountryComponent } from './country/country.component';
import { SpiceDishComponent } from './spice-dish/spice-dish.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'dashboard',
        component: HomeComponent
      },
      {
        path: 'dish',
        component: DishComponent,
      },
      {
        path: 'category',
        component: MaterialCategoryComponent,
      },
      {
        path: 'category/:id/material',
        component: MaterialComponent
      },
      {
        path: 'tool',
        component: ToolComponent
      },
      {
        path: 'spice',
        component: SpiceComponent
      },
      {
        path: 'dish/:id/recipe',
        component: TimelineComponent
      },
      {
        path: 'dish/:id/material',
        component: MaterialDishComponent
      },
      {
        path: 'country',
        component: CountryComponent
      },
      {
        path: 'dish/:id/spice',
        component: SpiceDishComponent
      }
    ],
    component: AdminComponent,
    canActivate: [AuthGuardGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
