import { Component, OnInit } from '@angular/core';
import { CountryResponse, Country } from 'src/app/model/Country.model';
import { ApiService } from 'src/app/service/api.service';
import { ToastNotificationService } from 'src/x/http/toast-notification.service';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/common/shared/shared.service';
import { NgForm } from '@angular/forms';
import { Response } from 'src/app/model/Response.model';
import { HttpParams } from '@angular/common/http';
declare var $: any;
@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent implements OnInit {
  arr_country    = [] as Country[]
  modalAction: String;
  idCategory: number
  
  constructor(
    private api   : ApiService,
    private toast  : ToastNotificationService,
    private router : Router,
    private shared  : SharedService
  ) { }

  ngOnInit() {
    this.loadOrigin()
  }

  openModalAdd(){
    this.modalAction = "add";
    $('#name').val("")
    $('#code').val("")
    $("#modalAddEdit").modal('show');
  }

  openModalEdit(item: { id: number; name: any; code: any; }){
    this.modalAction = "edit";
    this.idCategory = item.id
    $('#name').val(item.name)
    $('#code').val(item.code)
    $("#modalAddEdit").modal('show');
  }

  onDeleteModel(idModel){
    $('#modalDelete').modal('show');
    this.idCategory = idModel
  }

  onSubmit(f:NgForm, image:HTMLInputElement){
    if($('#name').val().trim() == "" || $('#code').val().trim() == ""){
      this.toast.error("Không được để trống trường truyền lên!")
      return;
    }
    switch(this.modalAction) {
      case "add":
        this.create(f)
        break;
      case "edit":
        this.update(f)
        break;
      default:
        // code block
    }
  }

  create(f:NgForm){
    this.api.onAddCountry({name:f.value.name, code:f.value.code}).subscribe(
      (res:Response) => {
        if(res.code_status == 200){
          this.loadOrigin()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      }
    )
  }

  update(f:NgForm){
    var body = {
      id: this.idCategory,
      name: f.value.name,
      code: f.value.code
    }

    this.api.onUpdateCountry(body).subscribe(
      (res:Response) => {
        if(res.code_status == 200){
          this.loadOrigin()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      } 
    )
  }

  destroy(){
    let params = new HttpParams();
    params = params.append('id', JSON.stringify(this.idCategory));
    this.api.onDeleteCountry(params).subscribe(
      (res: Response) => {
        this.loadOrigin();
        this.toast.success(res['message']);
        $('#modalDelete').modal('hide');
      }
    )
  }

  loadOrigin(){
    this.api.ongetAllCountry().subscribe(
      (res:CountryResponse) => {
        this.arr_country = res.data
      }
    )
  }
}
