import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { ToastNotificationService } from 'src/x/http/toast-notification.service';
import { UserInfo } from 'src/app/model/userInfo.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  user_Name   : string
  user_Avatar : string 
  constructor(
    private apiService   : ApiService,
    private toastService : ToastNotificationService,
  ) { }

  ngOnInit() {

  }
  

  /**
   * function get user information
   */
  // getDataUser(){
  //   this.apiService.getUserInfo().subscribe(
  //     (res:UserInfo ) => {
  //       if(res.code == 200){
  //         this.user_Name = res.data.name
  //         this.user_Avatar = res.data.avatar
  //         this.toastService.success(res['message'])
  //       }
  //     }
  //   )
  // }
}
