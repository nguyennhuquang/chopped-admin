import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { ToastNotificationService } from 'src/x/http/toast-notification.service';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/common/shared/shared.service';
import { Categories, Category } from 'src/app/model/Category.model';
import { NgForm } from '@angular/forms';
import { Response } from 'src/app/model/Response.model';
import { HttpParams } from '@angular/common/http';
declare var $: any;
@Component({
  selector: 'app-material-category',
  templateUrl: './material-category.component.html',
  styleUrls: ['./material-category.component.scss']
})
export class MaterialCategoryComponent implements OnInit {
  category_response = [] as Categories[]
  modalAction: String;
  idCategory: number


  constructor(
    private api   : ApiService,
    private toast  : ToastNotificationService,
    private router : Router,
    private shared  : SharedService
  ) { }

  ngOnInit() {
    this.getData()
  }

  openModalAdd(){
    this.modalAction = "add";
    $('#name').val("")
    $("#modalAddEdit").modal('show');
  }

  openModalEdit(item: { id: number; name: any; }){
    this.modalAction = "edit";
    this.idCategory = item.id
    $('#name').val(item.name)
    $("#modalAddEdit").modal('show');
  }

  onDeleteModel(idModel){
    $('#modalDelete').modal('show');
    this.idCategory = idModel
  }

  onSubmit(f:NgForm, image:HTMLInputElement){
    if($('#name').val().trim() == ""){
      this.toast.error("Không được để trống trường truyền lên!")
      return;
    }
    switch(this.modalAction) {
      case "add":
        this.create(f)
        break;
      case "edit":
        this.update(f)
        break;
      default:
        // code block
    }
  }

  create(f:NgForm){
    this.api.onAddCategory({name:f.value.name}).subscribe(
      (res:Response) => {
        if(res.code_status == 200){
          this.getData()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      }
    )
  }

  update(f:NgForm){
    var body = {
      id: this.idCategory,
      name: f.value.name
    }
    this.api.onUpdateCategory(body).subscribe(
      (res:Response) => {
        if(res.code_status == 200){
          this.getData()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      } 
    )
  }

  destroy(){
    let params = new HttpParams();
    params = params.append('id', JSON.stringify(this.idCategory));
    this.api.onDeleteCategory(params).subscribe(
      (res: Response) => {
        this.getData();
        this.toast.success(res['message']);
        $('#modalDelete').modal('hide');
      }
    )
  }

  getData(){
    this.api.onGetAllCategory().subscribe(
      (res:Category) => {
        this.category_response = res.data
      }
    )
  }

}
