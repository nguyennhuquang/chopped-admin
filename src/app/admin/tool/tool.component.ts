import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { ToastNotificationService } from 'src/x/http/toast-notification.service';
import { Router } from '@angular/router';
import { SharedService } from 'src/app/common/shared/shared.service';
import { Tool, Tools } from 'src/app/model/Tool.model';
import { NgForm } from '@angular/forms';
import { Response } from 'src/app/model/Response.model';
import { HttpParams } from '@angular/common/http';
declare var $: any;
@Component({
  selector: 'app-tool',
  templateUrl: './tool.component.html',
  styleUrls: ['./tool.component.scss']
})
export class ToolComponent implements OnInit {
  imageUrl: String = "assets/img/no-img.jpg"
  tool_response = [] as Tools[]
  modalAction: String;
  idTool: number
  checkName:String

  constructor(
    private api   : ApiService,
    private toast  : ToastNotificationService,
    private router : Router,
    private shared  : SharedService
  ) { }

  ngOnInit() {
    this.getData()
  }

  openModalAdd(){
    this.modalAction = "add";
    $('#image').prop("value", "");
    $("#preview").attr('src', "assets/img/no-img.jpg");
    this.imageUrl =  
    $('#name').val("")
    $("#modalAddEdit").modal('show');
  }

  openModalEdit(item: { id: number; name: String; image: any; }){
    this.modalAction = "edit";
    this.idTool = item.id
    this.checkName = item.name
    $('#name').val(item.name)
    $("#preview").attr('src', item.image);
    $('#image').prop("value", "");
    $("#modalAddEdit").modal('show');
  }

  onDeleteModel(idModel: number){
    $('#modalDelete').modal('show');
    this.idTool = idModel
  }

  onSubmit(f:NgForm, image:HTMLInputElement){
    if($('#name').val().trim() == ""){
      this.toast.error("Không được để trống trường truyền lên!")
      return;
    }
    switch(this.modalAction) {
      case "add":
        this.create(f, image)
        break;
      case "edit":
        this.update(f,image)
        break;
      default:
        // code block
    }
  }

  create(f:NgForm, image:HTMLInputElement){
    var formData = new FormData()
    if(!image.files[0]){
      this.toast.error("Không được để trống trường truyền lên!")
      return;
    }
    formData.set('image', image.files[0])
    formData.set('name', f.value.name)
    this.api.onAddTool(formData).subscribe(
      (res:Response) => {
        if(res.code_status == 200){
          this.getData()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      }
    )
  }

  update(f:NgForm, image:HTMLInputElement){
    var formData = new FormData()
    formData.set('id', JSON.stringify(this.idTool))
    if(image.files[0]){
      formData.set('image', image.files[0])
    }
    if(this.checkName != $('#name').val()){
      formData.set('name', $('#name').val())
    }
    this.api.onUpdateTool(formData).subscribe(
      (res:Response) => {
        if(res.code_status == 200){
          this.getData()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      }
    )
  }

  destroy(){
    let params = new HttpParams();
    params = params.append('id', JSON.stringify(this.idTool));
    this.api.onDeleteTool(params).subscribe(
      (res: Response) => {
        this.getData();
        this.toast.success(res['message']);
        $('#modalDelete').modal('hide');
      }
    )
  }

  getData(){
    this.api.onGetAllTool().subscribe(
      (res:Tool) => {
        this.tool_response = res.data
        console.log(this.tool_response)
      }
    )
  }

  /**
  * function preview images
  * @param event 
  */
 onFileChanged(event: any){
  this.shared.previewImage(event,'preview')
}

}
