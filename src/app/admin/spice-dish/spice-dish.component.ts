import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { ToastNotificationService } from 'src/x/http/toast-notification.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from 'src/app/common/shared/shared.service';
import { Spice, Spices } from 'src/app/model/SpiceResponse.model';
import { HttpParams } from '@angular/common/http';
import { Response } from 'src/app/model/Response.model';
declare var $: any;
@Component({
  selector: 'app-spice-dish',
  templateUrl: './spice-dish.component.html',
  styleUrls: ['./spice-dish.component.scss']
})
export class SpiceDishComponent implements OnInit {
  spice_response  = [] as Spices[]
  spice_dishresponse  = [] as Spices[]
  Category_id: string;
  constructor(
    private api   : ApiService,
    private toast  : ToastNotificationService,
    private router : Router,
    private shared  : SharedService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loadSpice()
    this.loadAllSpice()
  }

  openModalAdd(){
    // this.spice_response.forEach(element => {
    //   this.spice_dishresponse.forEach(element1  => {
    //     if (element.id == element1.id) {
    //       element.status = 2
    //     } 
    //   });
    // });
    $("#modalAddEdit").modal('show');
  }

  loadAllSpice(){
    this.api.onGetAllSpice().subscribe(
      (res:Spice) => {
        this.spice_response = res.data
      }
    )
  }

  loadSpice() {
    let params = new HttpParams();
    params = params.append('dish_id', this.route.snapshot.paramMap.get('id'));
    this.api.onGetAllSpiceDish(params).subscribe(
      (res:Spice) => {
        this.spice_dishresponse = res.data
      }
    )
  }

  action(id) {
    this.spice_response.forEach(element => {
      if (element.id == id) {
        var statusValue = element.status == 1 ? 0 : 1
        element.status = statusValue  
        // if (element.status == 2) {
        //   console.log("Đã chọn")
        // } else {
        //   var statusValue = element.status == 1 ? 0 : 1
        //   element.status = statusValue            
        // }
      }
    });
  }

  save() {
    let spiceAnswer = [] as number[]
    this.spice_response.forEach(element => {
      if (element.status == 1) {
        spiceAnswer.push(element.id)
      }
    });
    let body = {
      "dish_id": this.route.snapshot.paramMap.get('id'),
      "spice_ids": spiceAnswer
    }

    this.api.onAddSpiceDish(body).subscribe(
      (res:Response) => {
        if(res.code_status == 200){
          this.loadSpice()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      }
    )
  }

  onDeleteModel(idModel){
    $('#modalDelete').modal('show');
    this.Category_id = idModel
  }

  destroy(){
    let body = {
      "dish_id": this.route.snapshot.paramMap.get('id'),
      "spice_id": this.Category_id
    }
    this.api.onDeleteSpiceDish(body).subscribe(
      (res:Response) => {
        if(res.code_status == 200){
          this.loadSpice()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      }
    )
  }

  goToSpice() {
    this.router.navigate(["/admin/dish/", this.route.snapshot.paramMap.get('id'), "material"])
  }

  FuncSearch(){
    var idInput = "mySearch"
    var idUl = "myMenu"
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById(idInput);
    filter = input.value.toUpperCase();
    table = document.getElementById(idUl);
    tr = table.getElementsByTagName("tr");
  
    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td")[1];
      if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
          tr[i].style.display = "";
        } else {
          tr[i].style.display = "none";
        }
      }
    }
  }
}
