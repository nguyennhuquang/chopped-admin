import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpiceDishComponent } from './spice-dish.component';

describe('SpiceDishComponent', () => {
  let component: SpiceDishComponent;
  let fixture: ComponentFixture<SpiceDishComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpiceDishComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpiceDishComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
