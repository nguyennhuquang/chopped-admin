import { Component, OnInit } from '@angular/core';
import { Recipes, Recipe } from 'src/app/model/Recipe.model';
import { ApiService } from 'src/app/service/api.service';
import { ToastNotificationService } from 'src/x/http/toast-notification.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from 'src/app/common/shared/shared.service';
import { HttpParams } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Response } from 'src/app/model/Response.model';
import { ThrowStmt } from '@angular/compiler';
declare var $: any;
@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {
  recipe_response = [] as Recipes[]
  modalAction: String;
  idRecipe: number
  distId: String
  time:string = "0"
  checkActonModel:boolean
  checkName:String
  
  constructor(
    private api   : ApiService,
    private toast  : ToastNotificationService,
    private router : Router,
    private shared  : SharedService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.time = "0"
    this.loadAllRecipe()
  }

  openModalAdd(){
    $("#index-show").hide();
    this.modalAction = "add";
    $('#name').val("")
    $('#description').val("")
    this.time = "0"
    $("#modalAddEdit").modal('show');
  }

  openModalEdit(item){
    this.modalAction = "edit";
    $("#index-show").show();
    this.idRecipe = item.id
    $('#name').val(item.name)
    $('#description').val(item.description)
    this.checkName = item.name
    $('#index').val(item.index)
    this.time = item.time
    $("#modalAddEdit").modal('show');
  }

  onDeleteModel(idModel){
    $('#modalDelete').modal('show');
    this.idRecipe = idModel
  }

  onSubmit(f:NgForm){
    console.log(f.value.time)
    if($('#name').val().trim() == "" || $('#description').val().trim() == "" || f.value.time == "0"){
      this.toast.error("Không được để trống trường truyền lên!")
      return;
    }
    switch(this.modalAction) {
      case "add":
        this.create(f)
        break;
      case "edit":
        this.update(f)
        break;
      default:
        // code block
    }
  }

  create(f:NgForm){
    var body = {
      dish_id: this.route.snapshot.paramMap.get('id'),
      name: f.value.name,
      description: f.value.description,
      time: f.value.time
    }
    this.api.onAddRecipe(body).subscribe(
      (res:Response) => {
        if(res.code_status == 200){
          this.loadAllRecipe()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      }
    )
  }

  update(f:NgForm){
    var body : {}
    if(this.checkName != $('#name').val()){
      body = {
        id: this.idRecipe,
        name: $("#name").val(),
        description: $("#description").val(),
        time: $("#time").val(),
        index: $("#index").val(),
      }
    }else {
      body = {
        id: this.idRecipe,
        description: $("#description").val(),
        time: $("#time").val(),
        index: $("#index").val(),
      }
    }
    this.api.onUpdateRecipe(body).subscribe(
      (res:Response) => {
        if(res.code_status == 200){
          this.loadAllRecipe()
          this.toast.success(res.message)
          $("#modalAddEdit").modal('hide');
        }else if(res.code_status == 400){
          this.toast.error("Name is exist!")
        }
      }
    )
  }

  destroy(){
    let params = new HttpParams();
    params = params.append('id', JSON.stringify(this.idRecipe));
    this.api.onDeleteRecipe(params).subscribe(
      (res: Response) => {
        this.loadAllRecipe();
        this.toast.success(res['message']);
        $('#modalDelete').modal('hide');
      }
    )
  }

  loadAllRecipe(){
    let params = new HttpParams();
    params = params.append('dish_id', this.route.snapshot.paramMap.get('id'));
    this.api.onGetAllRecipe(params).subscribe(
      (res:Recipe) => {
        this.recipe_response = res.data
        console.log(this.recipe_response)
      }
    )
  }

}
