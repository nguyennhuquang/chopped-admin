import { Injectable } from '@angular/core';
import { HttpService } from 'src/x/http/http.service';
import { API_URL } from '../common/api';
import { UserInfo } from '../model/userInfo.model';
import { SignIn } from 'src/app/model/SignIn.model'
import { from } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Dish } from '../model/Dish.model';
import { CountryResponse } from '../model/Country.model';
import { DishResponse } from '../model/DishResponse';
import { Material } from '../model/Material.model';
import { Category } from '../model/Category.model';
import { Response } from '../model/Response.model';
import { Tool } from '../model/Tool.model';
import { Spice } from '../model/SpiceResponse.model';
import { Recipe } from '../model/Recipe.model';
import { materialDish, materialDishResponse } from '../model/materialDish.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpService,
    private httpApi :HttpClient
  ) { }

  onSignIn(f:any){
    return this.http.Post<SignIn>(API_URL.signIn, f)
  }

  ongetAllCountry(){
    return this.http.Get<CountryResponse>(API_URL.getAllCountry)
  }

  onAddCountry(f:any) {
    return this.http.Post<Response>(API_URL.addCountry, f)
  }

  onUpdateCountry(f:any) {
    return this.http.Post<Response>(API_URL.updateCountry, f)
  }

  onDeleteCountry(params: any){
    return this.http.Get<Response>(API_URL.deleteCountry, params)
  }
  /**
   * dish
   */
  onGetDish(){
    return this.http.Get<Dish>(API_URL.dishGetAll)
  }

  onAddDish(f:any){
    return this.http.Post<DishResponse>(API_URL.dishAdd, f)
  }

  onUpdateDish(f:any){
    return this.http.Post<DishResponse>(API_URL.dishUpdate, f)
  }

  onDeleteDish(params: any){
    return this.http.Get<DishResponse>(API_URL.dishDelete, params)
  }

  /**
   * material
   */
  onGetAllMaterial(params: any){
    return this.http.Get<Material>(API_URL.materialGetAll, params)
  }
  onAddMaterial(f:any){
    return this.http.Post<Response>(API_URL.materialAdd, f)
  }
  onUpdateMaterial(f:any){
    return this.http.Post<Response>(API_URL.materialUpdate, f)
  }
  onDeleteMaterial(params: any){
    return this.http.Get<Response>(API_URL.materialDelete, params)
  }

  /**
   * category
   */
  onGetAllCategory(){
    return this.http.Get<Category>(API_URL.categoryGetAll)
  }
  onAddCategory(f:any){
    return this.http.Post<Response>(API_URL.categoryAdd, f)
  }
  onUpdateCategory(f:any){
    return this.http.Post<Response>(API_URL.categoryUpdate, f)
  }
  onDeleteCategory(params: any){
    return this.http.Get<Response>(API_URL.categoryDelete, params)
  }

  /**
   * tool
   */
  onGetAllTool(){
    return this.http.Get<Tool>(API_URL.toolGetAll)
  }
  onAddTool(f:any){
    return this.http.Post<Response>(API_URL.toolAdd, f)
  }
  onUpdateTool(f:any){
    return this.http.Post<Response>(API_URL.toolUpdate, f)
  }
  onDeleteTool(params: any){
    return this.http.Get<Response>(API_URL.toolDelete, params)
  }

  /**
   * spice
   */
  onGetAllSpice(){
    return this.http.Get<Spice>(API_URL.spiceGetAll)
  }
  onAddSpice(f:any){
    return this.http.Post<Response>(API_URL.spiceAdd, f)
  }
  onUpdateSpice(f:any){
    return this.http.Post<Response>(API_URL.spiceUpdate, f)
  }
  onDeleteSpice(params: any){
    return this.http.Get<Response>(API_URL.spiceDelete, params)
  }

  /**
   * recipe
   */
  onGetAllRecipe(params: any){
    return this.http.Get<Recipe>(API_URL.recipeGetAll, params)
  }
  onAddRecipe(f:any){
    return this.http.Post<Response>(API_URL.recipeAdd, f)
  }
  onUpdateRecipe(f:any){
    return this.http.Post<Response>(API_URL.recipeUpdate, f)
  }
  onDeleteRecipe(params: any){
    return this.http.Get<Response>(API_URL.recipeDelete, params)
  }

  /**
   * 
   * @param params 
   */
  onGetAllMaterialsDish(params: any){
    return this.http.Get<materialDishResponse>(API_URL.materialsDishGet, params)
  }
  onAdd(f:any){
    return this.http.Post<Response>(API_URL.materialsDishAdd, f)
  }
  onUpdate(f:any){
    return this.http.Post<Response>(API_URL.materialsDishUpdate, f)
  }
  onDelete(f: any){
    return this.http.Post<Response>(API_URL.materialsDishDelete, f)
  }

  onGetAllSpiceDish(params: any){
    return this.http.Get<Spice>(API_URL.spicesDishGet, params)
  }

  onAddSpiceDish(f: any){
    return this.http.Post<Response>(API_URL.spicesDishAdd, f)
  }

  onDeleteSpiceDish(f: any){
    return this.http.Post<Response>(API_URL.spicesDishDelete, f)
  }
  /// 

}