import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  getToken(){
    return localStorage.getItem('token')
  }

  setToken(value: string){
    return localStorage.setItem('token', value)
  }

  getItem(key: string) {
    localStorage.getItem(key)
  }

  setItem(key: string, value: Object) {
    localStorage.setItem(key, JSON.stringify(value))
  }
}
