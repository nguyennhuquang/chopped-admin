import { Component } from '@angular/core';
import { ToastNotificationService } from 'src/x/http/toast-notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'master';
}
