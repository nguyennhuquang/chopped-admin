import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ToastNotificationService } from './toast-notification.service';
import { AuthService } from 'src/app/service/auth.service';
import 'rxjs/add/operator/finally';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';


@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http         : HttpClient,
    private toastService : ToastNotificationService,
  ) { }

  Get<T>(url: string, params?: any) {
    return this.http.get<T>(url, { params: params }).map(res => {
      return res ? res : []
    }).finally(() => { 
    });
  }

  Post<T>(url: string, body: any) {
      return this.http.post(url, body).map(res => {
          return res ? res : []
      }).catch(err => {
          return Observable.throw(err)
      }).finally(() => {
      });
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthHttpService implements HttpInterceptor {

    constructor(
      private authService : AuthService
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        req = req.clone({
            setHeaders: {
              Authorization: `Bearer ${this.authService.getToken()}`
            },
        });
        return next.handle(req);
    }
}


@Injectable({
  providedIn: 'root'
})
export class HttpErrorService implements HttpInterceptor {

    constructor(
        private toastService : ToastNotificationService,

    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // return next.handle(req).catch((err: HttpErrorResponse) => {
        //     console.log(err)
        //     if (err.error instanceof Error) {
        //         console.error('An error accured', err.error.message);
        //     } else {
        //         this.toastService.error(err.error['error']);
        //     }
        //     return Observable.throw(err);
        // });

        return next.handle(req).do((event: HttpEvent<any>) => { }, (err: any) => {
            if (err instanceof HttpErrorResponse) {
                // do error handling here
                if (err.status == 0) {
                    this.toastService.error('Không kết nối được đến server');
                } else {
                    this.toastService.error(err.error['error']);
                }
            }
        });
    }
}

