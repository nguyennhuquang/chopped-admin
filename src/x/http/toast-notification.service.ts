import { Injectable } from '@angular/core';
import { SnotifyService } from 'ng-snotify';

@Injectable({
  providedIn: 'root'
})
export class ToastNotificationService {

  constructor(
    private snotifyService: SnotifyService,
  ) { }

  clear() {
    this.snotifyService.clear()
  }

  success(message: string) {
    this.clear()
    this.snotifyService.success(message);
  }

  error(message: string) {
      this.clear()
      this.snotifyService.error(message);
  }

}
