export class LocalFactory {
    static getItem(key: string) {
        return localStorage.getItem(key)
    }
    static setItem(key: string, value: Object) {
        return localStorage.setItem(key, JSON.stringify(value))
    }
}